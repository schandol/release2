<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AutoPlus</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">http://vwplpv-j-int.application.amfam.com/vwplpv/viewPolicyDetail.do?nameSearch=nameSearch&amp;policyNumber=</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">http://vwplpv.amfam.com/vwplpv/viewPolicyDetail.do?nameSearch=nameSearch&amp;policyNumber=</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">http://accr05/vwplpv_j10/viewPolicyDetail.do?nameSearch=nameSearch&amp;policyNumber=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Policy</value>
    </values>
</CustomMetadata>
