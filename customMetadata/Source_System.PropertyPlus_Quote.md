<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PropertyPlus</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage-ctpint-auto.application.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexresumequote?</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexresumequote?</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://producerengage-ctpqa-auto.application.amfam.com/ProducerEngage/dist/html/index-agents.html#/apexresumequote?quoteID=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Quote</value>
    </values>
</CustomMetadata>
