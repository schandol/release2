<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Vantage</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">http://vwlife-j-int.application.amfam.com/vwlife/viewPolicyDetail.do?policyNumber=</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">http://vwlife.amfam.com/vwlife/viewPolicyDetail.do?policyNumber=</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://vwlife-j-qa.application.amfam.com/vwlife/viewPolicyDetail.do?policyNumber=</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Policy</value>
    </values>
</CustomMetadata>
