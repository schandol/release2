<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Life_Cornerstone</label>
    <protected>false</protected>
    <values>
        <field>INT_URL__c</field>
        <value xsi:type="xsd:string">https://intssg.amfam.com/saml/v1/ipipeline?</value>
    </values>
    <values>
        <field>Product_Policy_Code__c</field>
        <value xsi:type="xsd:string">Life_Cornerstone</value>
    </values>
    <values>
        <field>Production_URL__c</field>
        <value xsi:type="xsd:string">https://prdssg.amfam.com/saml/v1/ipipeline?</value>
    </values>
    <values>
        <field>QA_URL__c</field>
        <value xsi:type="xsd:string">https://accssg.amfam.com/saml/v1/ipipeline?</value>
    </values>
</CustomMetadata>
