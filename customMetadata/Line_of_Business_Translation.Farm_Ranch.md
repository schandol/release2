<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Farm/Ranch</label>
    <protected>false</protected>
    <values>
        <field>LPE_Code__c</field>
        <value xsi:type="xsd:string">FARM_RANCH</value>
    </values>
    <values>
        <field>LPE_Description__c</field>
        <value xsi:type="xsd:string">Farm/Ranch</value>
    </values>
</CustomMetadata>
