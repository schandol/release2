<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Life NB</label>
    <protected>false</protected>
    <values>
        <field>PlaceHolders__c</field>
        <value xsi:type="xsd:string">https://paymentmanager.amfam.com/PaymentManager/OTP/processPayment.do?paymentMethod=OTP&amp;method=setUpPage&amp;mode=Input</value>
    </values>
</CustomMetadata>
