<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Manage PCIFY settings for all objects with the Manager custom metadata type.</description>
    <fields>
        <fullName>pcify__AuditAction__c</fullName>
        <deprecated>false</deprecated>
        <description>Action to perform when credit card is detected. Update will mask the number. Delete will delete the entire record. Report will generate a PCIFY Log.  Detection supports only Update or Report.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Action to perform when credit card is detected. Update will mask the number. Delete will delete the entire record. Report will generate a PCIFY Log.  Detection supports only Update or Report.</inlineHelpText>
        <label>AuditAction</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Update</fullName>
                    <default>true</default>
                    <label>Update</label>
                </value>
                <value>
                    <fullName>Delete</fullName>
                    <default>false</default>
                    <label>Delete</label>
                </value>
                <value>
                    <fullName>Report</fullName>
                    <default>false</default>
                    <label>Report</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pcify__BatchEndDate__c</fullName>
        <defaultValue>Now()</defaultValue>
        <deprecated>false</deprecated>
        <description>Not required for standard audits via the Audit tab.

Date for the audit to stop finding and masking credit cards.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Not required for standard audits via the Audit tab.

Date for the audit to stop finding and masking credit cards.</inlineHelpText>
        <label>AuditEndDate</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>pcify__BatchFilterField__c</fullName>
        <defaultValue>&quot;CreatedDate&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>When scheduling an Audit, you need to specify which Date/Time field to query with - i.e. CreatedDate, LastModifiedDate, or another Date/Time field.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>When scheduling an Audit, you need to specify which Date/Time field to query with - i.e. CreatedDate, LastModifiedDate, or another Date/Time field.</inlineHelpText>
        <label>AuditDateField</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pcify__BatchQueryLimit__c</fullName>
        <defaultValue>50000</defaultValue>
        <deprecated>false</deprecated>
        <description>50 million records is the maximum number of records retrievable by a Database.QueryLocator query</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>50 million records is the maximum number of records retrievable by a Database.QueryLocator query</inlineHelpText>
        <label>AuditQueryLimit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pcify__BatchSize__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>Number of records in each batch. 200 is the default Batch size. 2000 is the maximum.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Number of records in each batch. 200 is the default Batch size. 2000 is the maximum.</inlineHelpText>
        <label>AuditBatchSize</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pcify__BatchStartDate__c</fullName>
        <defaultValue>Now()</defaultValue>
        <deprecated>false</deprecated>
        <description>Not required for standard audits via the Audit tab.

Date for the audit to start finding and masking records.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Not required for standard audits via the Audit tab.

Date for the audit to start finding and masking records.</inlineHelpText>
        <label>AuditStartDate</label>
        <required>false</required>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>pcify__DetectionAction__c</fullName>
        <defaultValue>&quot;Update&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>PCIFY action upon record Insert or Update. Update will mask the number. Report will generate a PCIFY Log. Delete will delete the entire record.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>PCIFY action upon record Insert or Update. Update will mask the number. Report will generate a PCIFY Log. Delete will delete the entire record.</inlineHelpText>
        <label>DetectionAction</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Update</fullName>
                    <default>true</default>
                    <label>Update</label>
                </value>
                <value>
                    <fullName>Report</fullName>
                    <default>false</default>
                    <label>Report</label>
                </value>
                <value>
                    <fullName>Delete</fullName>
                    <default>false</default>
                    <label>Delete</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pcify__DetectionFieldsPlus__c</fullName>
        <deprecated>false</deprecated>
        <description>If you cannot fit all of your desired PCIFY fields in DetectionFields - add the additional fields here.

Maximum fields length for both DetectionFields and DetectionFieldsPlus is 255 characters.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If you cannot fit all of your desired PCIFY fields in DetectionFields - add the additional fields here.

Maximum fields length for both DetectionFields and DetectionFieldsPlus is 255 characters.</inlineHelpText>
        <label>DetectionFieldsPlus</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>pcify__IsActive__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>To Activate, you MUST either install the PCIFY Extension Package for this object, or write your own Apex Trigger to call PCIFY Managed Classes.  Please contact PCIFY support for details.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>To Activate, you MUST either install the PCIFY Extension Package for this object, or write your own Apex Trigger to call PCIFY Managed Classes.  Please contact PCIFY support for details.</inlineHelpText>
        <label>IsActive</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pcify__LuhnOnly__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This checkbox skips all Detection Patterns.  If selected, PCIFY will only find PII that passes the Luhn Algorithm.  USE WITH CAUTION.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>This checkbox skips all Detection Patterns.  If selected, PCIFY will only find PII that passes the Luhn Algorithm.  USE WITH CAUTION.</inlineHelpText>
        <label>LuhnOnly</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>pcify__MaskFields__c</fullName>
        <deprecated>false</deprecated>
        <description>Enter a list of comma-delimited API field names.

example: 
Subject,Description,CustomTextField__c

IF YOU CANNOT FIT YOUR DESIRED PCIFY FIELDS IN 255 CHARACTERS OR LESS - THEN ADD ADDITIONAL FIELDS TO DETECTIONFIELDSPLUS.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>Enter a list of comma-delimited API field names.

example: 
Subject,Description,CustomTextField__c

IF YOU CANNOT FIT YOUR DESIRED PCIFY FIELDS IN 255 CHARACTERS OR LESS - THEN ADD ADDITIONAL FIELDS TO DETECTIONFIELDSPLUS.</inlineHelpText>
        <label>DetectionFields</label>
        <length>131072</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>pcify__MaskType__c</fullName>
        <businessStatus>DeprecateCandidate</businessStatus>
        <defaultValue>&quot;FullMask&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>DEPRECATED field - use DetectionPattern.MaskType instead.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>DEPRECATED field - use DetectionPattern.MaskType instead.</inlineHelpText>
        <label>MaskType [DEPRECATED]</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>FullMask</fullName>
                    <default>true</default>
                    <label>FullMask</label>
                </value>
                <value>
                    <fullName>Last4</fullName>
                    <default>false</default>
                    <label>Last4</label>
                </value>
                <value>
                    <fullName>First6andLast4</fullName>
                    <default>false</default>
                    <label>First6andLast4</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>pcify__UseOwnTrigger__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If selected, PCIFY will not execute managed package triggers.  You will need to write your own custom trigger and call PCIFY managed classes.</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <inlineHelpText>If selected, PCIFY will not execute managed package triggers.  You will need to write your own custom trigger and call PCIFY managed classes.</inlineHelpText>
        <label>Use Custom Trigger</label>
        <type>Checkbox</type>
    </fields>
    <label>Manager</label>
    <listViews>
        <fullName>pcify__All_Manager_Settings</fullName>
        <columns>MasterLabel</columns>
        <columns>pcify__IsActive__c</columns>
        <columns>pcify__MaskFields__c</columns>
        <columns>pcify__DetectionAction__c</columns>
        <columns>pcify__AuditAction__c</columns>
        <filterScope>Everything</filterScope>
        <label>All Manager Settings</label>
    </listViews>
    <pluralLabel>Manager</pluralLabel>
    <validationRules>
        <fullName>pcify__EmailMessages_Cannot_Be_Updated</fullName>
        <active>true</active>
        <description>You can only Report or Delete EmailMessages after they have been created. Update is supported only on EmailMessages whose Status is Draft or before they have been created.</description>
        <errorConditionFormula>MasterLabel = &apos;EmailMessage&apos; 
&amp;&amp; ISPICKVAL(pcify__AuditAction__c, &apos;Update&apos;)</errorConditionFormula>
        <errorDisplayField>pcify__AuditAction__c</errorDisplayField>
        <errorMessage>You can only Report or Delete EmailMessages after they have been created. Update is supported only on EmailMessages whose Status is Draft or before they have been created.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>pcify__End_Date_Must_Be_Greater_Than_Start_Date</fullName>
        <active>true</active>
        <errorConditionFormula>pcify__BatchStartDate__c &gt; pcify__BatchEndDate__c</errorConditionFormula>
        <errorMessage>The Audit End Date must be GREATER than the Audit Start Date!</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
