({
    getPageLayout : function(component, helper) {
        var action = component.get("c.getPageLayoutMetadata");
        var pageLayoutName = component.get("v.pageLayoutName");

        console.log("pageLayoutName: " + pageLayoutName);

        var actionParams = {
            "pageLayoutName" : pageLayoutName
        };

        action.setParams(actionParams);
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("callback state: " + state);

            if (component.isValid() && state === "SUCCESS") {
                var pageLayout = response.getReturnValue();
                console.log("pageLayout: " + JSON.stringify(pageLayout));

                component.set("v.pageLayout", pageLayout);
            }
        });

        $A.enqueueAction(action);
	},

    validateLeadForm : function(cmp, fields) {
        console.log('in the helper');
        var action = cmp.get("c.sendLeadToCDH");
        
        console.log(JSON.stringify(fields));
        action.setParams({ l : fields });
 
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                alert("From server: " + response.getReturnValue());
 
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
 
        // optionally set storable, abortable, background flag here
 
        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    }
})