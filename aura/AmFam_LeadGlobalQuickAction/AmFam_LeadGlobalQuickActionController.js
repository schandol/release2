({
    doInit : function(cmp, event, helper) {
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/lightning/o/Lead/new"
        });
        urlEvent.fire();
        
        window.setTimeout(
        	$A.getCallback(function() {
        		$A.get("e.force:closeQuickAction").fire();
            }), 0
         );
       
    }
})