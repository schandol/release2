({
    init : function(component, event, helper) {
        component.set("v.isLoading",true);
        //helper.refreshLeadJS(component, helper); commenting out temp until we figure out the solution on this
        helper.retrievePageLayout(component, helper);
        helper.initYearOptions(component, helper);
        helper.initDataTableColumns(component, helper);
    },
    
    onPageReferenceChanged: function(cmp, event, helper) {
        $A.get('e.force:refreshView').fire();
    },

    onsuccess : function(component, event, helper) {
        var payload = event.getParams('fields').response;
        helper.onsuccess(component, payload.id);
    },
    
    validate : function(component, event, helper) {
        console.log('on click');
        var lob = component.find('lob').get('v.value');
        if($A.util.isUndefinedOrNull(lob) ){
          helper.scrollToTop();  
        }
        
    },

    onsubmit : function(component, event, helper) {
        component.set("v.validationErrorMessages", null);
        component.set("v.isLoading",true);
        event.preventDefault();
        
        var fields = event.getParam('fields');

        if(component.get('v.selectedValue') != ''){
            fields.Drivers_License_Issue_Year__c = component.get('v.selectedValue');
        } else {
            fields.Drivers_License_Issue_Year__c = null;
        }
        
        helper.onSubmitValidation(component, event, helper);
        console.log('validationErrorMessages' + component.get("v.validationErrorMessages"));
        if(component.get("v.validationErrorMessages").length == 0){
            if (component.get("v.pageReference").attributes.actionName == 'edit') {
                console.log('in edit submit sequence');
                component.set("v.editLeadRecord",true); 
                helper.editLeadRecord(component,JSON.stringify(fields));
            } else {
                component.find('recordForm').submit(fields);
            }
        }else{
            component.set("v.isLoading",false);
            helper.scrollToTop();
        }
    },

    onerror : function(component, event, helper) {
        component.set("v.isLoading",false);
        // Scroll to top of the page
        helper.scrollToTop();
    },
    
    handleOnLoad: function(component, event, helper) {
        var rec = event.getParam('recordUi');
        if(JSON.stringify(rec).includes('Drivers_License_Issue_Year__c')){
           if (component.get("v.pageReference").attributes.actionName == 'edit') {
               component.set('v.selectedValue', rec.record.fields.Drivers_License_Issue_Year__c.value);
                if (rec.record.fields.Drivers_License_Number__c.value || rec.record.fields.Driver_s_License_State__c.value) {
                    $A.util.removeClass(component.find('dlInfo'),'slds-hide');
                } 
           }
        }


        var canEditAddress = false;

        if(JSON.stringify(rec).includes('Residence_Business__c')) {

            
            if (component.get("v.pageReference").attributes.actionName == 'edit') {    

                //Find out if Address field can be editable: Usage should be Primary and Type should include Residence
                component.set("v.canEditAddress", false);
                if (rec.record.fields.Residence_Business__c.value == 'PRM') {
                    component.set("v.canEditAddress", true);
                    canEditAddress = true;
                }

                var pageLayout = component.get("v.pageLayout");
                var sections = pageLayout['Sections'];
        
                for (const x in sections) {
                    var section = sections[x];
                    var columns = section.Columns;
        
                    for (const colIndex in columns) {
                        var col = columns[colIndex];
                        var fields = col.Fields;
        
                        for (const fieldIndex in fields) {
                            var field = fields[fieldIndex];
                            
                            //Make Address field read only based on canEditAddress 
                            if (field.APIName == 'Address') {
                                field.isReadOnly = !canEditAddress;
                                component.set("v.pageLayout", pageLayout);
                            }
                        }
                    }
                    
                  }
                  //Commit the pageLayout overwrite 
                  component.set("v.pageLayout", pageLayout);
            }
            else {
                //Set the default selected values on Address Usage and Type
                component.find("residence").set("v.value", "PRM");
                component.find("mailing").set("v.value", "PRM");
            }
          
        } 
    },

    
    showDLInfo : function(component, event, helper) {
        if (component.find('dlNumber').get("v.value") || component.find('dlState').get("v.value")) {
            $A.util.removeClass(component.find('dlInfo'),'slds-hide');
         }else if(!component.find('dlMonth').get("v.value") && !component.find('dlYear').get("v.value")){
            $A.util.addClass(component.find('dlInfo'),'slds-hide');
        }
    },
    
    handleRowAction: function (component, event, helper) {
        
        component.set("v.isLoading",true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        helper.select_match(component, row);
    },
    
    
    
   forceCreateUpdateLead : function(component, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.applyPartyMatch", false);
        helper.forceCreateUpdateLeadRecord(component, event);
        
   },

    handleCancel : function(component, event, helper) {
        component.find('navService').navigate({
            "type": "standard__objectPage",
            "attributes": {
                "objectApiName": "Lead",
                "actionName": "home"
            }
        });
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    
     handleBack : function(component, event, helper) {
        component.set("v.errors",null);
        component.set("v.code1Results",null);
        component.set("v.code1ReturnedAddress",null);
        component.set("v.selectedAddress",[]);
        component.set("v.partyMatches",null);
        component.set("v.householdMatches",null);
        component.set("v.code1Complete", false);
        component.set("v.validAddress", true);
        component.set("v.validationErrorMessages", null);
        component.set("v.partyType", "");
        component.set("v.partyLevelCode", "");
        $A.util.removeClass(component.find('leadForm'),'slds-hide');
        var fields = event.getParam('fields');
        if(component.get('v.selectedValue') != '' && fields.Drivers_License_Issue_Year__c != 'undefined'){
            fields.Drivers_License_Issue_Year__c = component.get('v.selectedValue');
        }
    },
    
    addressRowSelected: function(component, event, helper) {
        component.set("v.selectedAddress", event.getParam('selectedRows'));
    },
    
    addressRowToSend: function(component, event, helper) {
        component.set("v.isLoading",true);
        var message = '';
        
        if(component.get("v.selectedAddress").length > 0 ){
            helper.select_address(component, JSON.stringify(component.get("v.selectedAddress"))); 
        }else{
            message = 'Please select one address';
        }
        
        if(message != ''){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"error",
                "title": "Error!",
                "message": message,
                "mode":"dismissible",
                "duration":"20000"
             });
            toastEvent.fire();
            component.set("v.isLoading",false);
        }
     }
})