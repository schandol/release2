({
    
    refreshLeadJS : function(component, helper) {
        var action = component.get("c.refreshLead");
        var recordId = component.get("v.recordId");
        var actionParams = {
            "leadRecordId" : recordId
        };

        action.setParams(actionParams);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("callback state: " + state);

            if (component.isValid() && state === "SUCCESS") {
                // If success, move on to retrieving the page layout for the form
                this.retrievePageLayout(component, helper);
            } else {
                // If the callout fails for whatever reason, continue on regardless.
                this.retrievePageLayout(component, helper);
            }
        });

        $A.enqueueAction(action);
    },

    retrievePageLayout : function(component, helper) {
        var action = component.get("c.getPageLayoutMetadata");
        var pageLayoutName = component.get("v.pageLayoutName");

        console.log("pageLayoutName: " + pageLayoutName);
        var actionParams = {
            "pageLayoutName" : pageLayoutName
        };

        action.setParams(actionParams);
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("callback state: " + state);

            if (component.isValid() && state === "SUCCESS") {
                var pageLayout = response.getReturnValue();
                console.log("pageLayout: " + JSON.stringify(pageLayout));
                component.set("v.isLoading",false);
                component.set("v.pageLayout", pageLayout);
            }
        });

        $A.enqueueAction(action);
    },

    onsuccess : function(component, leadId) {
        
        component.set("v.validAddress", true);
        component.set("v.exactMatch",false);
        var selectedAddress = null;
        if(component.get("v.selectedAddress") && component.get("v.selectedAddress").length >0){
            selectedAddress = JSON.stringify(component.get("v.selectedAddress"));
        }
        var action = component.get("c.sendLeadToCDH");

        var actionParams = {
            "leadId" : leadId,
            "code1Complete":component.get("v.code1Complete"),
            "applyPartyMatch":component.get("v.applyPartyMatch"),
            "selectedAddress":selectedAddress
        };
        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("callback state: " + state);

            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If successful insert, redirect
                console.log(resp);
                if (resp.errors == null && resp.householdMatch == null && (resp.partyMatch == null || resp.partyMatch == "" ) && resp.code1 == null) {
                    this.redirectToRecord(component, resp.l.Id);
                } else {
                    $A.util.removeClass(component.find("leadForm"), 'slds-hide');
                    //Otherwise report back any CDH responses to the component
                    component.set("v.lead",resp.l);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    if(JSON.stringify(resp.errors).includes('D-112') || JSON.stringify(resp.errors).includes('D-113')){
                        component.set("v.validAddress", false);
                    }else{
                        component.set("v.validAddress", true);
                        if(resp.code1 != null){
                            component.set("v.code1ReturnedAddress",resp.returnedCode1Addresses.Code1Wrapper.AddressesReturnedFromCode1);
                            component.set("v.code1Complete", true);
                        }    
                    }
                    
                    component.set("v.partyMatches",resp.partyMatch);
                    component.set("v.partyLevelCode", resp.partyLevelCode);
                    component.set("v.partyType", JSON.stringify(resp.partyMatch).includes('isLead')?'isLead':JSON.stringify(resp.partyMatch).includes('isPersonAccount')?'isPersonAccount':JSON.stringify(resp.partyMatch).includes('isBusinessAccount')?'isBusinessAccount':null);
                    
                    component.set("v.householdMatches",resp.householdMatch);
                    if(JSON.stringify(resp.partyMatch).includes('EXACT')){
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    if (resp.code1 && resp.code1 != "" && component.get("v.validAddress") || resp.partyMatch && resp.partyMatch != ""|| resp.householdMatch && resp.householdMatch != "") {
                        $A.util.addClass(component.find('leadForm'),'slds-hide');
                    }
                    this.scrollToTop();
                }
            }else if(state == "ERROR"){
                  var errors = response.getError();
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.validationErrorMessages", errors[0].message);
              }
        });

        $A.enqueueAction(action);
    },
    
    editLeadRecord : function(component, lead) {
        
        component.set("v.validAddress", true);
        var recordId = component.get("v.recordId");
        var action = component.get("c.sendLeadEditToCDH");
        var selectedAddress = null;
        component.set("v.exactMatch",false);
        if(component.get("v.selectedAddress") && component.get("v.selectedAddress").length >0){
            selectedAddress = JSON.stringify(component.get("v.selectedAddress"));
        }
        
        var actionParams = {
            "leadRecord" : lead,
            "leadId":recordId,
            "code1Complete":component.get("v.code1Complete"),
            "applyPartyMatch":component.get("v.applyPartyMatch"),
            "selectedAddress":selectedAddress
        };
        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // If successful insert, redirect
                console.log(resp);
                
                if (resp.errors == null && resp.householdMatch == null && (resp.partyMatch == null || resp.partyMatch == "" ) && resp.code1 == null) {
                    this.redirectToRecord(component, component.get("v.recordId"));
                } else {
                    $A.util.removeClass(component.find("leadForm"), 'slds-hide');
                    // Otherwise report back any CDH responses to the component
                    component.set("v.lead",resp.l);
                    component.set("v.errors",resp.errors);
                    component.set("v.code1Results",resp.code1);
                    if(JSON.stringify(resp.errors).includes('D-112') || JSON.stringify(resp.errors).includes('D-113')){
                        component.set("v.validAddress", false);
                    }else{
                        component.set("v.validAddress", true);
                        if(resp.code1 != null){
                            component.set("v.code1ReturnedAddress",resp.returnedCode1Addresses.Code1Wrapper.AddressesReturnedFromCode1);
                            component.set("v.code1Complete", true);
                        }    
                    }
                    component.set("v.partyMatches",resp.partyMatch);
                    component.set("v.partyLevelCode", JSON.stringify(resp.partyMatch).includes('LEAD')?'LEAD':'CONTACT');
                    component.set("v.partyType", JSON.stringify(resp.partyMatch).includes('isLead')?'isLead':JSON.stringify(resp.partyMatch).includes('isPersonAccount')?'isPersonAccount':JSON.stringify(resp.partyMatch).includes('isBusinessAccount')?'isBusinessAccount':null);
                    component.set("v.householdMatches",resp.householdMatch);
                    if(JSON.stringify(resp.partyMatch).includes('EXACT')){
                        component.set("v.exactMatch",true);
                    }
                    component.set("v.isLoading",false);
                    if (resp.code1 && resp.code1 != "" && component.get("v.validAddress") || resp.partyMatch && resp.partyMatch != ""|| resp.householdMatch && resp.householdMatch != "") {
                        $A.util.addClass(component.find('leadForm'),'slds-hide');
                    }
                    this.scrollToTop();
                    component.set("v.isLoading",false); 
                }
             }else if(state == "ERROR"){
                  var errors = response.getError(); 
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.validationErrorMessages", errors[0].message);
              }
        });

        $A.enqueueAction(action);
        },
    
    select_match : function(component, row) {
        
        var leadRec = component.get("v.lead");
        var action;
        
        if(component.get("v.partyType") == "isLead" || component.get("v.partyLevelCode") == 'LEAD'){
            action = component.get("c.selectLeadPartyMatch");
        }else{
            action = component.get("c.selectAccountPartyMatch");
        }
        
        var actionParams = {
            "selectedRow" : JSON.stringify(row),
            "leadRecord" : leadRec,
            "editLead" : component.get("v.editLeadRecord")?true:false ,
            "partyType" : component.get("v.partyType")
        };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                // Redirect to existing Lead
                this.redirectToRecord(component, resp);
                /*if(component.get("v.partyType") == "isLead"){
                    this.redirectToRecord(component, resp);
                }else{
                    if(component.get("v.editLeadRecord")){
                        this.redirectToRecord(component, resp);
                        //component.set("v.applyPartyMatch", false);
                        //this.editLeadRecord(component, resp);
                    }else{
                        console.log('leadID>>' + resp);
                        this.redirectToRecord(component, resp);
                        //component.set("v.applyPartyMatch", false);
                        //this.onsuccess(component, resp); 
                    }
                }*/
                
            }else if(state == "ERROR"){
                  var errors = response.getError();
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.validationErrorMessages", errors[0].message);
             }
        });

        $A.enqueueAction(action);
    },
    

    onsubmit : function(component, event, helper) {
    },
    
    initDataTableColumns : function(component, helper) {
        component.set('v.addressMatchColumns',[
            {label: 'Address Type', fieldName: 'Code1ReturnIndicator', type: 'text', wrapText: true, iconName: 'standard:marketing_actions'}, 
            {label: 'Street', fieldName: 'addressLine1', type: 'text', iconName: 'standard:visits'},
            //{label: 'Address Line 2', fieldName: 'addressLine2', type: 'text'},
            {label: 'City', fieldName: 'city', type: 'text', iconName: 'standard:service_territory'},
            {label: 'State', fieldName: 'stateCode', type: 'text', iconName: 'standard:location_permit'},
            {label: 'Zip', fieldName: 'zip5Code', type: 'text', iconName: 'standard:location'},
            {label: 'Confirmed Delivery Point?', fieldName: 'deliveryPointValidationMatchIndicator', type: 'text', wrapText: true, iconName: 'standard:home'},
            {label: 'Commercial Mail Receiving Agency?', fieldName: 'deliveryPointValidationCommercialMailreceivingCode', type: 'text', wrapText: true, iconName: 'standard:store'}
            //{label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Address', name: 'select_address', title: 'Click to select this address', iconName: 'standard:address', iconPosition: 'left' }}
        ]);
        
        component.set('v.partyMatchColumns', [
            {label: 'Match Type', fieldName: 'matchType', type: 'text', iconName: 'standard:user_role'},
            {label: 'Name', fieldName: 'fullName', type: 'text', iconName: 'standard:avatar'},
            {label: 'Company', fieldName: 'companyName', type: 'text', iconName: 'standard:store_group'},
            {label: 'Age', fieldName: 'age', type: 'text', iconName: 'standard:channel_program_members'},
            {label: 'Type', fieldName: 'partyLevelCode', type: 'text', iconName: 'standard:category'},
            {label: 'Address', fieldName: 'address', type: 'text', wrapText: true, iconName: 'standard:address'},
            {label: 'Driver\'s License' , fieldName: 'dlicense', type: 'text',iconName: 'standard:employee_contact'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_match', title: 'Click to select this match', variant: 'brand'}}
        ]);
        
        component.set('v.partyMatchPersonColumns', [
            {label: 'Match Type', fieldName: 'matchType', type: 'text', iconName: 'standard:user_role'},
            {label: 'Name', fieldName: 'fullName', type: 'text', iconName: 'standard:avatar'},
            {label: 'Age', fieldName: 'age', type: 'text', iconName: 'standard:channel_program_members'},
            {label: 'Type', fieldName: 'partyLevelCode', type: 'text', iconName: 'standard:category'},
            {label: 'Address', fieldName: 'address', type: 'text', wrapText: true, iconName: 'standard:address'},
            {label: 'Driver\'s License' , fieldName: 'dlicense', type: 'text',iconName: 'standard:employee_contact'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_match', title: 'Click to select this match', variant: 'brand'}}
        ]);
        
        component.set('v.partyMatchBusinessColumns', [
            {label: 'Match Type', fieldName: 'matchType', type: 'text'},
            {label: 'Name', fieldName: 'fullName', type: 'text' , iconName: 'standard:avatar'},
            {label: 'Type', fieldName: 'partyLevelCode', type: 'text', iconName: 'standard:category'},
            {label: 'Address', fieldName: 'address', type: 'text', wrapText: true, iconName: 'standard:address'},
            {label: 'Form Of Business', fieldName: 'formOfBusiness', type: 'text', iconName: 'standard:store'},
            {label: 'Taxpayer ID', fieldName: 'taxpayerID', type: 'text', iconName: 'standard:employee_contact'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_business_match', title: 'Click to select this match', variant: 'brand'}}
        ]);
      },
    
    
     
    select_address: function(component, selectedAddress) {
        
        var action;
        if(component.get("v.editLeadRecord")){
            action = component.get("c.updateLeadRecord");
        }else{
            action = component.get("c.createLead");
        }
        
        var leadRec = component.get("v.lead");
        
        var actionParams = {
            "leadRec" : leadRec,
            "selectedAddress":selectedAddress,
         };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                
                   if(component.get("v.editLeadRecord")){
                        this.editLeadRecord(component, JSON.stringify(resp));
                    }else{
                        this.onsuccess(component, resp);
                    }
            }else if(state == "ERROR"){
                  var errors = response.getError();
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.validationErrorMessages", errors[0].message);
              }
        });

        $A.enqueueAction(action);
    },
    
    forceCreateUpdateLeadRecord: function(component, event) {
        var action;
        var leadId;
        if(component.get("v.editLeadRecord")){
            action = component.get("c.updateLeadRecord");
            leadId = component.get("v.recordId");
        }else{
            action = component.get("c.createLead");
        }
        var leadRec = component.get("v.lead");
        var actionParams = {
            "leadRec" : leadRec,
            "selectedAddress":null,
            "leadId": leadId
         };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            console.log("Success" + response.getReturnValue());
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                
                    if(component.get("v.editLeadRecord")){
                        this.editLeadRecord(component, JSON.stringify(resp));
                    }else{
                        this.onsuccess(component, resp);
                    }
            }else if(state == "ERROR"){
                  var errors = response.getError();
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.validationErrorMessages", errors[0].message);
              }
        });

        $A.enqueueAction(action);
    },
    
    onSubmitValidation : function(component, event, helper) {
        
        var fields = event.getParam('fields');
        const messages = [];
        var firstName = fields.FirstName ? fields.FirstName.toLowerCase() : null; 
        var lastName = fields.LastName ? fields.LastName.toLowerCase() : null;
        var middleName = fields.MiddleName ? fields.MiddleName.toLowerCase() : null;
        
        //Name Validtion
        if(!fields.FirstName || !fields.LastName ){
            messages.push('First/Last Name Cannot be Blank');
        }
        if(fields.FirstName.length > 30){
            messages.push('First Name should be less than 30 characters');
        }
        if(fields.MiddleName) {
            if(fields.MiddleName.length > 30){
                messages.push('Middle Name should be less than 30 characters');
            }
        }
        if(fields.LastName.length > 40){
            messages.push('Last Name should be less than 40 characters');
        }
        if(fields.FirstName == "'" || fields.LastName == "'"){
            messages.push('First/Last Name Cannot be only a Apostrophe');
        }
        if(fields.FirstName == "-" || fields.LastName == "-"){
            messages.push('First/Last Name Cannot be only a Hyphen');
        }
        if(fields.MiddleName == "'" ){
            messages.push('Middle Name Cannot be only a Apostrophe');
        }
        if(fields.MiddleName == "-" ){
            messages.push('Middle Name Cannot be only a Hyphen');
        }
        if(firstName == "and" || firstName == "or" || lastName == "and" || lastName == "or"){
            messages.push('The words And or OR cannot be part of the First/Last Name');
        }
        if(middleName == "and" || middleName == "or"){
            messages.push('The words And or OR cannot be part of the Middle Name');
        }
        
        if (firstName.includes(' estate') || firstName.includes(' estates') || firstName.includes(' trust') || firstName.includes('estate of ')) {
            messages.push('First Name: Trusts and Estates must be added as organizations');
        }
        if (middleName) {
            if (middleName.includes(' estate') || middleName.includes(' estates') || middleName.includes(' trust') || middleName.includes('estate of ')) {
                messages.push('Middle Name: Trusts and Estates must be added as organizations');
            }
        }
        if (lastName.includes(' estate') || lastName.includes(' estates') || lastName.includes(' trust') || lastName.includes('estate of ')) {
            messages.push('Last Name: Trusts and Estates must be added as organizations');
        }

        if(fields.FirstName ? !new RegExp('^[a-z A-Z\'-]+$').test(fields.FirstName): false){
            messages.push('First Name Cannot Contain a Number or Special Characters	');
        }
        if(fields.LastName ? !new RegExp('^[a-z A-Z\'-]*$').test(fields.LastName): false){
            messages.push('Last Name Cannot Contain a Number or Special Characters');
        }
        if(fields.MiddleName ? !new RegExp('^[a-z A-Z\'-]+$').test(fields.MiddleName): false){
            messages.push('Middle Name Cannot Contain a Number or Special Characters');
        }
        if(lastName == "nln"){
            messages.push('The Letters NLN are not valid entries for Last Name');
        }
        if(middleName == "nmi" || middleName == "nmn"){
            messages.push('The Letters NMI or NMN are not valid entries for Middle Name');
        }
        if(!fields.Phone && !fields.Email && !fields.Street){
            messages.push('At least 1 method of contact is required (email, complete address or phone number)');
        }
        if(fields.Street || fields.City || fields.PostalCode || fields.StateCode){
            if(!fields.Street || !fields.City || !fields.PostalCode || !fields.StateCode){
                messages.push('The address is incomplete, please review all the address fields');    
            }
        }
        
        //DOB validation
        var todaysDate = new Date();
        if(fields.Date_of_Birth__c){
            if(new Date(fields.Date_of_Birth__c) > todaysDate){
                messages.push('Date of Birth cannot be in the future');
            } 
            if(new Date(fields.Date_of_Birth__c) < new Date("12/31/1900")){
                messages.push('DOB of Must be greater than 1900');
            } 
        }
        
        //Phone validation
        if(fields.Phone != '' && fields.Phone != null){
            if(!new RegExp('^\\d{10}$').test(fields.Phone)){
                messages.push('Phone Number must contain 10 Numbers and only numbers are allowed');
            }
            if(fields.Phone.startsWith('999') ||fields.Phone.startsWith('000') || fields.Phone.endsWith('9999999')||fields.Phone.endsWith('0000000')){
                messages.push('Phone Number cannot contain a sequence of 0s or 9s');
            }
        }
        //Email Validation
        if(fields.Email ? !new RegExp('^[a-z A-Z 0-9\'-@/._&]+$').test(fields.Email) : false){
           messages.push('Email cannot contain special characters');
        }
        
        //Primary Language
        if(fields.Primary_Language__c ? (fields.Primary_Language__c == 'en-us' || fields.Primary_Language__c == 'enUS' || fields.Primary_Language__c == 'ENU' || fields.Primary_Language__c == 'en-US') : false){
           messages.push('Please update the Primary Language to English before saving');
        }

        //Phone and Email Usage Validation
        if(fields.Phone && !fields.Phone_Usage__c || fields.Email && !fields.Email_Usage__c ){
            if(!fields.Phone_Usage__c && !fields.Email_Usage__c && fields.Phone && fields.Email ){
                messages.push('Please specify a usage for both phone and email');
            }else if(!fields.Phone_Usage__c && fields.Phone){
                messages.push('Please specify a usage for this phone');
            }else if(!fields.Email_Usage__c && fields.Email){
                messages.push('Please specify a usage for this email');
            }
        }
        if(!fields.Phone && fields.Phone_Usage__c) {
            messages.push('Please specify a phone number as well as a usage');
        }
        if(!fields.Email && fields.Email_Usage__c) {
            messages.push('Please specify an email address as well as a usage');
        }
        
        if(!fields.Company && (fields.Phone_Usage__c ? fields.Phone_Usage__c.includes('BIZ') : false||
                               fields.Additional_Phone_1_Usage__c ? fields.Additional_Phone_1_Usage__c.includes('BIZ') : false ||
                               fields.Additional_Phone_2_Usage__c ? fields.Additional_Phone_2_Usage__c.includes('BIZ') : false || 
                               fields.Additional_Phone_3_Usage__c ? fields.Additional_Phone_3_Usage__c.includes('BIZ') : false || 
                               fields.Email_Usage__c ? fields.Email_Usage__c.includes('BIZ')  : false || 
                               fields.Additional_Email_1_Usage__c ? fields.Additional_Email_1_Usage__c.includes('BIZ') : false||
                               fields.Additional_Email_2_Usage__c ? fields.Additional_Email_2_Usage__c.includes('BIZ') : false)){
            
            messages.push('Business is not a valid usage when lead is a person');
        }
        
        
        if(fields.Company){
            
            //Home usage on Business Phone & Email
            if(fields.Phone_Usage__c ? fields.Phone_Usage__c.includes('HOME') : false||
                               fields.Additional_Phone_1_Usage__c ? fields.Additional_Phone_1_Usage__c.includes('HOME') : false ||
                               fields.Additional_Phone_2_Usage__c ? fields.Additional_Phone_2_Usage__c.includes('HOME') : false || 
                               fields.Additional_Phone_3_Usage__c ? fields.Additional_Phone_3_Usage__c.includes('HOME') : false || 
                               fields.Email_Usage__c ? fields.Email_Usage__c.includes('HOME')  : false || 
                               fields.Additional_Email_1_Usage__c ? fields.Additional_Email_1_Usage__c.includes('HOME') : false||
                               fields.Additional_Email_2_Usage__c ? fields.Additional_Email_2_Usage__c.includes('HOME') : false){
                
                messages.push('Home is not a valid usage when lead is a business');
            }
            //Emergency usage on Business Phone
            if(fields.Phone_Usage__c ? fields.Phone_Usage__c.includes('EMRGNCY') : false||
                               fields.Additional_Phone_1_Usage__c ? fields.Additional_Phone_1_Usage__c.includes('EMRGNCY') : false ||
                               fields.Additional_Phone_2_Usage__c ? fields.Additional_Phone_2_Usage__c.includes('EMRGNCY') : false || 
                               fields.Additional_Phone_3_Usage__c ? fields.Additional_Phone_3_Usage__c.includes('EMRGNCY') : false){
                
                messages.push('Emergency is not a valid usage when lead is a business');
            }
            //School usage on Business Email
            if(fields.Email_Usage__c ? fields.Email_Usage__c.includes('SCHL')  : false || 
                               fields.Additional_Email_1_Usage__c ? fields.Additional_Email_1_Usage__c.includes('SCHL') : false||
                               fields.Additional_Email_2_Usage__c ? fields.Additional_Email_2_Usage__c.includes('SCHL') : false){
                
                messages.push('School is not a valid usage when lead is a business');
            }
        }
        //Country Validation (BLOCK INTERNATIONAL ADDRESSES FOR R1)
        if (fields.CountryCode ? fields.CountryCode != 'US' : false) {
            messages.push('International addresses are not allowed at this time');
        } 
        //City Validation
        if( fields.City ? ((!new RegExp('^[a-z A-Z-/#&]+$').test(fields.City) || fields.City.length < 2) && fields.City) : false){
            messages.push('City must have at-least 2 Characters and special characters allowed in this field are -/#&');
        }
        //Zip Validation
        if(fields.PostalCode ? (!new RegExp('^[0-9]*$').test(fields.PostalCode) || fields.PostalCode.length > 5) : false){
            messages.push('ZIP Can only contain numbers and a maximum of 5 numbers');
        }
        //DL Validation
        if(fields.Drivers_License_Issue_Year__c != null ? fields.Drivers_License_Issue_Year__c - new Date(fields.Date_of_Birth__c).getFullYear() <12: false){
            messages.push('Drivers License must greater than DOB by 12 Years');
        }
        if (fields.Drivers_License_Issue_Year__c && !fields.Drivers_License_Issue_Month__c) {
            messages.push('Must have both License Issue Month and License Issue Year entered');
        }  
        if (!fields.Drivers_License_Issue_Year__c&& fields.Drivers_License_Issue_Month__c) {
            messages.push('Must have both License Issue Month and License Issue Year entered');
        }  	
        if(!fields.Drivers_License_Number__c && fields.Driver_s_License_State__c !==  'IT' && fields.Driver_s_License_State__c){
           messages.push('Driver\'s License Number is Required');
        }  
        
        //Contact Date
        if(fields.Next_Contact_Date__c ? fields.Next_Contact_Date__c <= fields.Last_Contact_Date__c : false){
           messages.push('Next Contact Date needs to be greater than the Last Contact Date');
        }
        
        console.log('messages' + messages);
        component.set("v.validationErrorMessages", messages);
    },
    
    redirectToAccountRecord : function(component, recordId) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": recordId,
                    "objectApiName": "Account",
                    "actionName": "view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId){
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.refreshTab({tabId : newTabId, includeAllSubtabs: true});
                workspaceAPI.focusTab(newTabId);
            });
        });
    },
    
    redirectToRecord : function(component, recordId) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": recordId,
                    "objectApiName": "Lead",
                    "actionName": "view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId){
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.refreshTab({tabId : newTabId, includeAllSubtabs: true});
                workspaceAPI.focusTab(newTabId);
            });
        });
    },
    
    initYearOptions : function(component, helper) {
        var yearOptions = [];
        let currentYear = new Date().getFullYear();
        let earliestYear = 1912;
        yearOptions.push({ id: null, label: '-- None --'})
        while (currentYear >= earliestYear) {
            let dateOption = {};
            dateOption.id = currentYear;
            dateOption.label = currentYear;
            yearOptions.push(dateOption);
            currentYear -= 1;
        }
        component.set("v.yearOptions",yearOptions);
    },

    scrollToTop : function() {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }
})