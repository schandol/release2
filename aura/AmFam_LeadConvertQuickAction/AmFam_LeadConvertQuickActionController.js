({
    doInit: function(component, event, helper) {

        console.log('lead id: ' + component.get("v.recordId"));
        helper.getInitialData(component, event, helper);
        helper.initDataTableColumns(component, helper);
        console.log('### afet doInit');

    },
    
    convert: function(component, event, helper) {
      helper.convert(component, event, helper);
    },
    
    close: function(component, event, helper) {
        helper.close(component, event, helper);
    },
    
    handleRowAction: function (component, event, helper) {
        component.set("v.isLoading",true);
        var row = event.getParam('row');
        var selectedHousehold = JSON.stringify(row);
        component.set('v.selectedHousehold', selectedHousehold);
        console.log('selectedHousehold' + selectedHousehold);
        helper.convert(component, event, helper);
    },
    
    forceCreateNewHousehold : function(component, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.applyHouseholdMatch",false);
        helper.convert(component, event, helper);
    },

    addressRowSelected: function(component, event, helper) {
        component.set("v.selectedAddress", null);
        component.set("v.selectedAddress", event.getParam('selectedRows'));
    },
    
    additionaladdress1RowSelected: function(component, event, helper) {
        component.set("v.selectedAdditionalAddress1", null);
        component.set("v.selectedAdditionalAddress1", event.getParam('selectedRows'));
    },
    
    additionaladdress2RowSelected: function(component, event, helper) {
        component.set("v.selectedAdditionalAddress2", null);
        component.set("v.selectedAdditionalAddress2", event.getParam('selectedRows'));
    },

    addressRowToSend: function(component, event, helper) {
        component.set("v.isLoading",true);
        var message = '';
        var selectedAddressList = [];
        
        if(component.get("v.selectedAddress").length == 0 && component.get("v.code1ReturnedAddress").length > 0){
        	message = 'Please select one address from Primary Address';    
        }else{
            selectedAddressList.push(component.get("v.selectedAddress")[0]);  
        }
        
        if(component.get("v.returnedCode1AdditionalAddresses1")){
            if(component.get("v.selectedAdditionalAddress1").length == 0 && component.get("v.returnedCode1AdditionalAddresses1").length > 0){
                if(message != ''){
                    message = 'Please select one address from each section';    
                }else{
                    message = 'Please select one address from Address 2';    
                }    
            }else{
                selectedAddressList.push(component.get("v.selectedAdditionalAddress1")[0]);  
            }    
        }
        
        if(component.get("v.returnedCode1AdditionalAddresses2")){
            if(component.get("v.selectedAdditionalAddress2").length == 0 && component.get("v.returnedCode1AdditionalAddresses2").length > 0){
                if(message != ''){
                    message = 'Please select one address from each section';    
                }else{
                    message = 'Please select one address from Address 3';    
                }    
            }else{
                selectedAddressList.push(component.get("v.selectedAdditionalAddress2")[0]);  
            }     
        }

        
        
        if(selectedAddressList.length > 0 && message == ''){
            console.log('AddressList' + JSON.stringify(selectedAddressList));
        	helper.select_address(component, JSON.stringify(selectedAddressList)); 	    
        }else if (message != '') {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": message,
                "mode": "dismissible",
                "duration": "20000"
            });
            toastEvent.fire();
            component.set("v.isLoading", false);
        }
    },
    
    matchRowSelected: function(component, event, helper) {
        component.set("v.isLoading",true);
        var row = event.getParam('row');
        component.set("v.cdhid", row.cdhId);
        if(!component.get("v.cdhid")){
    	    var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": 'An unexpected error occurred, Please try again',
                "mode": "dismissible",
                "duration": "20000"
            });
            toastEvent.fire();
        }else{
            helper.convert(component, event, helper);
        }
    },

    handleChange: function(component, event, helper) {

    }
})