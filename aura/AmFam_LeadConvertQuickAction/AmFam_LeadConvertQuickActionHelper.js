({
    getInitialData: function(component, event, helper) {

        this.toggleSpinner(component, event);

        var leadIdValue = component.get("v.recordId");
        component.set("v.createNewButton", false);

        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.getConvertOptionsForLead");
        action.setParams({
            "leadId": leadIdValue
        });



        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {

            this.toggleSpinner(component, event);
            component.set("v.isLoading",false);
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server

                var leadWrapper = response.getReturnValue();
                console.log('converted AccountId' + leadWrapper.convertedAccountId);
                if(leadWrapper.convertedAccountId != null){
                    
                        //Show Success message to the user
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type":"success",
                            "title": "Success!",
                            "message": "The lead has been converted into an existing account."
                        });
                        toastEvent.fire();
                    
                        this.close(component, event, helper);
                        //Redirect the page to the existing Account
                        this.redirectToAccountRecord(component, leadWrapper.convertedAccountId);
                 }
                else{
                	component.set("v.accounts", leadWrapper.accounts);
                    component.set("v.contacts", leadWrapper.contacts);
                    component.set("v.currentLead", leadWrapper.currentLead);
                    component.set("v.isBusinessLead", leadWrapper.isBusinessLead);
                    component.set("v.createNewAccount", leadWrapper.createNewAccount);
    
                    component.set("v.isConverted", leadWrapper.isConverted);
                    component.set("v.disableConvert", leadWrapper.isConverted);
                    component.set("v.convertedAccount", leadWrapper.convertedAccount);
    
                    var accountMapKeys = []; 
                        
    
                        if (leadWrapper.accounts != null) {
                            for (var i=0; i < leadWrapper.accounts.length; i++) {
                                var acc = leadWrapper.accounts[i];
                                accountMapKeys.push({'label': acc.Name,'value':acc.Id});
                            }
                        }
    
    
                    component.set("v.accountOptions", accountMapKeys);
                 }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);


                        component.set("v.hasErrors", true);
                        component.set("v.disableConvert", true);
                        component.set("v.errorMessage", errors[0].message);

                        /*
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                        */

                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
    convert: function(component, event, helper) {

        this.toggleSpinner(component, event);
        component.set("v.code1ReturnedAddress", null);
        component.set("v.returnedCode1AdditionalAddresses1", null);
        component.set("v.returnedCode1AdditionalAddresses2", null);
        component.set("v.createNewButton", false);
        var leadIdValue = component.get("v.recordId");
        var cdhid = component.get("v.cdhid");
        var createNewAccount = component.get("v.createNewAccount");

        var currentLead = component.get("v.currentLead");

        var firstName = currentLead.FirstName;
        var lastName = currentLead.LastName;

        var selectedAddress = null;
        if(component.get("v.selectedAddress") && component.get("v.selectedAddress").length >0){
            selectedAddress = JSON.stringify(component.get("v.selectedAddress"));
        }
        
        var selectedAdditionalAddress1 = null;
        if(component.get("v.selectedAdditionalAddress1") && component.get("v.selectedAdditionalAddress1").length >0){
            selectedAdditionalAddress1 = JSON.stringify(component.get("v.selectedAdditionalAddress1"));
        }
        
        var selectedAdditionalAddress2 = null;
        if(component.get("v.selectedAdditionalAddress2") && component.get("v.selectedAdditionalAddress2").length >0){
            selectedAdditionalAddress2 = JSON.stringify(component.get("v.selectedAdditionalAddress2"));
        }
        
        var applyPartyMatch = true;
        var applyHouseholdMatch = component.get("v.applyHouseholdMatch");
        if(JSON.stringify(component.get("v.returnedPartyMatches")).includes('POSSIBLE')){
            applyPartyMatch = false;
        }
        if(JSON.stringify(component.get("v.returnedPartyMatches")).includes('EXACT')){
            applyHouseholdMatch = false;
        }
        var selectedHousehold = null; 
        selectedHousehold = component.get("v.selectedHousehold");
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        var action = component.get("c.convertLead");
        action.setParams({
            "leadId": leadIdValue,
            "cdhid": cdhid,
            "firstName": firstName,
            "lastName": lastName,
            "code1Complete": component.get("v.code1Complete"),
            "selectedAddress": selectedAddress,
            "selectedAdditionalAddress1": selectedAdditionalAddress1,
            "selectedAdditionalAddress2": selectedAdditionalAddress2,
            "applyPartyMatch" : applyPartyMatch,
            "applyHouseholdMatch" : applyHouseholdMatch,
            "selectedHousehold" : selectedHousehold
        });


        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {

            this.toggleSpinner(component, event);
            var state = response.getState();
            component.set("v.isLoading",false);
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server

                var resultValue = response.getReturnValue();

                console.log(JSON.parse(JSON.stringify(resultValue)));
                if (resultValue != null) {

                    var convertedAccountId = resultValue.convertedAccountId;
                    var possibleAccounts = resultValue.possibleAccounts;
                    if (convertedAccountId != null) {
                        //******************************** */
                        //Lead was promoted and converted

                        this.close(component, event, helper);

                        //Show Success message to the user
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type":"success",
                            "title": "Success!",
                            "message": "The lead was converted successfully."
                        });
                        toastEvent.fire();

                        //Redirect the page to the newly created Account
                        this.redirectToAccountRecord(component, convertedAccountId);
                        
                    } else if (possibleAccounts != null && possibleAccounts.length > 0) {
                        component.set("v.createNewButton", true);
                        component.set("v.createNewAccount", false);
                        component.set("v.returnedPartyMatches", possibleAccounts);
                        if(JSON.stringify(possibleAccounts).includes('EXACT')){
                            component.set("v.disableConvert", true);
                        }
                    } else if (resultValue.returnedCode1Addresses != null) {
                          component.set("v.code1ReturnedAddress", resultValue.returnedCode1Addresses.Code1Wrapper.AddressesReturnedFromCode1);
                    }
                    
                    if(resultValue.addressPurposeAndUsages != null){
                        if(JSON.stringify(resultValue.addressPurposeAndUsages).includes('address1')){
                        	component.set("v.address1PurposeAndUsages", resultValue.addressPurposeAndUsages.address1);
                        }
                        if(JSON.stringify(resultValue.addressPurposeAndUsages).includes('address2')){
                        	component.set("v.address2PurposeAndUsages", resultValue.addressPurposeAndUsages.address2);    
                        }
                        if(JSON.stringify(resultValue.addressPurposeAndUsages).includes('address3')){
                            component.set("v.address3PurposeAndUsages", resultValue.addressPurposeAndUsages.address3); 
                        }
                    }
                    
                    if (resultValue.returnedCode1AdditionalAddresses1 != null) {
                          component.set("v.returnedCode1AdditionalAddresses1", resultValue.returnedCode1AdditionalAddresses1.Code1Wrapper.AddressesReturnedFromCode1);
                     }                  
                    
                    if (resultValue.returnedCode1AdditionalAddresses2 != null) {
                          component.set("v.returnedCode1AdditionalAddresses2", resultValue.returnedCode1AdditionalAddresses2.Code1Wrapper.AddressesReturnedFromCode1);
                    }
                    
                    if(resultValue.householdMatches != null && resultValue.householdMatches.length > 0){
                        component.set("v.householdMatches", resultValue.householdMatches);  
                    }
                    //Handle error messages
                    var errorMessages = resultValue.errorMessages;
                    if (errorMessages != null && errorMessages.length > 0) {
                        var message = errorMessages[0];
                        component.set("v.errorMessage",message);
                        component.set("v.hasErrors",true);
                    }
                }

            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: [" +
                            errors[0].message) + "]";
        
                        component.set("v.disableConvert", true);
                        component.set("v.hasErrors", true);
                        component.set("v.errorMessage", errors[0].message);
                        }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.




        $A.enqueueAction(action);
    },
    
    select_address: function(component, selectedAddress) {
        
        console.log('inside Promote Lead after address select');
        
        var action = component.get("c.updateLeadRecord");
        
        var leadRec = component.get("v.currentLead");
        console.log("leadRec" + leadRec);
        console.log("leadId" + component.get("v.recordId"));
        
        var actionParams = {
            "leadRec" : leadRec,
            "selectedAddress":selectedAddress,
            "leadId":component.get("v.recordId")
         };

        action.setParams(actionParams);

        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var resp = response.getReturnValue();
                component.set("v.code1Complete", true);
                component.set("v.currentLead", resp);
                this.convert(component, event);
            }else if(state == "ERROR"){
                  var errors = response.getError();
                  this.scrollToTop();
                  component.set("v.isLoading",false);
                  component.set("v.errorMessage", errors[0].message);
            }
        });

        $A.enqueueAction(action);
    },

    initDataTableColumns: function(component, helper) {
        component.set('v.addressMatchColumns',[
            {label: 'Address Type', fieldName: 'Code1ReturnIndicator', type: 'text', wrapText: true, iconName: 'standard:marketing_actions'}, 
            {label: 'Street', fieldName: 'addressLine1', type: 'text', iconName: 'standard:visits'},
            //{label: 'Address Line 2', fieldName: 'addressLine2', type: 'text'},
            {label: 'City', fieldName: 'city', type: 'text', iconName: 'standard:service_territory'},
            {label: 'State', fieldName: 'stateCode', type: 'text', iconName: 'standard:location_permit'},
            {label: 'Zip', fieldName: 'zip5Code', type: 'text', iconName: 'standard:location'},
            {label: 'Confirmed Delivery Point?', fieldName: 'deliveryPointValidationMatchIndicator', type: 'text', wrapText: true, iconName: 'standard:home'},
            {label: 'Commercial Mail Receiving Agency?', fieldName: 'deliveryPointValidationCommercialMailreceivingCode', type: 'text', wrapText: true, iconName: 'standard:store'}
            //{label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Address', name: 'select_address', title: 'Click to select this address', iconName: 'standard:address', iconPosition: 'left' }}
        ]);
        
        component.set('v.partyMatchColumns',[
            {label: 'Match Type', fieldName: 'matchType', type: 'text'}, 
            {label: 'Name', fieldName: 'fullName', type: 'text', wrapText: true}, 
            {label: 'Address', fieldName: 'address', type: 'text', wrapText: true},
            {label: 'Age', fieldName: 'age', type: 'text'},
            {label: 'Driver\'s License' , fieldName: 'dlicense', type: 'text'},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_PersonAccount_match', title: 'Click to select this match',variant: 'brand'}}
        ]);
            
       component.set('v.partyMatchColumnsForBusiness',[
            {label: 'Match Type', fieldName: 'matchType', type: 'text'}, 
            {label: 'Name', fieldName: 'fullName', type: 'text', wrapText: true}, 
            {label: 'Address', fieldName: 'address', type: 'text', wrapText: true},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Match', name: 'select_BusinessAccount_match', title: 'Click to select this match',variant: 'brand'}}
        ]);
        
      component.set('v.householdMatchColumns',[
            {label: 'Household Name', fieldName: 'householdName', type: 'text'},
            {label: 'Household Greeting Name', fieldName: 'householdGreetingName', type: 'text'},
            {label: 'Household Members', fieldName: 'additionalHouseholdMembers', type: 'text', wrapText: true},
            {label: 'Action', type: 'button', initialWidth: 135, typeAttributes: { label: 'Select This Household', name: 'select_household_match', title: 'Click to select this household',variant: 'brand'}}
        ]);
    },
    
    redirectToAccountRecord : function(component, recordId) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            pageReference: {
                "type": "standard__recordPage",
                "attributes": {
                    "recordId": recordId,
                    "objectApiName": "Account",
                    "actionName": "view"
                }
            },
            focus: true
        }).then(function(newTabId){
            workspaceAPI.getEnclosingTabId()
            .then(function(enclosingTabId){
                workspaceAPI.closeTab({tabId : enclosingTabId});
                workspaceAPI.refreshTab({tabId : newTabId, includeAllSubtabs: true});
                workspaceAPI.focusTab(newTabId);
            });
        });
    },
    
    close: function(component, event, helper) {
        // Close the action panel 
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    toggleSpinner: function(component, event) {
        var spinner = component.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    }
})