({
	init: function (cmp, event, helper) {

		cmp.set("v.countryOptions", helper.getCountryOptions());
		
		helper.getAddressTypes(cmp);

    },

	saveRecord: function (cmp, event, helper) {

        var address = cmp.find("newaddress");
        var isValid = address.checkValidity();

		console.log('### address: ' + address);
        if(isValid) {
			console.log('### before helper call');
			helper.saveToServer(cmp, event);
			console.log('### after helper call');
        }
        else {
            address.showHelpMessageIfInvalid();
        }
    }
})