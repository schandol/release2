({
    getURLFor : function(selectedDropdownValue, urlMap) {
        var foundURLInMap = false;
        var lookUpKey;
        var url;

        // selected drop down value is the option value from the screen.
        // since Homeowners and Personal auto use different values that 
        // are part of overall url, we change lookup key here to only
        // get value out of the map.
        console.log('Selected Dropdown Value: ' + selectedDropdownValue);
        if (selectedDropdownValue === 'Life_Cornerstone'){
            lookUpKey = selectedDropdownValue;
        } else {
            lookUpKey = 'Sales_Portal';
        }
        url = urlMap[lookUpKey];
        console.log('Base URL is: ' + url);

        return url;

    }
})