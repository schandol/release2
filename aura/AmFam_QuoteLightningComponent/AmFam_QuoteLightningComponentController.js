({
    myAction : function(component, event, helper) {

        var quoteList = component.get("c.getRelatedList");
        quoteList.setParams
        ({
            recordId: component.get("v.recordId")
        });

        quoteList.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var events = response.getReturnValue();
                if (response.getReturnValue()!= null) {
                    for(var i = 0; i < events.length; i++)
                {
                    if(events[i].typeOfProductCode == "POLICY") {
                        var formatPolicyNumner = events[i].sourceSystemProductIdentifier;
                        events[i].sourceSystemProductIdentifier = formatPolicyNumner.substring(0, 5) + '-' + formatPolicyNumner.substring(5, 10) + '-' + formatPolicyNumner.substring(10, 12);
                    }
                }
                component.set("v.QuoteList", response.getReturnValue());
                } else {
                    component.set("v.QuoteList", null);
                }
            }
        });

        var salesPortalLinks = component.get('c.getSalesPortalLinks');
        salesPortalLinks.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var portalLinks = response.getReturnValue();
                var listLength = portalLinks.length;
                for (var i = 0; i < listLength; i++) {
                    if (portalLinks[i].DeveloperName == 'Quote')
                        component.set('v.quoteLink', portalLinks[i].URL__c);
                    if (portalLinks[i].DeveloperName == 'Policy')
                        component.set('v.policyLink', portalLinks[i].URL__c);
                }
            }
        });

        var curDateTime = new Date();
        component.set('v.currentDateTime', curDateTime);

        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        component.set('v.today', today);

        $A.enqueueAction(quoteList);
        $A.enqueueAction(salesPortalLinks);
    },

    refreshView : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
   })