({
	doInit : function(component, event, helper) {
        
		var pageReference = component.get("v.pageReference");
        var policyId=component.get("v.recordId")===undefined?pageReference.state.c__recordId:component.get("v.recordId");
        var billingAccountNumber = component.get("v.recordId")===undefined? pageReference.state.c__billingAccount =='undefined'?null:pageReference.state.c__billingAccount: null;
        var action = component.get("c.getSourceSystemURL");
        action.setParams({
        	"policyId" : policyId,
            "billingAccount" : billingAccountNumber
        });
        
        
        action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
            var result = response.getReturnValue();
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": result
            });
            urlEvent.fire();
            
            if(pageReference !== null){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": '/'+policyId
            });
            urlEvent.fire();
            }
            
            
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"error",
                "title": "Error",
                "message": "Something went wrong, please contact your adminstrator!"
            });
            toastEvent.fire();
            
            if(pageReference !== null){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": '/'+policyId
            });
            urlEvent.fire();
            }
        }
            
        $A.get("e.force:closeQuickAction").fire();
        });
       $A.enqueueAction(action);
	}
})