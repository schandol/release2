({
    init: function(component, event, helper) {
        component.set("v.isLoading",true);
        helper.getEditAccess(component, helper);
        helper.getExistingRecordTypeId(component, helper);
        helper.initDataTableColumns(component, helper);
        helper.initPrepopulateAgencyForPA(component, helper);
        helper.initYearOptions(component, helper);
        console.log(JSON.parse(JSON.stringify(component.get("v.pageReference").state)));
    },
    
    onPageReferenceChanged: function(cmp, event, helper) {
        $A.get('e.force:refreshView').fire();
    },

    swapFormVisibility : function(component, event, helper) {
        $A.util.toggleClass(component.find('contactForm'),'slds-hide');
        $A.util.toggleClass(component.find('addressForm'),'slds-hide');
    },

    handlePersonAccountSubmit : function(component, event, helper) {
        console.log('in handlePersonAccountSubmit');
        component.set("v.isLoading",true);
        component.set("v.validationErrorMessages",[]);
        component.set("v.errors",[]);
        var isEdit = component.get("v.pageReference").attributes.actionName == 'edit';
        
        event.preventDefault();
        
        var fields = event.getParam('fields');

        if (!$A.util.hasClass(component.find('dlInfo'),'slds-hide')) {
            fields.Drivers_License_Issue_Year__c = component.find('dlYear').get('v.value');
        }
        if (component.get("v.gender")) {
            fields.FinServ__Gender__pc = component.get("v.gender");
        }

        // Validate the form
        helper.validateForm(component, event, helper, false, isEdit);
        
        if (component.get("v.validationErrorMessages").length == 0) {
            if (isEdit) {
                console.log('in edit submit sequence');
                helper.editPersonAccount(component, event, fields);
            } else {
                component.find('recordForm').submit(fields);
            }
        } else {
            component.set("v.isLoading",false);
            helper.scrollToTop();
        }
    },

    handlePersonAccountSuccess : function(component, event, helper) {
        console.log('success');
        var payload = event.getParams().response;
        helper.sendAccountToCDH(component, payload.id);
    },

    createNewPersonAccount : function(component, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.applyPartyMatch",false);
        helper.createPersonAccountRecord(component);
    },

    forceCreateNewHousehold : function(component, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.applyHouseholdMatch",false);
        helper.createPersonAccountRecord(component);
    },

    handleBusinessAccountSubmit : function(component, event, helper) {
        var isEdit = component.get("v.pageReference").attributes.actionName == 'edit';
        component.set("v.isLoading",true);
        component.set("v.validationErrorMessages",[]);
        component.set("v.errors",[]);
        event.preventDefault();
        var fields = event.getParam('fields');

        helper.validateForm(component, event, helper, true, isEdit);
      
        if(component.get("v.validationErrorMessages").length == 0){
            if (fields.Taxpayer_ID_Type__c == 'FEIN') {
                fields.SSN__c = null;
            } else {
                fields.FEIN__c = null;
            }
            if (fields.Form_of_Business__c == 'JNTVNTR') {
                fields.Other_Comment__c = null;
            } else if (fields.Form_of_Business__c == 'OTHER') {
                fields.Joint_Venture_Comment__c = null;
            }
            if (isEdit) {
                console.log('in edit business account submit sequence');
                helper.editBusinessAccount(component, event, fields);
            } else {
                component.find('businessAccountForm').submit(fields);
            }
        } else {
            component.set("v.isLoading",false);
            helper.scrollToTop();
         }
    },

    handleBusinessAccountSuccess : function(component, event, helper) {
        console.log('handle business account success');
        var payload = event.getParams().response;
        helper.sendBusinessAccountToCDH(component, payload.id);
    },

    createNewBusinessAccount : function(compoment, event, helper) {
        component.set("v.isLoading",true);
        component.set("v.applyPartyMatch",false);
        helper.createBusinessAccountRecord(component);
    },

    handleOnLoad: function(component, event, helper) {
        var rec = event.getParam('recordUi');
        var isEdit = component.get("v.pageReference").attributes.actionName == 'edit';
        if ((component.get('v.isEditable') && isEdit) || !isEdit) {
            let primarySpokenLanaguage = component.find('primarySpokenLanguage');
            if (primarySpokenLanaguage && primarySpokenLanaguage.get('v.value') == 'OTHER') {
                $A.util.removeClass(component.find('primarySpokenLanguageSection'),'slds-hide');
                component.find('primarySpokenLanguageOther').set('v.required',true);
            }
            let secondarySpokenLanaguage = component.find('secondarySpokenLanguage');
            if (secondarySpokenLanaguage && secondarySpokenLanaguage.get('v.value') == 'OTHER') {
                $A.util.removeClass(component.find('secondarySpokenLanguageSection'),'slds-hide');
                component.find('secondarySpokenLanguageOther').set('v.required',true);
            }
            switch (rec.record.recordTypeInfo.name) {
                case 'Business':
                    if (component.get('v.taxpayerIdType') == '' && rec.record.fields.Taxpayer_ID_Type__c.value != null) {
                        component.set('v.taxpayerIdType',rec.record.fields.Taxpayer_ID_Type__c.value);
                    }
                    if (component.find("dissolvedIndicator")) {
                        let dissolvedInd = component.find("dissolvedIndicator").get("v.value");
                        component.set("v.dissolvedIndicator",dissolvedInd);
                    }
                    break;
                case 'Individual':
                    if(JSON.stringify(rec.record.fields).includes('Resident_State__pc')){
                        component.find('residentState').set('v.value',rec.record.fields.Resident_State__pc.value);
                        helper.initGenderOptions(component, helper);
                    }
                    if(JSON.stringify(rec).includes('FinServ__Gender__pc')){
                        // Timeout for rendering issue
                        window.setTimeout(
                            $A.getCallback( function() {
                                // Now set our preferred value
                                if (rec.record.fields.FinServ__Gender__pc.value == 'NB') {
                                    if (!JSON.stringify(component.get('v.genderOptions')).includes('NB')) {
                                        component.find('gender').set('v.required',true);
                                    } else {
                                        component.set('v.gender',rec.record.fields.FinServ__Gender__pc.value);
                                    }
                                } else {
                                    component.set('v.gender',rec.record.fields.FinServ__Gender__pc.value);
                                }
                            }
                        ));
                    }
                    if(JSON.stringify(rec).includes('Drivers_License_Issue_Year__c')){
                        component.find('dlYear').set('v.value',rec.record.fields.Drivers_License_Issue_Year__c.value);
                    }	
                    if (rec.record.fields.Driver_s_License_Number__c.value || rec.record.fields.Driver_s_License_State__c.value) {
                        $A.util.removeClass(component.find('dlInfo'),'slds-hide');
                    }
                    if (rec.record.fields.Occupation__pc.value) {
                        let ocs = component.find('otherCommentSection');
                        if (rec.record.fields.Occupation__pc.value.includes('OTH')) {
                            $A.util.removeClass(ocs,'slds-hide');
                            component.find('otherValue').set('v.required',true);
                        }
                    }
                    if (rec.record.fields.Professional_Suffix__pc.value) {
                        if (rec.record.fields.Professional_Suffix__pc.value == 'Other') {
                            $A.util.removeClass(component.find('professionalSuffixComment'),'slds-hide');
                            component.find('professionalSuffixComment').set('v.required',true);
                        }
                    }
                    break;
            }
            if (rec.record.fields.Last_4_SSN__c.value) {
                if (component.find("ssnField")) {
                    component.find("ssnField").set("v.value",'XXXXX'+rec.record.fields.Last_4_SSN__c.value);
                }
            }
            if (rec.record.fields.Last_4_FEIN__c.value) {
                if (component.find("feinField")) {
                    component.find("feinField").set("v.value",'XXXXX'+rec.record.fields.Last_4_FEIN__c.value);
                }
            }
        }
    },

    handleError : function(component, event, helper) {
        console.log('error');
        component.set("v.isLoading",false);
        var response = event.getParams();
        helper.scrollToTop();
    },

    showDLInfo : function(component, event, helper) {
        if (component.find('dlNumber').get("v.value") || component.find('dlState').get("v.value")) {
            $A.util.removeClass(component.find('dlInfo'),'slds-hide');
         }else if(!component.find('dlMonth').get("v.value") && !component.find('dlYear').get("v.value")){
            $A.util.addClass(component.find('dlInfo'),'slds-hide');
        }
    },

    showOccupationInfo : function(component, event, helper) {
        let ocs = component.find('otherCommentSection');
        if (component.find('occupation').get("v.value").includes('OTH')) {
            $A.util.removeClass(ocs,'slds-hide');
            component.find('otherValue').set('v.required',true);
        } else {
            $A.util.addClass(ocs,'slds-hide');
            component.find('otherValue').set('v.required',false);
            component.find('otherValue').set('v.value',null);
        }
    },

    formatSSN : function(component, event, helper) {
        var ssnField = component.find('ssnField');
        var val = ssnField.get("v.value").replace(/\D/g, '');
        val = val.replace(/^(\d{3})/, '$1-');
        val = val.replace(/-(\d{2})/, '-$1-');
        val = val.replace(/(\d)-(\d{4}).*/, '$1-$2');
        ssnField.set("v.value",val);
    },
   
    handleCancel : function(component, event, helper) {
        component.find('navService').navigate({
            "type": "standard__objectPage",
            "attributes": {
                "objectApiName": "Account",
                "actionName": "home"
            }
        });
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    },

    handleBack : function(component, event, helper) {
        component.set("v.errors",null);
        component.set("v.code1Results",null);
        component.set("v.residentialAddressList",null);
        component.set("v.mailingAddressList",null);
        component.set("v.selectedResidentialAddress",'');
        component.set("v.selectedMailingAddress",'');
        component.set("v.partyMatches",null);
        component.set("v.householdMatches",null);
        component.set("v.code1ResultValidation", false);
        component.set("v.validAddress", true);
        component.set("v.onlyOverrideResidential", false);
        component.set("v.onlyOverrideMailing", false);
        $A.util.removeClass(component.find('accountForm'),'slds-hide');
        if (component.get('v.account.Drivers_License_Issue_Year__c')) {
            component.find("dlYear").set("v.value", component.get('v.account.Drivers_License_Issue_Year__c'));
        }
    },

    handleRowAction: function (component, event, helper) {
        component.set("v.isLoading",true);
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'select_match':
                helper.select_match(component, row);
                break;
            case 'select_household_match':
                helper.select_household_match(component, row);
                break;
            case 'select_business_match':
                // SHOW CONFIRMATION MESSAGE
                component.set("v.showPartyMatchConfirmDialog",true);
                component.set("v.selectedRow",JSON.stringify(row));
                //helper.select_business_match(component, row);
                break;
        }
    },

    addressRowSelectedForResidential: function(component, event, helper) {
        component.set("v.selectedResidentialAddress", event.getParam('selectedRows'));
    },
    
    addressRowSelectedForMailing: function(component, event, helper) {
        component.set("v.selectedMailingAddress", event.getParam('selectedRows'));
        
    },
    
    addressRowsToSend: function(component, event, helper) {
        component.set("v.isLoading",true);
        var residentialRecord = [];
        var mailingRecord = [];
       
        if(component.get("v.selectedResidentialAddress") != ''){
            residentialRecord = component.get("v.selectedResidentialAddress");
        }else if(component.get("v.residentialAddressList") != null && component.get("v.residentialAddressList").length == 1 && !component.get("v.onlyOverrideResidential")){
            residentialRecord = component.get("v.residentialAddressList");
        }
        if(component.get("v.selectedMailingAddress") != ''){
            mailingRecord = component.get("v.selectedMailingAddress");
        }else if(component.get("v.mailingAddressList") != null && component.get("v.mailingAddressList").length == 1 && !component.get("v.onlyOverrideMailing")){
            mailingRecord = component.get("v.mailingAddressList");
        }
        
        var message = '';
        
        var residentialORBilling = component.get("v.isBusinessAccount")?'Billing':'Residential';
        
        if(component.find('addressCheckbox').get('v.checked')){
            if(residentialRecord.length > 0 ){
                if(component.get("v.isBusinessAccount")){
                    helper.select_business_address(component, JSON.stringify(residentialRecord), JSON.stringify(residentialRecord));
                }else{
                    helper.select_address(component, JSON.stringify(residentialRecord), JSON.stringify(residentialRecord));
                }
            }else{
                message = 'Please select one '+residentialORBilling+' address';
            }
        }else if(!component.find('addressCheckbox').get('v.checked')){
            if(residentialRecord.length > 0 && mailingRecord.length > 0){
                if(component.get("v.isBusinessAccount")){
                    helper.select_business_address(component, JSON.stringify(residentialRecord), JSON.stringify(mailingRecord));    
                }else{
                    helper.select_address(component, JSON.stringify(residentialRecord), JSON.stringify(mailingRecord));
                }
            }else{
                if(!residentialRecord.length > 0 && !mailingRecord.length > 0){
                    message = 'Please select one address from both '+residentialORBilling+' and Mailing';
                }else if(residentialRecord.length > 0 && !mailingRecord.length > 0){
                    message = 'Please select one address from Mailing';
                }else if(!residentialRecord.length > 0 && mailingRecord.length > 0){
                    message = 'Please select one address from  '+residentialORBilling;
                }
            }
         }
        
        if(message != ''){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type":"error",
                "title": "Error!",
                "message": message,
                "mode":"dismissible",
                "duration":"20000"
             });
            toastEvent.fire();
            component.set("v.isLoading",false);
        }else{
            component.set("v.selectedResidentialAddress", residentialRecord);
            if(!component.find('addressCheckbox').get('v.checked')){
              component.set("v.selectedMailingAddress", mailingRecord);
            }else{
              component.set("v.selectedMailingAddress", residentialRecord);
            }
        }
         
        
    },

    handleDefaultSuccess : function (component, event, helper) {
        console.log('in handle default success');
        var payload = event.getParams("id");
        helper.redirectToRecord(component, payload.id);
    },

    handleDenyBusinessMatchSelect : function(component, event, helper) {
        component.set("v.showPartyMatchConfirmDialog",false);
        component.set("v.isLoading",false);
    },

    handleConfirmBusinessMatchSelect : function(component, event, helper) {
        helper.select_business_match(component,component.get("v.selectedRow"));
    },

    onDissolvedChange : function(component, event, helper) {
        let dissolvedInd = component.find("dissolvedIndicator").get("v.value");
        component.set("v.dissolvedIndicator",dissolvedInd);
    },

    onPIRDateChange : function(component, event, helper) {
        let pirDate = component.find("pirDate").get("v.value");
        component.set("v.showPIROutcome",pirDate ? true : false);
    },

    updateGenderOptions : function(component, event, helper) {
        helper.initGenderOptions(component, helper);
        console.log(JSON.parse(JSON.stringify(component.get('v.account'))));
    },

    onBillingChange : function(component, event, helper) {
        helper.setResidentStateFromBillingState(component, event, helper);
    },

    onAddressCheckboxChange : function(component, event, helper) {
        let addrCheckbox = component.find('addressCheckbox');
        if (addrCheckbox) {
            let mailingAddressSection = component.find('mailingAddress');
            // Address Checkbox is checked
            if (addrCheckbox.get('v.checked')) {
                if (mailingAddressSection) {
                    $A.util.addClass(mailingAddressSection,'slds-hide');
                }
                component.find('mailAddr').set('v.required',false);
            } else { // Address checkbox is not checked
                if (mailingAddressSection) {
                    $A.util.removeClass(mailingAddressSection,'slds-hide');
                    component.find('mailAddr').set('v.required',true);
                }
            }
        }
    }, 

    onSalutationChange : function(component, event, helper) {
        let salutationValue = event.getSource().get("v.value");
        console.log(salutationValue);
    },

    onSpokenLanguageChange : function(component, event, helper) {
        let primarySpokenLanaguage = component.find('primarySpokenLanguage').get('v.value');
        if (primarySpokenLanaguage == 'OTHER') {
            $A.util.removeClass(component.find('primarySpokenLanguageSection'),'slds-hide');
            component.find('primarySpokenLanguageOther').set('v.required',true);
        } else {
            $A.util.addClass(component.find('primarySpokenLanguageSection'),'slds-hide');
            component.find('primarySpokenLanguageOther').set('v.required',false);
            component.find('primarySpokenLanguageOther').set('v.value',null);
        }
        let secondarySpokenLanaguage = component.find('secondarySpokenLanguage').get('v.value');
        if (secondarySpokenLanaguage == 'OTHER') {
            $A.util.removeClass(component.find('secondarySpokenLanguageSection'),'slds-hide');
            component.find('secondarySpokenLanguageOther').set('v.required',true);
        } else {
            $A.util.addClass(component.find('secondarySpokenLanguageSection'),'slds-hide');
            component.find('secondarySpokenLanguageOther').set('v.required',false);
            component.find('secondarySpokenLanguageOther').set('v.value',null);
        }
    },

    onFormOfBusinessChange : function(component, event, helper) {
        let formOfBusiness = event.getSource().get("v.value");
        if (formOfBusiness == 'JNTVNTR') {
            $A.util.removeClass(component.find('jointVentureSection'),'slds-hide');
            if (!$A.util.hasClass(component.find('otherCommentSection'),'slds-hide')) {
                $A.util.addClass(component.find('otherCommentSection'),'slds-hide');
            }
            component.find('otherCommentField').set('v.required',false);
        } else if (formOfBusiness == 'OTHER') {
            if (!$A.util.hasClass(component.find('jointVentureSection'),'slds-hide')) {
                $A.util.addClass(component.find('jointVentureSection'),'slds-hide');
            }
            $A.util.removeClass(component.find('otherCommentSection'),'slds-hide');
            component.find('otherCommentField').set('v.required',true);
        } else {
            if (!$A.util.hasClass(component.find('otherCommentSection'),'slds-hide')) {
                $A.util.addClass(component.find('otherCommentSection'),'slds-hide');
            }
            component.find('otherCommentField').set('v.required',false);
            if (!$A.util.hasClass(component.find('jointVentureSection'),'slds-hide')) {
                $A.util.addClass(component.find('jointVentureSection'),'slds-hide');
            }
        }
    },

    onProfessionalSuffixChange : function(component, event, helper) {
        let professionalSuffixValue = event.getSource().get("v.value");
        if (professionalSuffixValue == 'Other') {
            $A.util.removeClass(component.find('professionalSuffixComment'),'slds-hide');
            component.find('professionalSuffixComment').set('v.required',true);
        } else {
            $A.util.addClass(component.find('professionalSuffixComment'),'slds-hide');
            component.find('professionalSuffixComment').set('v.required',false);
            component.find('professionalSuffixComment').set('v.value',null);
        }
    },

    closeFocusedTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})