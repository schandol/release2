trigger AmFam_ParticipantTrigger on Participant__c (after insert, after delete) {
   //Activate and Deactivate trigger using Custom Metadata
   List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt WHERE DeveloperName = 'Participant_Object' LIMIT 1];

   Boolean trigActive = true;
   if (triggerSwitch.size() > 0) {
       trigActive = triggerSwitch[0].isActive__c;
       if (!trigActive) {
           return;
       }
   }


   AmFam_ParticipantTriggerHandler handler = new AmFam_ParticipantTriggerHandler();
   if (Trigger.isInsert) {
       if (Trigger.isBefore) {
           //handler.OnBeforeInsert(trigger.New);
       } else {
           handler.OnAfterInsert(Trigger.New);
       }
   } else if (Trigger.isDelete) {
        if (Trigger.isBefore) {
            //handler.OnBeforeInsert(trigger.New);
        } else {
            handler.OnAfterDelete(Trigger.Old, Trigger.OldMap);
        }

    
   }
   /* 
   else if (Trigger.isUpdate) {
       if (Trigger.isBefore) {
           handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
       } else {
           handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
       }
   }
   */
}