trigger AmFam_AccountTrigger on Account (before insert, before update, after insert, after update) {

    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'Account_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }



    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_AccountTriggerHandler handler = new AmFam_AccountTriggerHandler();
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            handler.OnBeforeInsert(Trigger.New);
        } else {
            handler.OnAfterInsert(Trigger.New, Trigger.newMap);
        }
    }  else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            handler.OnBeforeUpdate(Trigger.New ,Trigger.Old,Trigger.NewMap,Trigger.OldMap);
        } /*else {
            handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }*/
    }
    
    
}