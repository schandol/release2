trigger AmFam_ProducerAccessTrigger on ProducerAccess__c (after insert, after delete, after update) {

    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'ProducerAccess_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }


    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_ProducerAccessTriggerHandler handler = new AmFam_ProducerAccessTriggerHandler();

    if (Trigger.isInsert) {
        if (Trigger.isAfter) {
            handler.OnAfterInsert(trigger.New);
        }
    } if (Trigger.isUpdate) {
        if (Trigger.isAfter) {
            handler.OnAfterUpdate(trigger.New, Trigger.oldMap);
        }
    } else if (Trigger.isDelete) {
        if (Trigger.isAfter) {
            handler.OnAfterDelete(trigger.Old,Trigger.OldMap);
        }
     }



}