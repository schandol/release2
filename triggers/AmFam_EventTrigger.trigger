trigger AmFam_EventTrigger on Event (before insert, before update) {
    
    
    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'Event_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }
    
    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_EventTriggerHandler handler = new AmFam_EventTriggerHandler();
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            handler.OnBeforeInsert(trigger.New);
        } else {
            //handler.OnAfterInsert(trigger.New);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        } else {
            //handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }

        

}