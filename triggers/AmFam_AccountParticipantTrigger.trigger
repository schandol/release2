trigger AmFam_AccountParticipantTrigger on AccountParticipant (after insert, after delete) {

    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'AccountParticipant_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }



    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_AccountParticipantTriggerHandler handler = new AmFam_AccountParticipantTriggerHandler();
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            //handler.OnBeforeInsert(trigger.New);
        } else {
            //DISABLED UNTIL NEW PRODUCER ACCESS Grant gets updated
            //handler.OnAfterInsert(trigger.New);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            //handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        } else {
            //handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    } else if (Trigger.isDelete) {
        if (Trigger.isBefore) {
            //handler.OnBeforeDelete(trigger.Old, Trigger.OldMap);
        } else {
            //DISABLED UNTIL NEW PRODUCER ACCESS Grant gets updated
            //handler.OnAfterDelete(trigger.Old, Trigger.OldMap);
        }
    }


}