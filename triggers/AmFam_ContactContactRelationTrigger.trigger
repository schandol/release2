trigger AmFam_ContactContactRelationTrigger on FinServ__ContactContactRelation__c(after insert, after delete) {

    //********************************************************
    //Activate and Deactivate trigger using Custom Metadata
    //********************************************************

    List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                WHERE DeveloperName = 'ContactContactRelation_Object' LIMIT 1];

    Boolean trigActive = true;
    if (triggerSwitch.size() > 0) {
        trigActive = triggerSwitch[0].isActive__c;
        if (!trigActive) {
            return;
        }
    }


    //********************************************************
    //Dispatch Trigger excution 
    //********************************************************

    AmFam_ContactContactRelationshipHandler handler = new AmFam_ContactContactRelationshipHandler();
    if (Trigger.isInsert) {
        if (Trigger.isBefore) {
            //handler.OnBeforeInsert(trigger.New);
        } else {
            handler.OnAfterInsert(trigger.New, Trigger.NewMap);
        }
    } else if (Trigger.isUpdate) {
        if (Trigger.isBefore) {
            //handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        } else {
            //handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    } else if (Trigger.isDelete) {
        if (Trigger.isBefore) {
            //handler.OnBeforeDelete(trigger.Old,Trigger.OldMap);
        } else {
             System.debug('### trigger after delete');
            //DISABLED UNTIL NEW PRODUCER ACCESS GRANT get updated
            //handler.OnAfterDelete(trigger.Old,Trigger.OldMap);
        }
     }
    
}