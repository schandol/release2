/**
*   @description  trigger for Lead object. Note: this should be the ONLY trigger created for Lead object.
*  Additional changes to trigger logic should be added/modified with the handler class
*
*   @author Salesforce Services
*   @date 05/20/2020
*   @group Trigger
*/
trigger AmFam_LeadTrigger on Lead (before insert, before update, after insert, after update) {
    AmFam_LeadTriggerHandler handler = new AmFam_LeadTriggerHandler();

    if( Trigger.isInsert )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeInsert(trigger.New);
        }
        else
        {
            handler.OnAfterInsert(trigger.New);
        }
    }
    else if ( Trigger.isUpdate )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
        else
        {
            handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
}