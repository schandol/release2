<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_LPE_Status_to_Invalid</fullName>
        <field>LPE_Status__c</field>
        <literalValue>Not Processed</literalValue>
        <name>Set LPE Status to Invalid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Invalidate LPE Leads after 24 hrs</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LPE_Status__c</field>
            <operation>equals</operation>
            <value>Processing</value>
        </criteriaItems>
        <description>Invalidate LPE Leads that have been processing for over 24 hrs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_LPE_Status_to_Invalid</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
