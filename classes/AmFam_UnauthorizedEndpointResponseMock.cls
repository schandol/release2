@isTest
global class AmFam_UnauthorizedEndpointResponseMock implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('Unauthorized Endpoint');
        throw e;
        //return response;
    }
}