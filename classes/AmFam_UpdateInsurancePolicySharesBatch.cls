global class AmFam_UpdateInsurancePolicySharesBatch implements database.batchable<sobject> {
    List<Id> newPolicyIds;
    public AmFam_UpdateInsurancePolicySharesBatch (List<Id> newPolicyIds) {
        this.newPolicyIds = newPolicyIds;
    }

    global List<InsurancePolicy> start(database.batchableContext bc)
    {
        return [SELECT Id,ProducerId FROM InsurancePolicy WHERE Id IN :this.newPolicyIds];
    }

    global void execute(database.batchablecontext bd, List<InsurancePolicy> policies) {
        AmFam_InsurancePolicyService.updateInsurancePolicyShares(policies);      
    }

    Public void finish(database.batchableContext bc) {
    }
}