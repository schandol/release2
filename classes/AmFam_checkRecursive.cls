/**
*   @description: the apex class is designed to handle recursion 
*	@author: Anbu Chinnathambi
*   @date: 08/12/2021
*   @group: Apex Class
*/

public class AmFam_checkRecursive {
    
    //static boolean checks recurvisive call to deleteDuplicateContactContactRel method on AmFam_ContactContactRelationshipService class
    public static Boolean firstCallAmFam_ContactContactRelationshipService = false;
    public static Boolean firstCallAmFam_AccountAccountRelationshipService = false;
    
}