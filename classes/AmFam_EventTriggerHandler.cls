/**
*   @description: trigger handler for Event object. Note: this should be the only place
*	where ALL Event related trigger logic should be implemented.
*   @author: Anbu Chinnathambi
*   @date: 06/23/2021
*   @group: Trigger
*/

public without sharing class AmFam_EventTriggerHandler {
    

    public void OnBeforeInsert (List<Event> newRecord) {

    	AmFam_EventTriggerService.extendPCIFYToEvent();
    }

    /*
    public void OnAfterInsert (List<Event> newRecords) {


    }
    
    public void OnAfterUpdate (List<Event> newRecords,
                               List<Event> oldRecords,
                               Map<ID, Event> newRecordsMap,
                               Map<ID, Event> oldRecordsMap) {
    }*/

    
    public void OnBeforeUpdate(List<Event> newRecords,
                                List<Event> oldRecords,
                                Map<ID, Event> newRecordsMap,
                                Map<ID, Event> oldRecordsMap) {
                                    
    	AmFam_EventTriggerService.extendPCIFYToEvent();
    }
    

}