/**
*   @description trigger handler for AccountContactRelation object. Note: this should be the only place
*	where ALL AccountContactRelation related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 02/08/2020
*   @group Trigger
*/


public without sharing class AmFam_AccountContactTriggerHandler {
    
    /*
    public void OnBeforeInsert (List<AccountContactRelation> newRecord) {


    }
    */

    
  
    public void OnAfterInsert (List<AccountContactRelation> newRecord) {

		//AmFam_AccountContactService.shareAccessToChildFromAgencies(newRecord);
        AmFam_AccountContactService.extendProudcerAccessToHousehold(newRecord);
    }


    /*
    public void OnAfterUpdate (List<AccountContactRelation> newRecord,
                               List<AccountContactRelation> oldRecord,
                               Map<ID, AccountContactRelation> newRecordMap,
                               Map<ID, AccountContactRelation> oldRecordtMap) {

    }


    public void OnBeforeUpdate(List<AccountContactRelation> newRecord,
                                List<AccountContactRelation> oldRecord,
                                Map<ID, AccountContactRelation> newRecordMap,
                                Map<ID, AccountContactRelation> oldRecordMap) {

    }


    public void OnBeforeDelete(List<AccountContactRelation> oldRecord,
                              Map<ID, AccountContactRelation> oldRecordMap) {

    }
    */


    public void OnAfterDelete(List<AccountContactRelation> oldRecords,
                              Map<ID, AccountContactRelation> oldRecordsMap) {
      
    	
        AmFam_AccountContactService.removeProudcerAccessFromHousehold( oldRecords);

    }


}