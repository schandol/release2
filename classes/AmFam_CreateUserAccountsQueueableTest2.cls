@IsTest
public with sharing class AmFam_CreateUserAccountsQueueableTest2 
{
    @testSetup static void testSetup() {
        Id individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Id agencyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();  

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();
        u.Producer_ID__c = '73320';
        insert u;
        //List<User> userList = new List<User>{u};
        
    }

    @IsTest
    private static void testCollectInfo()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = [SELECT Id FROM User WHERE Producer_ID__c = '73320' LIMIT 1];
        AmFam_CreateUserAccountsQueueable.processUser(u.Id);

        Test.startTest();
        AmFam_CreateUserAccountsQueueable.ExistingUserInfo info = AmFam_CreateUserAccountsQueueable.collectExistingUserInfo(u.Id);
        Test.stopTest();

        System.assert(info.agencyAccount != null);
        System.assert(info.individualAccount != null);
        System.assert(info.contact != null);
        System.assert(info.acr != null);
        System.assert(info.producerAccess != null && info.producerAccess.size() == 2);
        System.assert(info.u != null);
        System.assert(info.userId != null);
    }

    @IsTest
    private static void testRecreateACR()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = [SELECT Id FROM User WHERE Producer_ID__c = '73320' LIMIT 1];
        AmFam_CreateUserAccountsQueueable.processUser(u.Id);
        List<AccountContactRelation> acrs = [SELECT Id,ContactId,AccountId FROM AccountContactRelation];

        AccountContactRelation acr = [SELECT Id,ContactId,AccountId FROM AccountContactRelation LIMIT 1];

        Id accountId = acr.AccountId;


        Contact c = new Contact(Id=acr.ContactId);
        c.Id = acr.ContactId;
        delete c; //also deletes ACR

        Test.startTest();

        AmFam_CreateUserAccountsQueueable.processUser(u.Id);
        Test.stopTest();

        AccountContactRelation newACR = [SELECT Id,AccountId FROM AccountContactRelation LIMIT 1];
        System.assert(newACR != null);
        System.assert(newACR.AccountId == accountId);
    }

    @IsTest
    private static void testRecreateIndividualAccount()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = [SELECT Id FROM User WHERE Producer_ID__c = '73320' LIMIT 1];
        AmFam_CreateUserAccountsQueueable.processUser(u.Id);
        List<AccountContactRelation> acrs = [SELECT Id,ContactId,AccountId FROM AccountContactRelation];

        AccountContactRelation acr = [SELECT Id,ContactId,AccountId FROM AccountContactRelation LIMIT 1];

        Id accountId = acr.AccountId;

        //ACR points to Indiv account so ACR needs to go, so Contact needs to go
        Contact c = new Contact(Id=acr.ContactId);
        c.Id = acr.ContactId;
        delete c; //also deletes ACR
        List<ProducerAccess__c> prodAcc = [SELECT Id from ProducerAccess__c WHERE Producer_Identifier__c = 'P-73320'];
        delete prodAcc;
        List<Account> acct = [SELECT Id FROM Account WHERE Producer_ID__c = 'P-73320'];
        delete acct;

        Test.startTest();

        AmFam_CreateUserAccountsQueueable.processUser(u.Id);

        Test.stopTest();

        AccountContactRelation newACR = [SELECT Id,AccountId FROM AccountContactRelation LIMIT 1];
        System.assert(newACR != null);

        acct = [SELECT Id FROM Account WHERE Producer_ID__c = 'P-73320'];
        System.assert(acct.size() == 1);

        prodAcc = [SELECT Id FROM ProducerAccess__c WHERE Producer_Identifier__c = 'P-73320'];
        System.assert(prodAcc.size() == 1);
    }

    //cannot readily delete agency (setup vs non-setup issue) so need to take different approach than tests in 
    //AmFam_CreateUserAccountsQueueableTest2
    @IsTest
    private static void testRecreateAgencyAccount()
    {
        Id individualRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        Id contactOwnerId = [SELECT Id FROM User WHERE Name = 'Amfam Admin' LIMIT 1][0].Id;

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = [SELECT Id,Name,FirstName,LastName,Email,FederationIdentifier,MiddleName,Phone,Title,Producer_ID__c FROM User WHERE Producer_ID__c = '73320' LIMIT 1];

        //indiv
        Account acct = new Account();
        acct.Producer_ID__c = 'P-73320';
        acct.Name = u.Name;
        acct.recordTypeId = individualRecordTypeId;
        insert acct;

        //contact
        Contact con = new Contact();
        con.OwnerId = contactOwnerId;
        con.FirstName = u.FirstName;
        con.LastName = u.LastName;
        con.Email = u.Email;
        con.User_Federation_ID__c = u.FederationIdentifier;
        con.MiddleName = u.MiddleName;
        con.Phone = u.Phone;
        con.Title = u.Title;
        con.User_Producer_ID__c = u.Producer_ID__c;
        con.recordTypeId = contactRecordTypeId;
        con.AccountId = acct.Id;
        insert con;

        //PA
        ProducerAccess__c prodAccess = new ProducerAccess__c();
        prodAccess.RelatedId__c = acct.Id;
        prodAccess.Producer_Identifier__c = acct.Producer_ID__c;
        prodAccess.Reason__c = 'ENTRY';
        insert prodAccess;

        Test.startTest();
        AmFam_CreateUserAccountsQueueable.processUser(u.Id);
        Test.stopTest();

        Account agency = [SELECT Id,RecordType.Name,Producer_ID__c FROM Account WHERE Producer_ID__c = '73320' LIMIT 1];
        System.assert(agency != null);
        System.assert(agency.RecordType.Name == 'Agency');

        AccountContactRelation newACR = [SELECT Id,AccountId FROM AccountContactRelation WHERE AccountId = :agency.Id AND ContactId = :con.Id];
        System.assert(newACR != null);
    }

    @IsTest
    private static void testRecreateProducerAccess()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = [SELECT Id FROM User WHERE Producer_ID__c = '73320' LIMIT 1];
        AmFam_CreateUserAccountsQueueable.processUser(u.Id);

        List<ProducerAccess__c> pa = [SELECT Id FROM ProducerAccess__c WHERE Producer_Identifier__c = 'P-73320' OR Producer_Identifier__c = '73320'];
        delete pa;

        Test.startTest();

        AmFam_CreateUserAccountsQueueable.processUser(u.Id);

        Test.stopTest();

        pa = [SELECT Id FROM ProducerAccess__c];
        System.assert(pa.size() == 2);
    }
}