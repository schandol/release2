global class AmFam_ManageSharingAccessBatch implements database.batchable<sobject>, Database.AllowsCallouts, Database.Stateful{
    String userId;
    AmFam_AccountLeadSharingModel.ProducerWrapper g_ProducerWrapper;
    
    public AmFam_ManageSharingAccessBatch(String amfamUserId) {
        this.userId = amfamUserId;
        this.g_ProducerWrapper = new AmFam_AccountLeadSharingModel.ProducerWrapper();
    }
    global List<LeadShare> start(database.batchableContext bc){
        // Get the Producer ID From the Alternate Keys Service
        AmFam_ProducerTypeService prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/alternatekeys?requestIdentifierType=USER_ID&responseIdentifierType=PRODUCER_ID&sourceSystemIdentifier=MSA&requestIdentifierValue='+userId);
        if (prodTypeSvc?.alternateKey?.keyId != null) {
            // Get the Producer Info from the Producer Service
            prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/'+prodTypeSvc.alternateKey.keyId);
            List<LeadShare> ls = new List<LeadShare>();
            if (prodTypeSvc != null) {
                this.g_ProducerWrapper = AmFam_AccountLeadSharingModel.producerServiceResponse(prodTypeSvc,userId);
                List<LeadShare> lsRecs = g_ProducerWrapper.leadSharingRecs;

                return lsRecs != null ? lsRecs : ls;
            }
            return ls;
        } else {
            return null;
        }
    }
    
    global void execute(database.batchablecontext bd, List<LeadShare> scope) {
        List<LeadShare> leadSharingAdd = new List<LeadShare>();
        List<LeadShare> leadSharingDelete = new List<LeadShare>();
        if(scope !=null) {
            for (LeadShare ls : scope) {
                if(ls.Id == null){
                    leadSharingAdd.add(ls);
                } else {
                    leadSharingDelete.add(ls);
                }
            }
            if (leadSharingAdd.size() > 0) {
                Database.insert(leadSharingAdd, false);
            }
            if (leadSharingDelete.size() > 0) {
                Database.delete(leadSharingDelete, false);
            }
        }
    }
    global void finish(database.batchableContext bc) {
        System.debug('AmFam_ManageSharingAccessBatch.finish');
    }
}