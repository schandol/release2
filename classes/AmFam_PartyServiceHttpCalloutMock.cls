@isTest
global class AmFam_PartyServiceHttpCalloutMock implements HttpCalloutMock {
    Boolean  isMockResponseSuccessful;

    public AmFam_PartyServiceHttpCalloutMock (Boolean isMockResponseSuccessful) {
        this.isMockResponseSuccessful  = isMockResponseSuccessful;
    }

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        if (this.isMockResponseSuccessful) {
            response.setHeader('Content-Type', 'application/json');
            response.setBody(AmFam_PartyServiceAPIWrapperTest.json);
            response.setStatusCode(200);
        } else {
            response.setStatusCode(400);
            response.setStatus('Bad request');
            response.setBody(AmFam_PartyServiceAPIWrapperTest.json);
        }
        return response;
    }
}