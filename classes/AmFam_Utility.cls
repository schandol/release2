public without sharing class AmFam_Utility {

    public static boolean isTriggerSwitchActive(String switchObject) {

        List<Trigger_Switch__mdt> triggerSwitch = [SELECT isActive__c FROM Trigger_Switch__mdt
                                                    WHERE DeveloperName =: switchObject LIMIT 1];

        Boolean trigActive = true;

        if (triggerSwitch != null && triggerSwitch.size() > 0) {
            trigActive = triggerSwitch[0].isActive__c;
        }

        return trigActive;
    }

    public static Id getInsurancePolicyQuoteRecordType() {
        return Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Quote').getRecordTypeId();
    }

    public static Id getInsurancePolicyApplicationRecordType() {
        return Schema.SObjectType.InsurancePolicy.getRecordTypeInfosByDeveloperName().get('Application').getRecordTypeId();
    }

    public static Id getAccountAgencyRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();
    }

    public static Id getAccountHouseholdRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();
    }

    public static Id getAccountBusinessRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesBusiness').getRecordTypeId();
    }

    public static Id getAccountIndividualRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
    }

    public static Id getPersonAccountRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
    }

    public static Id getCOIAccountRecordType() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('COI').getRecordTypeId();
    }

    public static Id getContactPointPhoneBusinessRecordType() {
        return Schema.SObjectType.ContactPointPhone.getRecordTypeInfosByDeveloperName().get('Business_Record_Type').getRecordTypeId();
    }

    public static Id getContactPointPhonePersonRecordType() {
        return Schema.SObjectType.ContactPointPhone.getRecordTypeInfosByDeveloperName().get('Person_Record_Type').getRecordTypeId();
    }

    public static ParticipantRole getParticipantRoleAccount() {
        return [SELECT Id FROM ParticipantRole WHERE DeveloperName = 'Account_Role' LIMIT 1];
    }


    public static AccountShare preGrantAccountAccess(Id accountId, Id userId) {

        List<UserRecordAccess> accessRecords = [SELECT RecordId, HasReadAccess, HasEditAccess, HasAllAccess FROM UserRecordAccess WHERE UserId =: userId AND RecordId =: accountId];
        System.debug('###accessRoceds: ' + accessRecords);

        Boolean hasAccess = false;
        for (UserRecordAccess ura : accessRecords) {
            if (ura.HasReadAccess || ura.HasEditAccess || ura.HasAllAccess) {
                hasAccess = true;
                break;
            }
        }

        //Account acc = [SELECT Id, UserRecordAccess.HasReadAccess FROM Account WHERE Id =: accountId LIMIT 1];
        if (!hasAccess) {
            AccountShare accShare = new AccountShare();
            accShare.AccountId = accountId;
            accShare.RowCause = 'Manual';
            accShare.AccountAccessLevel = 'Read';
            accShare.OpportunityAccessLevel = accShare.AccountAccessLevel;
            accShare.UserOrGroupId = UserInfo.getUserId();

            insert accShare;

            return accShare;
        }

        return null;

    }

    public static SObject createNewShare(String objectType, Id parentId, Id userOrGroupId) {
        Schema.SObjectType shareType = Schema.getGlobalDescribe().get(objectType);
        SObject newShareObj = shareType.newSObject();


        newShareObj.put('AccessLevel', 'Edit');
        newShareObj.put('RowCause', 'Manual');
        newShareObj.put('ParentId', parentId);
        newShareObj.put('UserOrGroupId', userOrGroupId);

        return newShareObj;
    }

    public static Schema.Address buildAddressRecord(String street, String city, String state, String postalCode, String country) {
    if (String.isNotEmpty(city) && String.isNotEmpty(street)) {
            Schema.Address newAddress = new Schema.Address();

            newAddress.City = city;
            newAddress.Street = street;
            newAddress.PostalCode = postalCode;
            newAddress.StateCode = state;
            newAddress.CountryCode = country;

            return newAddress;
        }
        return null;
    }

    public static Schema.Address getAddressRecord(String street, String city, String postalCode, String state, String country) {
        String locationName = street + ', ' + city;
        return getAddressRecord( street,  city,  postalCode,  state,  country, locationName);
    }

    public static Schema.Address getAddressRecord(String street, String city, String postalCode, String state, String country, String locationName) {
    
        if (String.isBlank(street) && String.isBlank(city) && String.isBlank(postalCode)) {
            throw new AuraHandledException('The address is missing Street, City and Postal Code');
        }

        String query = 'SELECT City, Street, CountryCode, PostalCode, StateCode, Country, State, ParentId, LocationType ';
        query += 'FROM Address WHERE Street =: street AND City =: city AND State =: state AND PostalCode =: postalCode ';

        if (String.isNotBlank(country) && country.toUpperCase() != 'US' ) {
            query += 'AND CountryCode =: country';
        }

        List<Schema.Address> addressList = Database.Query(query);

        if (addressList != null && addressList.size() > 0) {
            return addressList.get(0);
        }
        
        Schema.Location newLocation = new Schema.Location();
        newLocation.LocationType = 'Location';
        newLocation.Name = locationName;
        insert newLocation;

        Schema.Address newAddress = new Schema.Address();
        newAddress.City = city;
        newAddress.Street = street;
        newAddress.CountryCode = country;
        newAddress.PostalCode = postalCode;
        newAddress.StateCode = state;
        newAddress.ParentId = newLocation.Id;
        newAddress.LocationType = 'Location';
        insert newAddress;

        return newAddress;
    }

    public static Schema.Address getAddressRecord(wsPartyManageParty.AddressType addr) {
        String street = addr.addressLine1;
        if (addr.addressLine2 != null) {
            street += '\n' + addr.addressLine2;
        }
        String city = addr.city;
        String state = addr.stateCode;
        String postalCode = addr.zip5Code;
        String country = addr.countryIdentifier;

        if (String.isBlank(street) && String.isBlank(addr.city) && String.isBlank(addr.zip5Code)) {
            throw new AuraHandledException('The address is missing Street, City and Postal Code');
        }

        String query = 'SELECT City, Street, CountryCode, PostalCode, StateCode, Country, State, ParentId, LocationType ';
        query += 'FROM Address WHERE Street =: street AND City =: city AND State =: state AND PostalCode =: postalCode ';

        if (String.isNotBlank(country) && country.toUpperCase() != 'USA' ) {
            query += 'AND Country =: country';
        }

        system.debug('query = '+query);

        List<Schema.Address> addressList = Database.Query(query);

        if (addressList != null && addressList.size() > 0) {
            return addressList.get(0);
        }
        
        Schema.Location newLocation = new Schema.Location();
        newLocation.LocationType = 'Location';
        newLocation.Name = street + ', ' + addr.city;
        insert newLocation;

        Schema.Address newAddress = new Schema.Address();
        newAddress.City = addr.city;
        newAddress.Street = street;
        newAddress.Country = addr.countryIdentifier;
        newAddress.PostalCode = addr.zip5Code;
        newAddress.StateCode = addr.stateCode;
        newAddress.carrierRoute__c = addr.carrierRoute;
        newAddress.censusBlockGroup__c = addr.censusBlockGroup;
        newAddress.censusBlockMD__c = addr.censusBlockMD;
        newAddress.censusTractNumber__c = addr.censusTractNumber;
        newAddress.CMRA_Code__c = addr.deliveryPointValidationCommercialMailreceivingCode;
        newAddress.Delivery_Point_Match_Code__c = addr.deliveryPointValidationMatchIndicator;
        newAddress.fipsCountyNumber__c = addr.fipsCountyNumber;
        newAddress.fipsStateNumber__c = addr.fipsStateNumber;
        newAddress.geoMatchLevelCode__c = addr.geoMatchLevelCode;
        if (addr.latitudeNumber != null) {
            newAddress.Latitude = Decimal.valueOf(addr.latitudeNumber);
        }
        if (addr.longitudeNumber != null) {
            newAddress.Longitude = Decimal.valueOf(addr.longitudeNumber);
        }
        newAddress.Short_City_Name__c = addr.shortCity;
        newAddress.State_Fallout__c = addr.provinceTerritory;
        newAddress.Zip_Code_Plus_2__c = addr.zip2Code;
        newAddress.Zip_Code_Plus_4__c = addr.zip4Code;
        newAddress.ParentId = newLocation.Id;
        newAddress.LocationType = 'Location';
        insert newAddress;

        return newAddress;
    }

    public static String generateUniqueKey() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,30);
        system.debug(guid);
        return guid;
    }
    
    @future
    public static void deleteLead(Id leadId) {
        Lead l = new Lead(Id = leadId);
        delete l;
    }

    public static void deleteAccount(Id accountId) {
        Account acc = new Account(Id = accountId);
        delete acc;
    }

    public static void deleteAccountAndProducerAccessRecords(Id accountId) {
        List<ProducerAccess__c> paList = [SELECT Id FROM ProducerAccess__c WHERE RelatedId__c =: accountId];
        if (paList.size() > 0) {
            delete paList;
        }
        Account acc = new Account(Id = accountId);
        delete acc;
    }

    public static String formatGeolocation(String geoString) {
        String formattedStr = forceDecimalTrailingZeros(geoString, 6);
        formattedStr = addPlusToNonNegativeString(formattedStr);

        return formattedStr;
    }

    
    public static String addPlusToNonNegativeString(String strValue) {

        //For non null values, if it doesn't start with '-' (negative) or '+' (already signed) 
        //then add the '+' at the begining
        if (String.isNotEmpty(strValue) && !strValue.startsWith('-') && !strValue.startsWith('+')) {
            strValue = '+' + strValue;
        }
        
        return strValue;
    }

    public static String forceDecimalTrailingZeros(String myString, Integer decimalSize) {

        decimalSize++; // the '.' takes 1 place at index 0 of decimal component, so the total size should be increased by 1
        Integer strLen = myString.length();
        Integer dotIndex = myString.lastIndexOf('.');
        Integer diff = decimalSize  - (strLen - dotIndex);
        
        String newStr = myString.rightPad(strLen + diff, '0');
        
        
        System.debug('### len:' + strLen);
        System.debug('### dotIndex:' + dotIndex);
        System.debug('### diff:' + diff);
        System.debug('### newStr:' + newStr);

        return newStr;
        
    }

    public static Lead mapPhones(List<wsPartyManageParty.PartyPhoneType> phoneList, Lead leadRecord) {
        Map<String, wsPartyManageParty.PartyPhoneType> existingPhoneMap = new Map<String, wsPartyManageParty.PartyPhoneType>();
        List <wsPartyManageParty.PartyPhoneType> newPhoneList = new List<wsPartyManageParty.PartyPhoneType>();
        List<String> SFPhoneNumbers = new List<String>();
        if (leadRecord.Phone != null) {
            SFPhoneNumbers.add(leadRecord.Phone);
        }
        if (leadRecord.Additional_Phone_1__c != null) {
            SFPhoneNumbers.add(leadRecord.Additional_Phone_1__c);
        }
        if (leadRecord.Additional_Phone_2__c != null) {
            SFPhoneNumbers.add(leadRecord.Additional_Phone_2__c);
        }

        if (phoneList != null) {
            for (wsPartyManageParty.PartyPhoneType phone : phoneList) {
                if (phone.Phone != null) {
                    if (phone.Phone.PhoneNumber != null) {
                        String phoneKey = phone.Phone.PhoneNumber.areaCode + phone.Phone.PhoneNumber.number_x;
                        if(SFPhoneNumbers.contains(phoneKey)) {
                            existingPhoneMap.put(phoneKey, phone);
                        } else {
                            newPhoneList.add(phone);
                        }
                    }
                }
            }
        }

        // Phone
        wsPartyManageParty.PartyPhoneType currentPhone;
        
        currentPhone  = existingPhoneMap.get(leadRecord.Phone);
        if (currentPhone == null) {
            if (!newPhoneList.isEmpty()) {
                currentPhone = newPhoneList.get(0);
                newPhoneList.remove(0);
            }
        }
        String usages = '';
        Boolean phonePreferred = false;
        if (currentPhone != null) {
            usages = parseUsages(currentPhone);
            leadRecord.Phone = usages == '' ? null : parsePhoneNumber(currentPhone);
            phonePreferred = currentPhone?.TrustManagedPrimaryIndicator == 'Y' ? true : false;
            leadRecord.ll_Is_Phone_Preferred__c = phonePreferred; 
            leadRecord.Phone_Usage__c = usages;
        } else {
            leadRecord.Phone = null;
            leadRecord.ll_Is_Phone_Preferred__c = false;
            leadRecord.Phone_Usage__c = null;
        }
        
        //Additional_Phone_1__c
        currentPhone  = existingPhoneMap.get(leadRecord.Additional_Phone_1__c);
        if (currentPhone == null) {
            if (!newPhoneList.isEmpty()) {
                currentPhone = newPhoneList.get(0);
                newPhoneList.remove(0);
            }
        }
        usages = '';
        if (currentPhone != null) {
            usages = parseUsages(currentPhone);
            leadRecord.Additional_Phone_1__c = usages == '' ? null : parsePhoneNumber(currentPhone);
            leadRecord.Additional_Phone_1_Usage__c = usages;
        } else {
            leadRecord.Additional_Phone_1__c = null;
            leadRecord.Additional_Phone_1_Usage__c = null;
        }

        //Additional_Phone_2__c
        currentPhone  = existingPhoneMap.get(leadRecord.Additional_Phone_2__c);
        if (currentPhone == null) {
            if (!newPhoneList.isEmpty()) {
                currentPhone = newPhoneList.get(0);
                newPhoneList.remove(0);
            }
        }
        usages = '';
        if (currentPhone != null) {
            usages = parseUsages(currentPhone);
            leadRecord.Additional_Phone_2__c = usages == '' ? null : parsePhoneNumber(currentPhone);
            leadRecord.Additional_Phone_2_Usage__c = usages;
        } else {
            leadRecord.Additional_Phone_2__c = null;
            leadRecord.Additional_Phone_2_Usage__c = null;
        }

        //Additional_Phone_3__c
        currentPhone  = existingPhoneMap.get(leadRecord.Additional_Phone_3__c);
        if (currentPhone == null) {
            if (!newPhoneList.isEmpty()) {
                currentPhone = newPhoneList.get(0);
                newPhoneList.remove(0);
            }
        }
        usages = '';
        if (currentPhone != null) {
            usages = parseUsages(currentPhone);
            leadRecord.Additional_Phone_3__c = usages == '' ? null : parsePhoneNumber(currentPhone);
            leadRecord.Additional_Phone_3_Usage__c = usages;
        } else {
            leadRecord.Additional_Phone_3__c = null;
            leadRecord.Additional_Phone_3_Usage__c = null;
        }

        return leadRecord;
    }

    private static String parsePhoneNumber(wsPartyManageParty.PartyPhoneType currentPhone) {
        return (currentPhone?.Phone?.PhoneNumber?.areaCode == null ? '' : currentPhone?.Phone?.PhoneNumber?.areaCode) 
            + (currentPhone?.Phone?.PhoneNumber?.number_x == null ? '' : currentPhone?.Phone?.PhoneNumber?.number_x);
    }

    private static String parseUsages(wsPartyManageParty.PartyPhoneType currentPhone) {
        String usages = '';
        for (wsPartyManageParty.PartyPhoneUsageAndDescriptionType usageAndDescription : currentPhone.PartyPhoneUsageAndDescription) {
            if (usageAndDescription.PartyPhoneUsage != 'OTHER') {
                usages += usageAndDescription.PartyPhoneUsage + ';';
            } else if (usageAndDescription.otherDescription == 'Day' || usageAndDescription.otherDescription == 'Evening') {
                usages += usageAndDescription.otherDescription + ';';
            }
        }
        return usages;
    }

    public static Lead mapEmails(List<wsPartyManageParty.PartyEmailType> emailList, Lead leadRecord) {
        Map<String, wsPartyManageParty.PartyEmailType> existingEmailMap = new Map<String, wsPartyManageParty.PartyEmailType>();
        List <wsPartyManageParty.PartyEmailType> newEmailList = new List<wsPartyManageParty.PartyEmailType>();
        List<String> SFEmails = new List<String>();

        if(leadRecord.Email != null) {
            SFEmails.add(leadRecord.Email);
        }
            if(leadRecord.Additional_Email_1__c != null) {
            SFEmails.add(leadRecord.Additional_Email_1__c);
        }
        if(leadRecord.Additional_Email_2__c != null) {
            SFEmails.add(leadRecord.Additional_Email_2__c);
        }
        
        if(emailList != null) {
            for(wsPartyManageParty.PartyEmailType email : emailList) {
                if(email.Email != null) {
                    if(email.Email.emailAddressText != null) {
                        String emailKey = email.Email.emailAddressText;
                        if(SFEmails.contains(emailKey)) {
                            existingEmailMap.put(emailKey, email);
                        } else {
                            newEmailList.add(email);
                        }
                    }
                }
            }
        }
        wsPartyManageParty.PartyEmailType currentEmail;

        // Email
        currentEmail  = existingEmailMap.get(leadRecord.Email);
        if(currentEmail == null){
            if(!newEmailList.isEmpty()) {
                currentEmail = newEmailList.get(0);
                newEmailList.remove(0);
            }
        }
        String usages = '';
        if(currentEmail != null) {
            if(currentEmail.PartyEmailUsageAndDescription != null) {
                for(wsPartyManageParty.PartyEmailUsageAndDescriptionType usageAndDescription : currentEmail.PartyEmailUsageAndDescription) {
                    if(usageAndDescription.PartyEmailUsage != 'OTHER') {
                        usages += usageAndDescription.PartyEmailUsage + ';';
                    }
                }
            }
            leadRecord.Email_Usage__c = usages;
            leadRecord.Email = usages == '' ? null : currentEmail?.Email?.emailAddressText;
            leadRecord.ll_Is_Preferred_Email__c = currentEmail?.TrustManagedPrimaryIndicator == 'Y' ? true : false;
        } else {
            leadRecord.Email_Usage__c = null;
            leadRecord.Email = null;
            leadRecord.ll_Is_Preferred_Email__c = false;
        }
        
        // Additional_Email_1__c
        currentEmail  = existingEmailMap.get(leadRecord.Additional_Email_1__c);
        if(currentEmail == null){
            if(!newEmailList.isEmpty()) {
                currentEmail = newEmailList.get(0);
                newEmailList.remove(0);
            }
        }
        usages = '';
        if(currentEmail != null) {
            if (currentEmail.PartyEmailUsageAndDescription != null) {
                for(wsPartyManageParty.PartyEmailUsageAndDescriptionType usageAndDescription : currentEmail.PartyEmailUsageAndDescription) {
                    if(usageAndDescription.PartyEmailUsage != 'OTHER') {
                        usages += usageAndDescription.PartyEmailUsage + ';';
                    }
                }
            }
            leadRecord.Additional_Email_1_Usage__c = usages;
            leadRecord.Additional_Email_1__c = usages == '' ? null : currentEmail?.Email?.emailAddressText;
        } else {
            leadRecord.Additional_Email_1_Usage__c = null;
            leadRecord.Additional_Email_1__c = null;
        }
        // Additional_Email_2__c
        currentEmail  = existingEmailMap.get(leadRecord.Additional_Email_2__c);
        if(currentEmail == null){
            if(!newEmailList.isEmpty()) {
                currentEmail = newEmailList.get(0);
                newEmailList.remove(0);
            }
        }
        usages = '';
        if(currentEmail != null) {
            if(currentEmail.PartyEmailUsageAndDescription != null) {
                for(wsPartyManageParty.PartyEmailUsageAndDescriptionType usageAndDescription : currentEmail.PartyEmailUsageAndDescription) {
                    if(usageAndDescription.PartyEmailUsage != 'OTHER') {
                        usages += usageAndDescription.PartyEmailUsage + ';';
                    }
                }
            }
            leadRecord.Additional_Email_2_Usage__c = usages;
            leadRecord.Additional_Email_2__c = usages == '' ? null : currentEmail?.Email?.emailAddressText;
        } else {
            leadRecord.Additional_Email_2_Usage__c = null;
            leadRecord.Additional_Email_2__c = null;
        }
        return leadRecord;
    }
    
    public static Lead mapAddresses(List<wsPartyManageParty.PartyAddressType> addressList, Lead leadRecord) {
        wsPartyManageParty.PartyAddressType  primaryResidenceAddress = null;
        List<wsPartyManageParty.PartyAddressType> nonPrimaryAddressList = new List<wsPartyManageParty.PartyAddressType>();
        if (addressList == null) {
            return leadRecord;
        }
        for(wsPartyManageParty.PartyAddressType address : addressList) {
            Boolean isPrimary = false; 
            if(address.PartyAddressPurposeAndUsage != null) {
                for(wsPartyManageParty.PartyAddressPurposeAndUsageType purposeAndUsage : address.PartyAddressPurposeAndUsage) {
                    if(purposeAndUsage.PartyAddressPurpose == 'PRM' && (purposeAndUsage.PartyAddressUsage == 'R' || purposeAndUsage.PartyAddressUsage == 'B')) {
                        isPrimary = true;
                    }
                }
            }
              
            if(isPrimary) {
                primaryResidenceAddress = address;
            } else {
                nonPrimaryAddressList.add(address);
            }
        }
        if(primaryResidenceAddress != null) {
            String streetAddress = primaryResidenceAddress.Address.addressLine1;
            if (primaryResidenceAddress.Address.addressLine2 != null) {
                streetAddress += '\n' + primaryResidenceAddress.Address.addressLine2;
            }
            leadRecord.Street = streetAddress;
            leadRecord.City = primaryResidenceAddress.Address.city;
            leadRecord.StateCode = primaryResidenceAddress.Address.stateCode;
            leadRecord.PostalCode = primaryResidenceAddress.Address.zip5Code;
            leadRecord.CountryCode = primaryResidenceAddress?.Address?.countryIdentifier == 'USA'? 'US':primaryResidenceAddress?.Address?.countryIdentifier;
            String residenceUsage = residenceUsage(primaryResidenceAddress?.PartyAddressPurposeAndUsage);
            String mailingUsage = mailingUsage(primaryResidenceAddress?.PartyAddressPurposeAndUsage);
            leadRecord.Residence_Business__c = residenceUsage;
            leadRecord.Mailing__c = mailingUsage;
        }
        wsPartyManageParty.PartyAddressType  currentAddress = null;
        // Additional Address 1
        if (!nonPrimaryAddressList.isEmpty()) {
            currentAddress =  nonPrimaryAddressList.get(0);
            nonPrimaryAddressList.remove(0); 
        
            leadRecord.Additional_Address_Line_1__c = currentAddress?.Address?.addressLine1;
            leadRecord.Additional_Address_Line_2__c = currentAddress?.Address?.addressLine2;
            leadRecord.City_1__c = currentAddress?.Address?.city;
            leadRecord.State_Province_1__c = currentAddress?.Address?.stateCode;
            leadRecord.Zip_Postal_Code_1__c = currentAddress?.Address?.zip5Code;
            leadRecord.Country_1__c = currentAddress?.Address?.countryIdentifier == 'USA'? 'US':currentAddress?.Address?.countryIdentifier;
            String residenceUsage2 = residenceUsage(currentAddress?.PartyAddressPurposeAndUsage);
            String mailingUsage2 = mailingUsage(currentAddress?.PartyAddressPurposeAndUsage);
            leadRecord.Residence_Business_2__c = residenceUsage2;
            leadRecord.Mailing_2__c = mailingUsage2;  
        }else{
            leadRecord.Additional_Address_Line_1__c = '';
            leadRecord.Additional_Address_Line_2__c = '';
            leadRecord.City_1__c = '';
            leadRecord.State_Province_1__c = '';
            leadRecord.Zip_Postal_Code_1__c = '';
            leadRecord.Country_1__c = '';
            leadRecord.Residence_Business_2__c = null;
            leadRecord.Mailing_2__c = null;
        }
        
        currentAddress = null;
        // Additional Address 2
        if (!nonPrimaryAddressList.isEmpty()) {
            currentAddress =  nonPrimaryAddressList.get(0);
            nonPrimaryAddressList.remove(0);
        
            leadRecord.Additional_Address_Line_3__c = currentAddress?.Address?.addressLine1;
            leadRecord.Additional_Address_Line_4__c = currentAddress?.Address?.addressLine2;
            leadRecord.City_2__c = currentAddress?.Address?.city;
            leadRecord.State_Province_2__c = currentAddress?.Address?.stateCode;
            leadRecord.Zip_Postal_Code_2__c = currentAddress?.Address?.zip5Code; 
            leadRecord.Country_2__c = currentAddress?.Address?.countryIdentifier == 'USA'? 'US':currentAddress?.Address?.countryIdentifier;
            String residenceUsage3 = residenceUsage(currentAddress?.PartyAddressPurposeAndUsage);
            String mailingUsage3 = mailingUsage(currentAddress?.PartyAddressPurposeAndUsage);
            leadRecord.Residence_Business_3__c = residenceUsage3;
            leadRecord.Mailing_3__c = mailingUsage3;
        }else{
            leadRecord.Additional_Address_Line_3__c = '';
            leadRecord.Additional_Address_Line_4__c = '';
            leadRecord.City_2__c = '';
            leadRecord.State_Province_2__c = '';
            leadRecord.Zip_Postal_Code_2__c = '';
            leadRecord.Country_2__c = '';
            leadRecord.Residence_Business_3__c = null;
            leadRecord.Mailing_3__c = null;
        }
        return leadRecord;
    }
    
    public static string residenceUsage(List<wsPartyManageParty.PartyAddressPurposeAndUsageType> usageList){

        string resUsage = '';
        for(wsPartyManageParty.PartyAddressPurposeAndUsageType purposeAndUsage : usageList) {
            if(purposeAndUsage.PartyAddressUsage == 'R' || purposeAndUsage.PartyAddressUsage == 'B') {
                resUsage = purposeAndUsage.PartyAddressPurpose;	
            }
        }
        return resUsage;
    }
     
    public static string mailingUsage(List<wsPartyManageParty.PartyAddressPurposeAndUsageType> usageList){

        string mailUsage = '';
        for(wsPartyManageParty.PartyAddressPurposeAndUsageType purposeAndUsage : usageList) {
            if(purposeAndUsage.PartyAddressUsage == 'M') {
                mailUsage = purposeAndUsage.PartyAddressPurpose;	
            }
        }
        return mailUsage; 
    }


    public static Lead getLeadByCDHID(String cdhID) {
        Lead leadRec = new Lead();
        try{
            leadRec = [SELECT Id, FirstName, MiddleName, LastName, Gender__c, Marital_Status__c, Date_of_Birth__c,
                                Drivers_License_Number__c,Driver_s_License_State__c, Drivers_License_Issue_Month__c, Drivers_License_Issue_Year__c,
                                CountryCode, Street, City, StateCode, PostalCode, Email, Email_Usage__c, Agency__r.Producer_Id__c,
                                Phone, MobilePhone, Party_Version__c, CDHID__c, Contact_CDHID__c, OwnerId, Party_Level__c, Source_System_Key__c,  Agency__r.Is_Corporate_Account__c
                    FROM Lead WHERE CDHID__c =: cdhID LIMIT 1];
                
        }catch(System.QueryException e){
           leadRec = null;
        }    
        system.debug('leadRecdup'+ leadRec);
        return leadRec;
    }
}