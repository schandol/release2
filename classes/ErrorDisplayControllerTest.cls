@isTest
public class ErrorDisplayControllerTest {
    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    @isTest
    static void getLeadTest() {
        wsPartyManage.lpeCallout = false;
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '12345');
        insert a;
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = 'FirstName';
        l.LastName = 'LastName';
        l.Phone = '1231231234';
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
        insert l;
        Lead ldRec = ErrorDisplayController.getLead(l.Id);
        system.assertEquals(ldRec.Id, l.Id);
    }
}