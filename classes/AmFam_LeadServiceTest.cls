@isTest
private class AmFam_LeadServiceTest {
    
    @isTest
    static void testLockLeadChangesAtContactPartyLevel() {
        
    }
    
    @isTest
    static void testCheckLeadOwnerChangeBelongsToSameAgency() {
        
    }
    
    @isTest
    static void testSetSharingAccessForNewLeads() 
    {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];

        User agent1 = new User (alias = 'agent1', email='agent1@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='testa1',
                                    firstname='User', lastname='Agent1',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='agent1@xyz.com.test',
                                    AmFam_User_ID__c='agent1'
                                );

        User agent2 = new User (alias = 'agent2', email='agent2@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='testa2',
                                    firstname='User', lastname='Agent2',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p2.Id,
                                    timezonesidkey='America/Chicago',
                                    username='agent2@xyz.com.test',
                                    AmFam_User_ID__c='agent2'
                                );

        List<User> users = new List<User>{agent1,agent2};
        insert users;
        
        List<Producer> prods = new List<Producer>();
        Producer prod = new Producer();
        prod.Producer_ID__c = '19999';
        prod.Name = '19999';
        prod.InternalUserId = agent1.Id;
        prod.Type = 'Agency';
        prods.add(prod);

        prod = new Producer();
        prod.Producer_ID__c = '19999';
        prod.Name = '19999';
        prod.InternalUserId = agent2.Id;
        prod.Type = 'Agency';
        prods.add(prod);

        insert prods;

        Account agency = AmFam_TestDataFactory.insertAgencyAccount('test agency', '19999');

        Lead l = new Lead();
        l.Agency__c = agency.Id;
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.Company = 'Test';
        l.LeadSource = 'Affinity';
        l.Phone = '1231231234';
        insert l;
        
        List<LeadShare> ls = [SELECT Id,UserOrGroupId FROM LeadShare WHERE UserOrGroupId = :agent1.Id OR UserOrGroupId = :agent2.Id];
        System.debug(ls);
        System.assert(ls.size() == 2);
    }
    
    @isTest
    static void testLeadExternalId() {
        wsPartyManage.lpeCallout = false;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = 'First';
        l.LastName = 'Last';
        l.Company = 'Test';
        l.LeadSource = 'Affinity';
        l.Phone = '1231231234';
        insert l;
        
        Lead l2 = [SELECT Id, Lead_ID__c, Lead_External_ID__c 
                    FROM Lead WHERE Id =: l.Id];
                    
        system.debug(l2.Lead_ID__c);
        system.debug(l2.Lead_External_ID__c);
        system.assertEquals(l.Lead_ID__c, l.Lead_External_ID__c);
    }
    
    @isTest
    static void testLeadStageDispositionValues() {
        
    }
    
    @isTest
    static void testMarkImportedLeadsLPEStatus() {
        
    }

    //Method to test extendPCIFYToLead method on AmFam_LeadService class
    static testMethod void testPCIFYOnLead(){
        
        wsPartyManage.lpeCallout = false;
        
        Id devRecordTypeId = AmFam_Utility.getAccountAgencyRecordType();        
        Account objAcc = new Account();
        objAcc.Name = 'Test Agency Account';
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.ShippingStreet = '123 Main Street';
        objAcc.ShippingCity = 'San Francisco';
        objAcc.BillingStreet = '123 Main Street';
        objAcc.BillingCity = 'San Francisco';
        
        insert objAcc;
       
        Lead testLead = new Lead(Agency__c = objAcc.Id, FirstName = 'First', LastName = 'Last', Company = 'Test',LeadSource = 'Affinity', Phone = '1231231234', Source__c ='4917610000000000');
        test.startTest();
        insert testLead;
        test.stopTest();
        
        Lead l = [Select id, Source__c from Lead where Id =: testLead.id LIMIT 1];
        System.assert(l.Source__c.contains('*'));
        
    }
}