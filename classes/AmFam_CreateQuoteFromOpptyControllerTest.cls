/**
*   @description: the apex class is a test class for AmFam_CreateQuoteFromOpptyController
*	@author: Anbu Chinnathambi
*   @date: 08/01/2021
*   @group: Test Class
*/

    @isTest
    public class AmFam_CreateQuoteFromOpptyControllerTest {
    
    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test getPicklistValues method from AmFam_ViewPolicyDetailsController class
    static testMethod void testgetPicklistValues() {
        
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '1447411');
        insert a;
        
        Account pa = new Account(Name = 'Test Person', CDHID__c = '9823269');
        insert pa;
        
        Opportunity oppForPersonalAuto = new Opportunity(Name = 'Test1 Policy System', CloseDate =system.today()+30 , AccountId =pa.id, StageName = 'New', Description = 'Test', Lines_of_Business__c ='Personal Vehicle');
        insert oppForPersonalAuto;
        
        Opportunity oppForCommercial = new Opportunity(Name = 'Test1 Policy System', CloseDate =system.today()+30 , AccountId =pa.id, StageName = 'New', Description = 'Test', Lines_of_Business__c ='Commercial');
        insert oppForCommercial;
        
        test.startTest();
        Map<String, String> mapForPersonalAuto = AmFam_CreateQuoteFromOpptyController.getPicklistValues(oppForPersonalAuto.id);
        Map<String, String> mapForCommercial = AmFam_CreateQuoteFromOpptyController.getPicklistValues(oppForCommercial.id);
        test.stopTest();
        
        System.assert(mapForPersonalAuto.values()[0]=='Personal Auto');
        System.assert(mapForCommercial.values()[0]=='Commercial (Fusion)');
     }
        
        
    //Method to test getSourceSystemURL method from AmFam_ViewPolicyDetailsController class
    static testMethod void testgetPolicySystemURL() {
        
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '144745');
        insert a;
        
        Account pa = new Account(Name = 'Test Person', CDHID__c = '9823261');
        insert pa;
        
        Opportunity opp = new Opportunity(Name = 'Test Policy System', CloseDate =system.today()+30 , AccountId =pa.id, StageName = 'New', Description = 'Test', Agency__c = a.id, Lines_of_Business__c ='Personal Vehicle');
        insert opp;
        
        test.startTest();
        String returnURLForAutoOrHome = AmFam_CreateQuoteFromOpptyController.getPolicyQuoteURL(opp.id, 'PersonalAuto');
        String returnURLForCommercialClassic = AmFam_CreateQuoteFromOpptyController.getPolicyQuoteURL(opp.id, 'CommercialClassic');
        String returnURLForCommercialFusion = AmFam_CreateQuoteFromOpptyController.getPolicyQuoteURL(opp.id, 'CommercialFusion');
        String returnURLForFarmRanchFusion = AmFam_CreateQuoteFromOpptyController.getPolicyQuoteURL(opp.id, 'FarmRanchFusion');
        String returnURLForLife_Cornerstone = AmFam_CreateQuoteFromOpptyController.getPolicyQuoteURL(opp.id, 'Life_Cornerstone');
        test.stopTest();
        
        System.assert(returnURLForAutoOrHome.contains('cdhID='+pa.CDHID__c));
        System.assert(returnURLForCommercialClassic.contains('cdhId='+pa.CDHID__c));
        System.assert(returnURLForCommercialFusion.contains('partyID='+pa.CDHID__c));
        System.assert(returnURLForFarmRanchFusion.contains('partyID='+pa.CDHID__c));
        System.assert(returnURLForLife_Cornerstone.contains('cdhID='+pa.CDHID__c));
        
     }

}