/**
*   @description: the class handles both Person and Household Account Merge Request from CDH
*	the merge request reparent all the duplicate Account's related child record to Master Account
*   @author: Anbu Chinnathambi
*   @date: 06/25/2021
*   @group: Apex Class/Rest API
*/

@RestResource(urlMapping='/MergeAccounts/*')
global with sharing class AmFam_CDHAccountMergeRequest{
    
    @HttpPost
    global static ResponseWrapper doMerge(){
        // rollback point    
         Savepoint sp = Database.setSavepoint();
        
         try{
            RestRequest req = RestContext.request;   
            String jsonInput = req.requestBody.toString();
            system.debug('jsonInput'+ jsonInput);
            Map<String, Object> jsonResults = (Map<String, Object>) JSON.deserializeUntyped(jsonInput);
            Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
            Id businessAccountRTId = AmFam_Utility.getAccountBusinessRecordType();
            Id houseHoldAccountRTId = AmFam_Utility.getAccountHouseholdRecordType();
        
            String master = JSON.serialize(jsonResults.get('master'));
            String duplicate = JSON.serialize(jsonResults.get('duplicates'));
            String surviving = JSON.serialize(jsonResults.get('survivingParty'));
            String houseHold = JSON.serialize(jsonResults.get('survivingHousehold'));
             
            system.debug('surviving'+ surviving);
            system.debug('houseHold' + houseHold);
           
            Account masterAccount = (Account)JSON.deserialize(master, Account.class);
            List<Account> duplicateAccounts = (List<Account>)JSON.deserialize(duplicate, List<Account>.class);
            List<Id> duplicateIds = new List<Id>();
            //collection of duplicate account ids
            for(Account a: duplicateAccounts){
                duplicateIds.add(a.id);
            }
             
            string accountSource = '';
            string source = '';
            string importantNotes = '';
            Date pirDate = null;
            string pirOutcome = null;
            string chatterPostBody = '';
             
            List<Id> accountIdList = duplicateIds;
            accountIdList.add(masterAccount.id);
            List<Id> personContactIds = new List<Id>();
             
            Map<string, string> accountSourceMap = new Map<string, string>();
            Map<Date, string> pirDateOutcomeMap = new Map<Date, string>();
            for(Account rec : [select id, PersonContactId, AccountSource, Source__c, Important_Notes__c, PIR_Date__c, PIR_Outcome__c, RecordtypeId  from Account where id=:accountIdList ORDER BY AccountSource DESC NULLS LAST]){
                
                //collection of PersonContactId from duplicate accounts
                if(rec.RecordtypeId == personAccountRTId && duplicateIds?.contains(rec.id)){
                	personContactIds.add(rec.PersonContactId);	    
                }
                
                if(String.isNotBlank(rec.AccountSource) || String.isNotBlank(rec.Source__c)){
                    accountSourceMap.put(rec?.AccountSource, rec?.Source__c);
                    accountSource = String.isBlank(accountSource) ? rec.AccountSource : accountSource ;
                    source = String.isBlank(accountSource) ? rec.Source__c : accountSourceMap?.get(accountSource);
                }
                if(String.isNotBlank(rec.Important_Notes__c)){
                	importantNotes = String.isBlank(importantNotes) ? rec.Important_Notes__c : rec.Important_Notes__c + '\n' + importantNotes ; 
                    if(importantNotes.length() > 4800){
                        chatterPostBody = 'Important Notes - '+ importantNotes;
                    	importantNotes = importantNotes.substring(0, 4800);
                    }
                }
                if(rec.PIR_Date__c != null || String.isNotBlank(rec.PIR_Outcome__c)){
                     pirDateOutcomeMap.put(rec?.PIR_Date__c, rec?.PIR_Outcome__c);
                     pirDate = pirDate == null ? rec.PIR_Date__c : pirDate > rec.PIR_Date__c ? pirDate : rec.PIR_Date__c;
                     pirOutcome = pirDate == null ? rec.PIR_Outcome__c : pirDateOutcomeMap?.get(pirDate) ; 
                }
            }
             
            masterAccount.AccountSource =  String.isNotBlank(accountSource)? accountSource :null;
            masterAccount.Source__c =  String.isNotBlank(source)? source :'';
            masterAccount.Important_Notes__c =  String.isNotBlank(importantNotes)? importantNotes :'';
            masterAccount.PIR_Date__c = pirDate!= null ? pirDate :null;  
            masterAccount.PIR_Outcome__c =  String.isNotBlank(pirOutcome)? pirOutcome :null; 
             
           //chatter post
           if(string.isNotBlank(chatterPostBody)){
               FeedItem post = new FeedItem();
               post.ParentId = masterAccount.id;
               post.Body = chatterPostBody;
               insert post;         
            }
           
            //deleting duplicate person account's AccountContactRelation record 
            if(surviving != 'null' && personContactIds.size()>0){
            	List<AccountContactRelation> accountConRelListToDelete = [Select id, Roles, AccountId, ContactId  from AccountContactRelation where ContactId =:personContactIds AND Roles = 'Member' ];
                if(accountConRelListToDelete.size()>0) delete accountConRelListToDelete;   
            }
            
            String returnAccId = masterAccount.Id;
            List<String> errorMessage = new List<String>();
            Boolean success;
            
            //Account Merge Request is handled here
            Database.MergeResult[] results = Database.merge(masterAccount, duplicateAccounts, false);
            for(Database.MergeResult res : results) {
                if (res.isSuccess()) {
                    System.debug('Master record ID: ' + res.getId());
                    System.debug('IDs of merged records: ' + res.getMergedRecordIds());                
                    System.debug('Reparented record ID: ' + res.getUpdatedRelatedIds());
                    success = true;
             }
                else {
                    for(Database.Error err : res.getErrors()) {
                        System.debug(err.getMessage());
                        errorMessage.add(err.getMessage());
                        success = false;
                    }
                }
            }
            
            if(success){
                
                //PersonAccount related survivig parties(ContactPointEmail,ContactPointPhone,AssociatedLocation,Address) are deleted and created in the below code
                if(surviving != 'null'){
                    List<sObject >contactPointEmailListToInsert = new List<ContactPointEmail>(); 
                    List<sObject> contactPointPhoneListToInsert = new  List<ContactPointPhone>(); 
                    List<sObject> associatedLocListToInsert = new List<AssociatedLocation>(); 
                    List<sObject> addressListToInsert =new List<Schema.Address>();
                    List<sObject> accountAccountRelationToInsert = new List<FinServ__AccountAccountRelation__c>(); 
                    List<sObject> contactContactRelationToInsert = new List<FinServ__ContactContactRelation__c>(); 
                    
                    for(Sobject obj : (List<Sobject>)JSON.deserialize(surviving, List<Sobject>.class)){
                    
                        if(String.valueOf(obj.getSObjectType()) == 'ContactPointEmail'){
                            contactPointEmailListToInsert.add(obj);
                        }
                        if(String.valueOf(obj.getSObjectType()) == 'ContactPointPhone'){
                            contactPointPhoneListToInsert.add(obj);
                        }
                        if(String.valueOf(obj.getSObjectType())== 'AssociatedLocation'){
                           associatedLocListToInsert.add(obj);
                        }
                        if(String.valueOf(obj.getSObjectType()) == 'Address'){
                            addressListToInsert.add(obj);
                        }
                        if(String.valueOf(obj.getSObjectType()) == 'FinServ__AccountAccountRelation__c'){
                            accountAccountRelationToInsert.add(obj);
                        }
                        if(String.valueOf(obj.getSObjectType()) == 'FinServ__ContactContactRelation__c'){
                            contactContactRelationToInsert.add(obj);
                        }
                     
                   }
                    //Delete all the existing records
                    List<ContactPointEmail> contactPointEmailListToDelete = [Select id, ParentId from ContactPointEmail where parentId =:masterAccount.id ]; 
                    List<ContactPointPhone> contactPointPhoneListToDelete =  [Select id, ParentId from ContactPointPhone where parentId =:masterAccount.id]; 
                    List<AssociatedLocation> associatedLocListToDelete =  [Select id, ParentRecordId from AssociatedLocation where ParentRecordId =:masterAccount.id]; 
                    List<FinServ__AccountAccountRelation__c> accountAccountRelationListToDelete =  [Select id, FinServ__RelatedAccount__c, FinServ__Account__c,FinServ__Role__c  from FinServ__AccountAccountRelation__c where FinServ__Account__c =:masterAccount.id]; 
                    List<FinServ__ContactContactRelation__c> contactContactRelationListToDelete =  [Select id, FinServ__RelatedContact__c, FinServ__Contact__c,FinServ__Role__c from FinServ__ContactContactRelation__c where FinServ__Contact__c =:masterAccount.id]; 
                    
                    if(contactPointEmailListToDelete.size()>0) delete contactPointEmailListToDelete; 
                    if(contactPointPhoneListToDelete.size()>0) delete contactPointPhoneListToDelete; 
                    if(associatedLocListToDelete.size()>0) delete associatedLocListToDelete;
                    if(accountAccountRelationListToDelete.size()>0) delete accountAccountRelationListToDelete;
                    if(contactContactRelationListToDelete.size()>0) delete contactContactRelationListToDelete;
                    
                    //Insert the new records
                    if(contactPointEmailListToInsert.size()>0) insert contactPointEmailListToInsert;
                    if(contactPointPhoneListToInsert.size()>0) insert contactPointPhoneListToInsert;
                    if(associatedLocListToInsert.size()>0) insert associatedLocListToInsert;
                    if(addressListToInsert.size()>0) insert addressListToInsert;
                    if(accountAccountRelationToInsert.size()>0) insert accountAccountRelationToInsert;
                    if(contactContactRelationToInsert.size()>0) insert contactContactRelationToInsert;
                    
                 //HouseHoldAccount related survivig party(AccountContactRelation) is deleted and created in the below code
                }else if(houseHold != 'null'){
                    
                    List<AccountContactRelation> accountContactRel = new List<AccountContactRelation>();
                    List<Id> AccountIds = new List<id>();
                    for(AccountContactRelation acr: (List<AccountContactRelation>)JSON.deserialize(houseHold, List<AccountContactRelation>.class)){
                        accountContactRel.add(acr);
                    }
                    List<AccountContactRelation> accountContactRelListToDelete = [Select id, Roles, AccountId, ContactId  from AccountContactRelation where AccountId =:masterAccount.Id AND Roles = 'Member' ];
                    if(accountContactRelListToDelete.size()>0) delete accountContactRelListToDelete;
                    if(accountContactRel.size()>0) insert accountContactRel;
                    
                }
                
            }else{
                Database.rollback(sp);
            }
            
            ResponseWrapper respwrap = new ResponseWrapper(returnAccId,success,errorMessage);
            System.debug('respwrap' + respwrap);
            return respwrap;
            
        }Catch(System.DmlException e){
            Database.rollback(sp);
            List<String> exceptionMessage = new List<String>{string.valueOf(e.getMessage())};
            ResponseWrapper respException = new ResponseWrapper(null,false,exceptionMessage);
            return respException;
        }
    }   
    
    //Response wrapper class
    global class ResponseWrapper  {
        public String id;
        public Boolean success;
        public List<String> errors;
        public ResponseWrapper (String id, Boolean success, List<String> errors) {
            this.id = id;
            this.success = success;
            this.errors = errors;
            }
    }
}