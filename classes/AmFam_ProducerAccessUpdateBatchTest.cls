/**
*   @description: test class for AmFam_ProducerAccessUpdateBatch and AmFam_ProducerAccessUpdateBatchScheduler classes 
*	@author: Anbu Chinnathambi
*   @date: 11/02/2021
*   @group: Test Class
*/
@istest
public with sharing class AmFam_ProducerAccessUpdateBatchTest {

    @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }

    static testMethod public void testProducerAccessUpdateBatchScheduler() {
        Datetime dt = Datetime.now().addMinutes(2);
        String CRON_EXP = dt.format('s m H d M \'?\' yyyy');
        /*
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        Integer minuteInt = system.now().minute() + 2;
        String minute = string.valueOf(minuteInt > 59 ? minuteInt - 60 : minuteInt);
        String second = string.valueOf(system.now().second());
        String year = string.valueOf(system.now().year());
        
        String CRON_EXP = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        */
        
        Test.startTest();
            
           String jobId = System.schedule('ProducerAccessUpdateBatchScheduler' + System.currentTimeMillis(),  CRON_EXP, new AmFam_ProducerAccessUpdateBatchScheduler());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            
        Test.stopTest();
    }
    
    static testMethod public void testProducerAccessUpdateBatch() {
          
        AmFam_UserService.callProducerAPI = false;

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'tagent', email='Agent12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='Agent12456',
                                    firstname='AmFam', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='Agent12456@xyz.com.test',
                                    AmFam_User_ID__c='testAgent12456'
                                );
        
        insert agent;

        Account agency = new Account();
        agency.Name = 'Acorn';
        agency.RecordTypeId = AmFam_Utility.getAccountAgencyRecordType();
        agency.Producer_ID__c = '989899';
        insert agency;
        
        Account personAccount = new Account();
        personAccount.Agency__c = agency.id;
        personAccount.FirstName = 'Aaron';
        personAccount.LastName = 'Finch';
        personAccount.RecordTypeId = AmFam_Utility.getPersonAccountRecordType();
        insert personAccount;
          
        Account householdAcc = new Account();
        householdAcc.Name = 'Aaron Finch HH';
        householdAcc.RecordTypeId = AmFam_Utility.getAccountHouseholdRecordType();
        insert householdAcc;
          
        Producer producer = new Producer();
        producer.AccountId = agency.Id;
        producer.InternalUserId = agent.id;
        producer.Name = '65554';
        insert producer;
          
        ProducerAccess__c pa = new ProducerAccess__c();
        pa.Producer_Identifier__c = '65554';
        pa.RelatedId__c = personAccount.Id;
        pa.Reason__c = 'ENTRY';
        insert pa;

        Test.startTest();
        AmFam_ProducerAccessUpdateBatch obj = new AmFam_ProducerAccessUpdateBatch(null);  
        Database.executeBatch( obj ); 
        List<AccountShare> accountShares = [SELECT Id, AccountId, Account.Name, AccountAccessLevel, UserOrGroupId, UserOrGroup.Name FROM AccountShare WHERE RowCause = 'Manual'];
        System.assert(accountShares.size()==2);
        Test.stopTest();
          
      }  
}