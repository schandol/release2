public without sharing class AmFam_ProducerTriggerHandler {
    public AmFam_ProducerTriggerHandler() {

    }

    public void OnAfterInsert (List<Producer> newRecords) {
        
        AmFam_ProducerTriggerService.createNewLeadShares(newRecords);
        AmFam_ProducerTriggerService.createNewAccountShares(newRecords);
        AmFam_ProducerTriggerService.createNewProducerShares(newRecords);
        AmFam_ProducerTriggerService.createPolicySharesForNewProducer(newRecords);
    }

    public void OnBeforeDelete(List<Producer> oldRecords,
                Map<ID, Producer> oldRecordsMap) 
    {
    }   

    public void OnAfterDelete(List<Producer> oldRecords,
                              Map<ID, Producer> oldRecordsMap) {

        AmFam_ProducerTriggerService.removeAccountShares(oldRecords, oldRecordsMap);
        AmFam_ProducerTriggerService.cleaupPolicyShares(oldRecords, oldRecordsMap);
        AmFam_ProducerTriggerService.removeLeadShares(oldRecords);
    }


}