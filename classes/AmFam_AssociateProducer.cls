public with sharing class AmFam_AssociateProducer {

    public static Boolean associationProducerCallOut(Account existingAccount, Account acc, Lead rec){
        Id agencyId = acc?.Agency__c!= null?acc.Agency__c:rec?.Agency__c;
        Account agency = [SELECT Producer_Id__c FROM Account WHERE Id =: agencyId LIMIT 1];
        User userRec = [SELECT AmFam_User_ID__c FROM User WHERE Id =: UserInfo.getUserId() Limit 1];
        wsPartyManageServiceCallout.PartyManageServiceSOAP associateProd = new wsPartyManageServiceCallout.PartyManageServiceSOAP();
        wsPartyManageService.AssociateProducerRequestType associateProducerRequest = new wsPartyManageService.AssociateProducerRequestType();
        associateProducerRequest.partyIdentifier = existingAccount.CDHID__c;
        associateProducerRequest.producerId = agency.Producer_Id__c;
        associateProducerRequest.PartyProducerReasonCode = 'EXACTMATCH';
        associateProducerRequest.sourceSystemName = 'SALESFORCE';
        associateProducerRequest.userId = userRec.AmFam_User_ID__c;
        associateProducerRequest.consumer = 'SALESFORCE';
        wsPartyManageService.AssociateProducerResultType resp;
        String errorLog = '';
            //call out
        try{
            resp = associateProd.associateProducer_Http(associateProducerRequest);
            if(resp.Errors != null) {
                for(wsPartyManageParty.ErrorType err : resp.Errors.Error) {
                    if(err.TypeOfError == 'ERROR') {
                        errorLog += err.ErrorCode + ' on field ' + err.field + ': ' + err.errorMessage + ' (CDH_ID :' + resp.Errors.partyIdentifierKey + ')';
                        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
                        failureLog.createAPIFailureLogNoProducerReturned('AmFam_AssociateProducer', 'http://service.amfam.com/partymanageservice', resp.Errors.partyIdentifierKey, errorLog);
                        return false;
                    }
                }
            }
            return true;
        } catch (Exception e) {
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLog('AmFam_AssociateProducer', null, 'http://service.amfam.com/partymanageservice', existingAccount.CDHID__c, e.getMessage());
            return false;
        }
    }

    
}