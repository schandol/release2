@IsTest
global class AmFam_ProducerTypeServiceHttpCalloutMock implements HttpCalloutMock {
    global Boolean useAgentJSON;
    global String modifier;

    public AmFam_ProducerTypeServiceHttpCalloutMock(Boolean agentJSON) {
        useAgentJSON = agentJSON;
    }

    public AmFam_ProducerTypeServiceHttpCalloutMock(Boolean agentJSON, String mod) {
        modifier = mod;
        useAgentJSON = agentJSON;
    }

    //modifier 999
    static String notFoundResponse = '{"status":{"maxMessageLevel":"ERROR",'+
	    '"reason":"Not found",'+
	    '"code":404,'+
	    '"messages":[{"code":"404000",'+
	    '"description":"Matching resource was not found",'+
	    '"level":"ERROR",'+
	    '"details":[{"field":"resourceIdentifier","description":"No Book Of Business for given resourceIdentifier"}]}],'+
	    '"metrics":{"acceptedOn":"2021-08-30T10:41:54","returnedOn":"2021-08-30T10:41:55"}}}';

    //modifier 888
    static String ssoBoBResponse = '{"status":{"maxMessageLevel":"INFO","reason":"OK","code":200,"metrics":{"acceptedOn":"2021-09-02T10:54:55","returnedOn":"2021-09-02T10:54:55"}},'+
        '"bookOfBusiness":[{"userId":"EQP525@amfam.com","role":"CALL_CENTER","title":"Sales Service & Operations Staff"}],'+
        '"partners":[{"partnerId":"AFI","partnerName":"American Family Insurance","distributionChannels":[{"distributionChannelId":"AMFAM","distributionChannelName":"American Family"}]}]}';

    global HTTPResponse respond(HTTPRequest req) {
        System.debug('AmFam_ProducerTypeServiceHttpCalloutMock modifier:'+modifier);
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if (req.getEndpoint().contains('alternatekeys')) {
            res.setBody(AmFam_ProducerTypeServiceTest.alternateKeysJSON);
        } else if (req.getEndpoint().contains('booksofbusiness')) {
            if (useAgentJSON) {
                if (modifier == '0')
                {
                    res.setBody(AmFam_ProducerTypeServiceTest.agentBookOfBusiness0JSON);
                }
                else if (modifier == '2')
                {
                    res.setBody(AmFam_ProducerTypeServiceTest.agentBookOfBusiness2JSON);
                }
                else if (modifier == '3')
                {
                    res.setBody(AmFam_ProducerTypeServiceTest.agentBookOfBusiness3JSON);
                }
                else if (modifier == '888')
                {
                    res.setBody(ssoBoBResponse);
                }
                else if (modifier == '999')
                {
                    res.setBody(notFoundResponse);
                }
                else
                {
                    res.setBody(AmFam_ProducerTypeServiceTest.agentBookOfBusinessJSON);
                }
            } else {
                res.setBody(AmFam_ProducerTypeServiceTest.csrBookOfBusinessJSON);
            }
        } else if (req.getEndpoint().contains('producers')) {
            if (useAgentJSON) {
                res.setBody(AmFam_ProducerTypeServiceTest.agentProducerJSON);
            } else {
                res.setBody(AmFam_ProducerTypeServiceTest.csrProducerJSON);
            }
        } else if (req.getEndpoint().contains('agencies')) {
            res.setBody(AmFam_ProducerTypeServiceTest.agencyJSON);
        }
        res.setStatusCode(200);
        return res;
    }
}