@IsTest
public with sharing class AmFam_HouseholdOppsListControllerTest {
    public AmFam_HouseholdOppsListControllerTest() {

    }

    @IsTest
    public static void testGetOpps()
    {
        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');

        List<FinServ__ReciprocalRole__c> roles = new List<FinServ__ReciprocalRole__c>();

        List<AccountContactRelation> rels = new List<AccountContactRelation>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        List<Opportunity> newOpps = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.CloseDate = Date.today().addDays(14);
        opp.Name = 'Person1 opp';
        opp.StageName = 'New';
        opp.AccountId = person1.Id;
        newOpps.add(opp);

        opp = new Opportunity();
        opp.CloseDate = Date.today().addDays(21);
        opp.Name = 'Person2 opp';
        opp.StageName = 'New';
        opp.AccountId = person2.Id;
        newOpps.add(opp);

        insert newOpps;

        List<Opportunity> householdOpps = AmFam_HouseholdOppsListController.getHouseholdOpportunities(household.Id);
        System.assertEquals(2,householdOpps.size());
        
    }
}