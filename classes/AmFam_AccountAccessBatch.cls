public without sharing class AmFam_AccountAccessBatch implements Database.Batchable<sObject>{

    public final String query;
    public final String agentUserId;
    public final String producerIdentifier;
    public final Set<String> accountIds;
    public final Set<String> householdIds;
    public final Set<String> allAccounts;
 
    public AmFam_AccountAccessBatch(String agent, Set<String> accounts, Set<String> households, String producer){
 
        this.accountIds = accounts;
        this.houseHoldIds = households;
        this.agentUserId = agent;
        this.producerIdentifier = producer;
        this.allAccounts = new Set<String>();

        if (accounts != null) {
            this.allAccounts.addAll(accounts); 
        }
        
        if (households != null) {
            this.allAccounts.addAll(households); 
        }


        this.query = 'SELECT Id, OwnerId FROM Account WHERE Id IN: allAccounts';

        Set<String> allAccounts = this.allAccounts;
    }
 
    public Database.QueryLocator start(Database.BatchableContext BC){

        return Database.getQueryLocator(this.query);
    }
 
    public void execute(Database.BatchableContext BC, List<Account> scope){

        List<ProducerAccess__c> producerAccess = new List<ProducerAccess__c>();

        List<ProducerAccess__c> existingProducerAccess = [SELECT Id, RelatedId__c, Producer_Identifier__c 
                                                        FROM ProducerAccess__c 
                                                        WHERE Producer_Identifier__c =: this.producerIdentifier AND RelatedId__c IN: this.accountIds];

        Set<String> producerAccessKeys = new Set<String>();
        for (ProducerAccess__c prac : existingProducerAccess) {
            String key = prac.RelatedId__c + '-' + prac.Producer_Identifier__c;
            producerAccessKeys.add(key);
        }

        List<Opportunity> oppList = [SELECT Id, OwnerId FROM Opportunity WHERE AccountId IN: scope];

        for(Account acc : scope){
    
            acc.OwnerId = this.agentUserId;

            if (this.accountIds.contains(acc.Id)) {

                //Do not craete producer Access record if it already exists
                String key = acc.Id + '-' + this.producerIdentifier;
                if (!producerAccessKeys.contains(key)) {

                    ProducerAccess__c pac = new ProducerAccess__c();
                    pac.RelatedId__c = acc.Id;
                    pac.Producer_Identifier__c = this.producerIdentifier;
                    pac.Reason__c = 'ENTRY';

                    producerAccess.add(pac);
                }
            }
    
        }

        for (Opportunity opp : oppList) {
            opp.OwnerId = agentUserId;
        }
        
        update scope;

        update oppList;

        insert producerAccess;
     }
 
    public void finish(Database.BatchableContext BC){

    }
 }