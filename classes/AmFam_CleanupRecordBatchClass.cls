global class AmFam_CleanupRecordBatchClass implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
     
    global Database.QueryLocator start(Database.BatchableContext bc){
        string type = 'Party';
        string soqlQuery = 'SELECT Id, Name, Identifier__c, Exception__c, Processed__c, Level_Of_Details__c, Type__c From CleanupRecord__c where Processed__c = false AND Type__c =: type';//
        return Database.getQueryLocator(soqlQuery); 
    }
     
    global void execute(Database.BatchableContext bc, List<CleanupRecord__c> scope){
        
        Map<String, String> cdhIdExceptionMessageMap = new Map<String, String>();
        //level of details 
        List<String> levelOfDetailsForProcessingAccount = new List<String>{'ALL', 'DEMOGRAPHIC','ADDITIONAL','IDENTITY', 'EMPLOYMENT'};
        //collection of CDHIDs
        List<string> cdhIdList = new List<string>();
        //Map of existing account and its CDHID
        Map<string, Account> cdhIdAccountMap = new  Map<string, Account>();
        //response map
        Map<String, wsPartySearchService.RetrieveResponseType> cdhIdRetrieveResponseMap = new Map<String, wsPartySearchService.RetrieveResponseType>();
        //collection of account records to be inserted/updated
        List<Account> listOfAccountsForUpsert = new List<Account>();
        //list of clean up records for updated
        List<CleanupRecord__c> listOfCleanUpRecForUpdate = new List<CleanupRecord__c>();
        //to avoid duplicate records 
        List<CleanupRecord__c> records = new List<CleanupRecord__c>();
        //HouseHold RecordType Id
        Id householdRT = AmFam_Utility.getAccountHouseholdRecordType();
        
        
        for(CleanupRecord__c crr : scope){
            if(crr?.Identifier__c != null && !cdhIdList.contains(crr?.Identifier__c)){
                cdhIdList.add(crr.Identifier__c);
                records.add(crr);
            }
        }
        
        
        for(Account acc : [select id, CDHID__c, RecordTypeId, PersonContactId from Account where CDHID__c =:cdhIdList]){
        	if(acc?.CDHID__c != null && ! cdhIdAccountMap.containsKey(acc?.CDHID__c) && acc.RecordTypeId != householdRT){
                cdhIdAccountMap.put(acc.CDHID__c, acc);
            }
        }
        
        //collection of CDH IDs of both processing account and its related parties
        List<string> relatedPartyCDHIdList = cdhIdList;
        
        //retrieve response for each clean up record and process account details to be updated
        for (CleanupRecord__c cr : records){
            
            string cdhId =cr?.Identifier__c; 
            try{
                Account accountRec = cdhIdAccountMap?.get(cdhId); 
                if(string.isNotBlank(cdhId)){
                    //retrieve 
                    if(levelOfDetailsForProcessingAccount.contains(cr.Level_Of_Details__c) && cr.Type__c == 'Party'){
                            wsPartySearchService.RetrieveResponseType partySearchResponse = AmFam_CleanupRecordBatchHelper.retrieveParty(cdhId);
                            Map<Account, List<string>> returnMap = AmFam_CleanupRecordBatchHelper.processAccountDetails(accountRec, partySearchResponse, cdhId);
                            listOfAccountsForUpsert.addAll(returnMap.keySet());
                            for(List<string> strList : returnMap.values()){
                                relatedPartyCDHIdList.addAll(strList);    
                            }
                            cdhIdRetrieveResponseMap.put(cdhId, partySearchResponse);     
                    }
                }
            }
            catch(Exception e){
                System.debug('Error-' + e?.getMessage());
                cdhIdExceptionMessageMap.put(cdhId, e?.getMessage());
            }
        }
        
        List<Id> accountIdSuccess = new List<Id>();
        if(listOfAccountsForUpsert.size()>0){
        	Database.UpsertResult[] results = Database.upsert(listOfAccountsForUpsert, Account.Id);
            for(Integer i=0;i<results.size();i++){
                if (results.get(i).isSuccess()){
                    accountIdSuccess.add(results.get(i).getId());
                }else if (!results.get(i).isSuccess()){
                    Database.Error error = results.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    listOfAccountsForUpsert.get(i);
                    system.debug('Failed ID'+listOfAccountsForUpsert.get(i).Id);
                 }
            }
        }
        
        Map<string, Account> cdhIdAllAccountMap = new  Map<string, Account>();
        List<Id> accountIdList = new List<Id>();
        Map<Id, Id> personContactIdAccIdMap = new Map<Id, Id>();
        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
        for(Account acc : [select id, CDHID__c, RecordTypeId, RecordType.Name, PersonContactId from Account where CDHID__c =:relatedPartyCDHIdList]){
            if(acc.RecordTypeId != householdRT){
                cdhIdAllAccountMap.put(acc.CDHID__c, acc);
                accountIdList.add(acc.id);
                if(acc.RecordTypeId == personAccountRTId){personContactIdAccIdMap.put(acc.PersonContactId, acc.Id);}
            }
        }
        
        system.debug('Account CDHID Map>>>>>>' + cdhIdAllAccountMap);

        //collection of all existing Phones, Emails and Associated Locations related to given CDHIDs
        Map<Id, List<ContactPointPhone>> accountIdContactPointPhoneListMap = new  Map<Id, List<ContactPointPhone>>();
        Map<Id, List<ContactPointEmail>> accountIdContactPointEmailListMap = new  Map<Id, List<ContactPointEmail>>();
        Map<Id, List<AssociatedLocation>> accountIdLocationListMap = new  Map<Id, List<AssociatedLocation>>();
        
        for(ContactPointPhone ph : [select id, ParentId, TelephoneNumber, UsageType, Usage_Type_Comment__c from ContactPointPhone where ParentId =: accountIdList]){
            if(accountIdContactPointPhoneListMap.containsKey(ph.ParentId)){
            	accountIdContactPointPhoneListMap.get(ph.ParentId).add(ph);
            }else{
                accountIdContactPointPhoneListMap.put(ph.ParentId, new List<ContactPointPhone>{ph});    
            }	    
        }
        
        for(ContactPointEmail email : [select id, ParentId, EmailAddress, UsageType, Usage_Type_Comment__c from ContactPointEmail where ParentId =: accountIdList]){
            if(accountIdContactPointEmailListMap.containsKey(email.ParentId)){
            	accountIdContactPointEmailListMap.get(email.ParentId).add(email);
            }else{
                accountIdContactPointEmailListMap.put(email.ParentId, new List<ContactPointEmail>{email});    
            }	    
        }
        
        for(AssociatedLocation associatedLoc : [select id, LocationId, Location.Name, Location.OwnerId, Location.LocationType, ParentRecordId, PersonAccount__c, Type, Purpose__c from AssociatedLocation where ParentRecordId =: accountIdList]){
            if(accountIdLocationListMap.containsKey(associatedLoc.ParentRecordId)){
            	accountIdLocationListMap.get(associatedLoc.ParentRecordId).add(associatedLoc);
            }else{
                accountIdLocationListMap.put(associatedLoc.ParentRecordId, new List<AssociatedLocation>{associatedLoc});    
            }	    
        }
        
        //collection of all existing Relationship Roles (for both Account-Account and Contact-Contact Relationships)
        Map<string, FinServ__ReciprocalRole__c> RelCodeReciprocalRoleMap = new  Map<string, FinServ__ReciprocalRole__c>();  
        for(FinServ__ReciprocalRole__c role : [select id, CDH_Relationship_Code__c, CDH_Relationship_Code_Inverse__c, Name from FinServ__ReciprocalRole__c]){
        	string uniqueKey = role.CDH_Relationship_Code__c+'-'+role.CDH_Relationship_Code_Inverse__c; 
            RelCodeReciprocalRoleMap.put(uniqueKey, role);
        }
        
        Map<Id, List<FinServ__AccountAccountRelation__c>> accountIdAccAccRelListMap = new  Map<Id, List<FinServ__AccountAccountRelation__c>>();
        List<Id> accIdList = new List<Id>();
        for(FinServ__AccountAccountRelation__c accRel : [select id, FinServ__Account__c, FinServ__RelatedAccount__c, FinServ__Role__c, FinServ__Role__r.CDH_Relationship_Code__c,FinServ__Role__r.CDH_Relationship_Code_Inverse__c from FinServ__AccountAccountRelation__c where (FinServ__Account__c =:accountIdList OR FinServ__RelatedAccount__c =:accountIdList)]){
            if(accRel.FinServ__Account__c != null){
            	accIdList.add(accRel.FinServ__Account__c);    
            }else if(accRel.FinServ__RelatedAccount__c != null){
            	accIdList.add(accRel.FinServ__RelatedAccount__c);    
            }    
            for(Id acId : accIdList){
                if(accountIdAccAccRelListMap.containsKey(acId)){
                    accountIdAccAccRelListMap.get(acId).add(accRel);
                }else{
                    accountIdAccAccRelListMap.put(acId, new List<FinServ__AccountAccountRelation__c>{accRel});    
                }	    
            }
        }
        
        Map<Id, List<FinServ__ContactContactRelation__c>> accountIdConConRelListMap = new  Map<Id, List<FinServ__ContactContactRelation__c>>();
        List<Id> conIdList = new List<Id>();
        for(FinServ__ContactContactRelation__c conRel : [select id, FinServ__Contact__c, FinServ__RelatedContact__c, FinServ__Role__c, FinServ__Role__r.CDH_Relationship_Code__c,FinServ__Role__r.CDH_Relationship_Code_Inverse__c from FinServ__ContactContactRelation__c where (FinServ__Contact__c =:personContactIdAccIdMap.keySet() OR FinServ__RelatedContact__c =:personContactIdAccIdMap.keySet())]){
            if(conRel.FinServ__Contact__c != null){
            	conIdList.add(conRel.FinServ__Contact__c);    
            }else if(conRel.FinServ__RelatedContact__c != null){
            	conIdList.add(conRel.FinServ__RelatedContact__c);    
            }  
            for(Id conId : conIdList){
                Id AccountIdForCCR = personContactIdAccIdMap.get(conId);
                if(accountIdConConRelListMap.containsKey(AccountIdForCCR)){
                    accountIdConConRelListMap.get(AccountIdForCCR).add(conRel);
                }else{
                    accountIdConConRelListMap.put(AccountIdForCCR, new List<FinServ__ContactContactRelation__c>{conRel});    
                }	   
            }
        }
        
        
        List<ContactPointPhone> existingPhoneList = new List<ContactPointPhone>();
        List<ContactPointEmail> existingEmailList = new List<ContactPointEmail>();
        List<AssociatedLocation> existingAssociatedLocationList = new List<AssociatedLocation>();
        List<ContactPointPhone> insertPhoneList = new List<ContactPointPhone>();
        List<ContactPointPhone> deletePhoneList = new List<ContactPointPhone>();
        List<ContactPointEmail> insertEmailList = new List<ContactPointEmail>();
        List<ContactPointEmail> deleteEmailList = new List<ContactPointEmail>();
        List<Schema.Location> insertLocationList = new List<Schema.Location>();
        List<sObject> insertRelationsipsList = new List<sObject>();
        List<sObject> deleteRelationsipsList = new List<sObject>();

        //processing the related records - Contact Point Phone/Email, Location and Relationships 
        for(CleanupRecord__c crR : records ){

            string cdhId = crR?.Identifier__c;
            Account rec = cdhIdAllAccountMap?.get(cdhId);
           if(rec!= null){
                wsPartySearchService.RetrieveResponseType response = cdhIdRetrieveResponseMap.get(cdhId);
                existingPhoneList = accountIdContactPointPhoneListMap.get(rec.id);
                existingEmailList = accountIdContactPointEmailListMap.get(rec.id);
                existingAssociatedLocationList = accountIdLocationListMap.get(rec.id);
                //Emails
                if(crR.Level_Of_Details__c =='EMAILS'|| crR.Level_Of_Details__c =='ALL'){
                    List<wsPartyManageParty.PartyEmailType> PartyEmail = response?.Party?.contact?.PartyEmail;
                     Map<String, List<ContactPointEmail>> returnedEmailListMap = AmFam_CleanupRecordBatchHelper.createContactPointEmail(PartyEmail, rec, existingEmailList);
                     if(returnedEmailListMap != null){
                        if(returnedEmailListMap?.get('Insert') != null){
                            insertEmailList.addAll(returnedEmailListMap?.get('Insert'));    
                        }
                        if(returnedEmailListMap?.get('Delete')!= null){
                            deleteEmailList.addAll(returnedEmailListMap?.get('Delete'));    
                        }
                     }    
                }
                
                //Phones
                if(crR.Level_Of_Details__c =='PHONES'|| crR.Level_Of_Details__c =='ALL'){
                    List<wsPartyManageParty.PartyPhoneType> PartyPhone = response?.Party?.contact?.PartyPhone;
                    Map<String, List<ContactPointPhone>> returnedPhoneListMap =  AmFam_CleanupRecordBatchHelper.createContactPointPhone(PartyPhone, rec, existingPhoneList);
                    if(returnedPhoneListMap != null){
                        if(returnedPhoneListMap?.get('Insert') != null){
                            insertPhoneList.addAll(returnedPhoneListMap?.get('Insert'));    
                        }
                        if(returnedPhoneListMap?.get('Delete')!= null){
                            deletePhoneList.addAll(returnedPhoneListMap?.get('Delete'));    
                        }
                    }
                }
                //Locations
                if(crR.Level_Of_Details__c =='ADDRESSES'|| crR.Level_Of_Details__c =='ALL'){
                    
                    List<wsPartyManageParty.PartyAddressType> PartyAddress = response?.Party?.contact?.PartyAddress;   
                    List<Schema.Location> returnedLocList = AmFam_CleanupRecordBatchHelper.createLocationRecords(PartyAddress, rec, existingAssociatedLocationList);
                    if(returnedLocList.size()>0)insertLocationList.addAll(returnedLocList);
                }
                
                //Relationships
                if(crR.Level_Of_Details__c =='PARTY RELATIONSHIPS' || crR.Level_Of_Details__c =='PRINCIPAL RELATIONSHIPS'|| crR.Level_Of_Details__c =='ALL'){
                    List<wsPartyManageParty.PartyRelationshipType> PartyRelationship = response?.Party?.contact?.PartyRelationship;
                    List<wsPartyManageParty.PrincipalRelationshipType> PrincipalRelationship = response?.Party?.contact?.PrincipalRelationship;
                    Map<String, List<SObject>> returnedRelationshipMap = AmFam_CleanupRecordBatchHelper.createRelationshipRecords(PartyRelationship, PrincipalRelationship, rec, RelCodeReciprocalRoleMap, accountIdAccAccRelListMap, accountIdConConRelListMap, cdhIdAllAccountMap);
                    if(returnedRelationshipMap.size()>0 && returnedRelationshipMap.get('insert') != null)insertRelationsipsList.addAll(returnedRelationshipMap.get('insert'));  
                    if(returnedRelationshipMap.size()>0 && returnedRelationshipMap.get('delete') != null)deleteRelationsipsList.addAll(returnedRelationshipMap.get('delete'));    
                }
             }
         }
        
        Set<id> newLocationIds = new Set<id>();
        
        //remove duplicate locations
        Map<Id, String> existingLocationNameMap = new Map<Id, String>();
        Map<String, Schema.Location> insertLocationPrepMap = new Map<String, Schema.Location>();
        if(insertLocationList != null && insertLocationList?.size()>0){
            for(Schema.Location loc : insertLocationList){
                if(!insertLocationPrepMap.containsKey(loc.Name)){
                    insertLocationPrepMap.put(loc.Name, loc);
                 }
            }
            for(Schema.Location l : [select id, Name from Location where Name =:insertLocationPrepMap.keySet()]){
                newLocationIds.add(l.id);
                existingLocationNameMap.put(l.id, l.Name);
                insertLocationPrepMap.remove(l.Name);
            }    
        }
        
        try{
            insertRelationsipsList?.sort();
            deleteRelationsipsList?.sort();
            if(deletePhoneList.size()>0) delete deletePhoneList;
            if(deleteEmailList.size()>0) delete deleteEmailList;
            if(deleteRelationsipsList.size()>0) delete deleteRelationsipsList;
            Database.SaveResult[] phoneSaveResultList = Database.insert(insertPhoneList, false);
            Database.SaveResult[] emailSaveResultList = Database.insert(insertEmailList, false);
            Database.SaveResult[] relationsipsSaveResultList = Database.insert(insertRelationsipsList, false);
            Database.SaveResult[] locationSaveResultList = Database.insert(insertLocationPrepMap?.values(), false);
                 
            for (Database.SaveResult sr : locationSaveResultList) {
                    if (sr.isSuccess()) {
                        newLocationIds.add(sr.getId()); 
                    }
                    else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            
         }catch(DmlException e){
            
        } 
        
        Map<String, Id> locNameLocationIdMap = new Map<String, Id>();
        for(Schema.Location loca : [select id, Name from Location where Id =:newLocationIds ]){
        	locNameLocationIdMap.put(loca.Name, loca.id);    
        }
        
        List<AssociatedLocation> existingAssociatedLocList = new List<AssociatedLocation>();
        List<Sobject> insertAllAddressRecords = new List<Sobject>();
        List<Sobject> insertAllAssociatedLocationRecords = new List<Sobject>();
        List<Sobject> deleteAllRecords = new List<Sobject>();
        
        //Processing Associated Location and Address record (location record should have created in order to create Address and Associated Location records)
        for(CleanupRecord__c crForAddress : records ){
            string cdhId = crForAddress?.Identifier__c;
            Account rec = new Account();
            if(!cdhIdAllAccountMap.isEmpty() && cdhIdAllAccountMap != null){
            	rec = cdhIdAllAccountMap.get(cdhId);    
            }
            system.debug('rec>>>' + rec);
            if(rec?.id != null && crForAddress.Level_Of_Details__c =='ADDRESSES'|| crForAddress.Level_Of_Details__c =='ALL'){
            	wsPartySearchService.RetrieveResponseType resp = cdhIdRetrieveResponseMap?.get(cdhId);
                existingAssociatedLocList = accountIdLocationListMap != null ? accountIdLocationListMap?.get(rec?.id): null;
                List<wsPartyManageParty.PartyAddressType> PartyAddressList = resp?.Party?.contact?.PartyAddress;   
                Map<String, List<SObject>> returnedAssLocAndAddressListMap = AmFam_CleanupRecordBatchHelper.createAssociatedLocationAndAddressRecords(PartyAddressList, rec, existingAssociatedLocList, locNameLocationIdMap, existingLocationNameMap);
                if(returnedAssLocAndAddressListMap != null ){
                    insertAllAssociatedLocationRecords.addAll(returnedAssLocAndAddressListMap?.get('AssociatedLocation'));
                    insertAllAddressRecords.addAll(returnedAssLocAndAddressListMap?.get('Address'));
                    deleteAllRecords.addAll(returnedAssLocAndAddressListMap?.get('delete'));    
                }
             }
            if(cdhIdExceptionMessageMap?.containsKey(cdhId)){
               crForAddress.Exception__c =  cdhIdExceptionMessageMap?.get(cdhId);   
            }else{
               crForAddress.Processed__c= true; 
            }
            listOfCleanUpRecForUpdate.add(crForAddress);
         }
        
        //To avoid duplicate address records 
        Map<String, Schema.Address> addressMapToInsert = new Map<String, Schema.Address>();
        for(sObject addressObj : insertAllAddressRecords){
            Schema.Address a =(Schema.Address)addressObj;
            string uniqueKey = a.ParentId+'-'+a.street;
            if(!addressMapToInsert.containskey(uniqueKey)){
            	addressMapToInsert.put(uniqueKey, a);    
            }
        }
        try{
            deleteAllRecords?.sort();
            delete deleteAllRecords;
            Database.SaveResult[] saveAssociatedLocationObj = Database.insert(insertAllAssociatedLocationRecords, false);
            Database.SaveResult[] saveAddressObj = Database.insert(addressMapToInsert.values(), false);
            update listOfCleanUpRecForUpdate;
        }catch(DmlException e){
            
        } 
        
        
      
    }
     
    global void finish(Database.BatchableContext bc){
        if(!Test.isRunningTest()){
            AmFam_CleanupRecordBatchClassForHH batch = new AmFam_CleanupRecordBatchClassForHH(); 
            database.executebatch(batch, 100);      
        }
    }
}