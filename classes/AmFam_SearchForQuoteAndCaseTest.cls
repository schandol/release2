/**
*   @description: the apex class is a test class for AmFam_SearchForQuoteAndCaseController
*	@author: Anbu Chinnathambi
*   @date: 08/30/2021
*   @group: Test Class
*/

@isTest
public class AmFam_SearchForQuoteAndCaseTest {
    
     @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test getSourceSystemURL method from AmFam_SearchForQuoteAndCaseController class
    static testMethod void testgetSourceSystemURL() {
        
        test.startTest();
        Map<string, string> returnURL = AmFam_SearchForQuoteAndCaseController.getSourceSystemURL();
        test.stopTest();
        system.assert(returnURL.values()[0] != null);
    }
}