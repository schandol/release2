public class AmFam_TaskService {
    public static void incrementContactAttemptOnLead(List<Task> newTasks) {
        Set<ID> leadIds = new Set<ID>();
        for (Task tsk : newTasks) {
            String whoId = tsk.WhoId;
            if(whoId !=null && whoId.startsWith('00Q'))
            leadIds.add(tsk.WhoId);
        }
        if (leadIds.size() > 0){
            Map<ID, Lead> leads = new Map<ID, Lead>([SELECT Id, Contact_Attempts__c FROM Lead WHERE Id IN :leadIds]);
            for (Task tsk : newTasks) {
                Lead ld = leads.get(tsk.WhoId);
                ld.Contact_Attempts__c = ld.Contact_Attempts__c + 1;
                leads.put(ld.Id, ld);
            }
            update leads.values();
        }
    }
}