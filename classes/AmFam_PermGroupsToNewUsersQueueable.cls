public class AmFam_PermGroupsToNewUsersQueueable implements Queueable {

    @TestVisible
    private static Boolean chainNextQueueable = true;

    private List<Id> userRecIds;

    public AmFam_PermGroupsToNewUsersQueueable(List<Id> userRecIds)
    {
        this.userRecIds = userRecIds;
    }

    //This is setup for JitSSO only and only expects a single record
    public void execute(QueueableContext context) 
    {
        List<User> userRecs = [SELECT Id, Email, FirstName, FederationIdentifier, AmFam_User_ID__c, LastName, MiddleName, Phone, 
                                Producer_ID__c, Profile.Name, Title, UserRole.Name FROM User WHERE Id in :userRecIds];

        AmFam_UserService.updatePermissionSetGroupAssignmentsForUser(userRecs);

        if (chainNextQueueable && !Test.isRunningTest())
        {
            System.enqueueJob(new AmFam_UserAccessToAccountLeadQueueable(userRecIds));
        }
    }
}