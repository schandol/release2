public without sharing class AmFam_ProducerAccessTriggerService {


    public static void updateAccountShares(List<ProducerAccess__c> newRecords,  Map<ID, ProducerAccess__c> oldRecordsMap) {

        Set<String> producerIdemtifiers = new Set<String>();
        Set<String> accountIds = new Set<String>();
        Set<String> userIds = new Set<String>();

        for(ProducerAccess__c pa : newRecords) {
            producerIdemtifiers.add(pa.Producer_Identifier__c);
        }

        Map<String, List<Producer>> producerMap = getProducerRecordsFromProducerAccess(producerIdemtifiers);

        List<AccountShare> newAccountShareList = new List<AccountShare>();
        List<AccountShare> newAccountShareListToUpdate = new List<AccountShare>();

        //Create a list of candidate AccountShares to update
        for(ProducerAccess__c pa : newRecords) {

            List<Producer> prList = producerMap.get(pa.Producer_Identifier__c);
            Id accountId = pa.RelatedId__c;
            String reasonCode = pa.Reason__c;
            if (String.isNotEmpty(reasonCode)) {
                reasonCode = reasonCode.toLowerCase();
            }

            if(accountId != null && prList != null && prList.size() > 0) {

                for (Producer pr : prList) {
                    Id uid = pr.InternalUserId;
                    userIds.add(uid);
                    accountIds.add(accountId);

                    AccountShare newShare = new AccountShare();
                    newShare.AccountId = accountId;
                    newShare.UserOrGroupId = uid;
                    newShare.AccountAccessLevel = reasonCode == 'lead' ? 'Read' : 'Edit';
                    newShare.OpportunityAccessLevel = newShare.AccountAccessLevel;

                    newAccountShareList.add(newShare);
                }
            }
        }


        //Retrieve the exising AccountShare records and build a map indexed by key = AccountId+'-'+UserOrGroupId
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId 
                                            FROM AccountShare 
                                            WHERE UserOrGroupId IN: userIds AND AccountId IN: accountIds AND UserOrGroup.IsActive = true AND RowCause = 'Manual'];

        Map<String, id> existingShares = new Map<String, Id>();

        for (AccountShare accs : accountShares ) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            existingShares.put(key, accs.Id);
        }



        //Only process the update for existing AccountShares
        Set<String> alreadyAdded = new Set<String>();
        for (AccountShare accs : newAccountShareList) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            Id accountShareId = existingShares.get(key);

            if (accountShareId != null && !alreadyAdded.contains(key)) {
                alreadyAdded.add(key);
                accs.Id = accountShareId;
                newAccountShareListToUpdate.add(accs);
            }
        }


        //Commit to DataBase
        if (newAccountShareListToUpdate.size() > 0) {
            update newAccountShareListToUpdate;


            updateHouseholdRelatedShared(newAccountShareListToUpdate);
        }

    }



    public static void updateHouseholdRelatedShared(List<AccountShare> updatedAccountShares) {

        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();
        Map<Id, AccountShare> accSharesToUpdateMap = new Map<Id, AccountShare>();


        List<Id> accountIds = new List<Id>();
        Map<String, AccountShare> accountShareMap = new Map<String, AccountShare>();

        for (AccountShare accs : updatedAccountShares) {
            accountIds.add(accs.AccountId);

            //Build a map of updated AccountShares indexed by AccountId - UserOrGroupId
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            accountShareMap.put(key, accs);
        }


        //Get the list of AccountContactRelation records associated with the accounts with updated share access
        List<AccountContactRelation> accContactRelationList = [SELECT Id, AccountId, ContactId, Contact.AccountId FROM AccountContactRelation WHERE Account.RecordTypeId =: householdRTId AND Contact.AccountId IN: accountIds];


        //Build a map of AccountConactRelation lists indexed by Household (account side of the ACR)
        Map<Id, List<AccountContactRelation>> acrByHousehold = new Map<Id, List<AccountContactRelation>>();
        for (AccountContactRelation acr : accContactRelationList) {

            Id householdId = acr.AccountId;
            List<AccountContactRelation> acrLisrt = acrByHousehold.get(householdId);
            if (acrLisrt == null) {
                acrLisrt = new List<AccountContactRelation>();
            } 
            acrLisrt.add(acr);

            acrByHousehold.put(householdId, acrLisrt);
        }


        //Get all the AccountShares related to the Household records from the AccountContacRelationships
        List<AccountShare> householdShares = [SELECT Id, AccountId, UserOrGroupId, AccountAccessLevel FROM AccountShare WHERE RowCause = 'Manual' AND AccountId IN:  acrByHousehold.keySet() AND UserOrGroup.IsActive = true];



        //Process and find out if an Account Share needs to be updated for the household
        for (AccountShare accShare : householdShares) {

            String householdId = accShare.AccountId;
            String userOrGroup = accShare.UserOrGroupId;

            //For the Househould in the loop, get the list of ACR
            List<AccountContactRelation> acrList = acrByHousehold.get(houseHoldId);


            //For each ACR, extract the Account ID related to the Contact side of the relation (contact.AccountId)
            for (AccountContactRelation acr : acrList) {
                String contactAccount = acr.Contact.AccountId;

                //Build a KEY with the account extracted and the userOrGroup from the household share
                String key = contactAccount + '-' + userOrGroup;

                //Find the AccountShare associated to that KEY (if it exists). If so, then the Household Share should be update with the access level of the Account Share just extracted
                AccountShare accShareToCopy = accountShareMap.get(key);
                if (accShareToCopy != null && !accSharesToUpdateMap.containsKey(accShare.id)) {
                    accShare.AccountAccessLevel = accShareToCopy.AccountAccessLevel;
                    accShare.OpportunityAccessLevel = accShare.AccountAccessLevel;
                    accSharesToUpdateMap.put(accShare.id, accShare);
                }

            }

        }

        if (accSharesToUpdateMap.values().size() > 0) {
            update accSharesToUpdateMap.values();
        }


    }




    public static void createNewAccountShares(List<ProducerAccess__c> newRecords) {


        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();

        Set<String> producerIdemtifiers = new Set<String>();
        Set<String> accountIds = new Set<String>();
        Set<String> userIds = new Set<String>();

        for(ProducerAccess__c pa : newRecords) {
            producerIdemtifiers.add(pa.Producer_Identifier__c);
        }

        Map<String, List<Producer>> producerMap = getProducerRecordsFromProducerAccess(producerIdemtifiers);

        List<AccountShare> newAccountShareList = new List<AccountShare>();
        List<AccountShare> newAccountShareListToInsert = new List<AccountShare>();

        //Create a list of candidate AccountShares to update
        for(ProducerAccess__c pa : newRecords) {

            List<Producer> prList = producerMap.get(pa.Producer_Identifier__c);
            Id accountId = pa.RelatedId__c;
            String reasonCode = pa.Reason__c;
            if (String.isNotEmpty(reasonCode)) {
                reasonCode = reasonCode.toLowerCase();
            }

            if(accountId != null && prList != null && prList.size() > 0) {

                for (Producer pr : prList) {
                    Id uid = pr.InternalUserId;
                    userIds.add(uid);
                    accountIds.add(accountId);

                    AccountShare newShare = new AccountShare();
                    newShare.AccountId = accountId;
                    newShare.UserOrGroupId = uid;
                    newShare.AccountAccessLevel = reasonCode == 'lead' ? 'Read' : 'Edit';
                    newShare.OpportunityAccessLevel = newShare.AccountAccessLevel;
                    
                    newAccountShareList.add(newShare);
                }
            }
        }


        //Retrieve the exising AccountShare records and build a set of keys = AccountId+'-'+UserOrGroupId
        List<AccountShare> accountShares = [SELECT Id, UserOrGroupId, AccountId 
                                            FROM AccountShare 
                                            WHERE UserOrGroupId IN: userIds AND AccountId IN: accountIds AND UserOrGroup.IsActive = true] ;
        Set<String> existingShares = new Set<String>();

        for (AccountShare accs : accountShares ) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            existingShares.add(key);
        }


        //Only process the insert for non existing AccountShares
        for (AccountShare accs : newAccountShareList) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            if (!existingShares.contains(key) ) {

                //Add it to avoid creating duplicates
                existingShares.add(key);

                accs.CaseAccessLevel = 'None';
                accs.OpportunityAccessLevel = accs.AccountAccessLevel; 
                accs.RowCause = 'Manual';

                newAccountShareListToInsert.add(accs);
            }
        }

        //Commit to DataBase
        if (newAccountShareListToInsert.size() > 0) {
            insert newAccountShareListToInsert;

            //Cal the InteractionSummaryService to extend the access to Interaction Summaries
            AmFam_InteractionSummaryService.grantSameAccessFromAccount(newAccountShareListToInsert);


        }


        //Get Household records related to the granted access accounts
        // ---- To get the households, we need to retrieve AccountContactRelation where the Account's RecordType is Household and the Contact's Account is on the granted access list from the current trigger
        List<AccountContactRelation> acrList = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE Account.RecordTypeId =: householdRTId AND Contact.AccountId IN: accountIds];

        //Call the AcountContactService to extend the access to Households
        AmFam_AccountContactService.extendProudcerAccessToHousehold(acrList);


    }





    public static void removeAccountShares(List<ProducerAccess__c> oldRecords, Map<ID, ProducerAccess__c> oldRecordsMap) {

        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();

        Set<String> producerIdemtifiers = new Set<String>();
        Set<String> accountIds = new Set<String>();
        Set<String> userIds = new Set<String>();

        for(ProducerAccess__c pa : oldRecords) {
            producerIdemtifiers.add(pa.Producer_Identifier__c);
        }

        Map<String, List<Producer>> producerMap = getProducerRecordsFromProducerAccess(producerIdemtifiers);

        Map<String, Set<String>> userMap = new Map<String, Set<String>>();

        List<AccountShare> newAccountShareList = new List<AccountShare>();
        List<AccountShare> newAccountShareListToDelete = new List<AccountShare>();
        Set<String> processedKeys = new Set<String>();

        for(ProducerAccess__c pa : oldRecords) {

            List<Producer> prList = producerMap.get(pa.Producer_Identifier__c);
            Id accountId = pa.RelatedId__c;

            if(accountId != null && prList != null && prList.size() > 0) {

                for (Producer pr : prList) {

                    Id uid = pr.InternalUserId;
                    userIds.add(uid);
                    accountIds.add(accountId);

                    String key = accountId + '-' + uid;

                    if (!processedKeys.contains(key)) {

                        processedKeys.add(key);

                        AccountShare newShare = new AccountShare();
                        newShare.AccountId = accountId;
                        newShare.UserOrGroupId = uid;

                        newAccountShareList.add(newShare);
                    }

                }
            }
        }

        //New way to process: batch call
        Set<String> prIdList = new Set<String>();
        List<Producer> producerList = [SELECT Id, Name, InternalUserId FROM Producer WHERE InternalUserId IN: userIds AND InternalUser.IsActive = true];        
        for (Producer pr : producerList) {
            prIdList.add(pr.Name);
            String userId = pr.InternalUserId;

            Set<String> prListForUser = userMap.get(userId);
            if (prListForUser == null) {
                prListForUser = new Set<String>();
            }

            prListForUser.add(pr.Name);
            userMap.put(userId, prListForUser);            
        }        

        if (prIdList != null && prIdList.size() > 0) {
            AmFam_RemoveSharesBatch rsb = new AmFam_RemoveSharesBatch(userMap, new List<String>(prIdList), null, accountIds, newAccountShareList, null, producerIdemtifiers) ;
            database.executebatch(rsb);
        }

    }



    //******************************************* */
    //******************************************* */
    // Utility Methods    
    //******************************************* */
    //******************************************* */

    private static String getCorporateAccountUser() {
        List<AmFam_Ids__mdt> amfamIds = [SELECT AFMIC_Corporate_UserId__c FROM AmFam_Ids__mdt WHERE DeveloperName = 'AFMIC_Corporate' LIMIT 1];

       
        if (amfamIds.size() > 0) {
            String returnId = amfamIds[0].AFMIC_Corporate_UserId__c;
            if (String.isNotEmpty(returnId)) {
                return returnId;
            }
        }

        return null;

    }

    
    private static Map<String, List<Producer>> getProducerRecordsFromProducerAccess(Set<String> producerIdentifiers) {


        Map<String, List<Producer>> producerMap = new Map<String, List<Producer>>();

        List<Producer> producerList = [SELECT Id, AccountId, ContactId, InternalUserId, Name FROM Producer WHERE Name =: producerIdentifiers AND InternalUser.IsActive = true];

        for (Producer pr : producerList) {

            List<Producer> producers = (List<Producer>) producerMap.get(pr.Name);

            if (producers == null) {
                producers = new List<Producer>();
            }


            producers.add(pr);
            producerMap.put(pr.Name, producers);
        }

        return producerMap;

    }


}