//!!! Coverage only test
@IsTest
public with sharing class AmFam_UserAccessToAcctLeadQueueableTest {

    //!!!works if you disable call to AmFam_UserService.userAccessToAccountLead(newUser);
    //in UserTriggerHandler OnAfterUpdate

    @IsTest
    public static void testUserAccessToAccountLead()
    {
        AmFam_UserUpdateFromCalloutQueueable.chainNextQueueable = false;
        AmFam_CreateUserAccountsQueueable.chainNextQueueable = false;

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();

        insert u;

        Test.startTest();

        List<Id> userList = new List<Id>{u.Id};
        AmFam_UserAccessToAccountLeadQueueable qOp = new AmFam_UserAccessToAccountLeadQueueable(userList);
        qOp.execute(null);

        Test.stopTest();
    }
}