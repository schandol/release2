global class UpdateApexNotes implements Database.AllowsCallouts 
, Database.Batchable<sObject>, Database.Stateful {

    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    global Integer failedRecords = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT ID, Contact_CDHID__c, Status, LastModifiedDate FROM Lead WHERE Contact_CDHID__c != null AND stage__c != \'Completed\''
        );
    }
    global void execute(Database.BatchableContext bc, List<LEad> scope){

        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
        API_Failure_Log__c[] errorLogs = new API_Failure_Log__c[0];

        // process each batch of records
        for (Lead lead : scope) {      
        
            try {
                ApexActServiceHelper.createChatterFeedApexNote(lead, false);
                ApexActServiceHelper.createTaskApexNotes(lead, false);
                recordsProcessed = recordsProcessed + 1;      
             } catch (Exception e) {
              API_Failure_Log__c log = failureLog.createGenericAPIFailure('ApexActService', 'ApexActService', lead.id, e.getMessage());
              errorLogs.add(log);
              failedRecords = failedRecords + 1;
            }

        }
  
        System.debug(recordsProcessed + ' records processed');
        insert errorLogs;

    }    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        // call some utility to send email
        sendMessage(job, recordsProcessed, failedRecords);
    }    
    
    global static void sendMessage(AsyncApexJob job, Integer recordsProcessed, Integer failedRecords){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{'SALESFORCE-LEADS-L@amfam.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Update Apex Notes ' + job.Status);
        mail.setPlainTextBody('records processed ' + recordsProcessed + '\n records failed ' + failedRecords);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        
    }
}