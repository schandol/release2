@isTest
private class AmFam_LeadAssociationToProducerTest {
    @TestSetup
    static void bypassProcessBuilderSettings(){
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }

    @isTest
    static void leadAssociationToProducersTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator'];
        User integrationUser = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Integration',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert integrationUser;
        System.runAs(integrationUser) {
            Test.setMock(HttpCalloutMock.class, new AmFam_LeadAssociationToProducerTestMock());
            Test.startTest();
            wsPartyManage.lpeCallout = false;
            Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 2);
            List<Lead> leads = [SELECT Id, OwnerId, Agency__c, Agency__r.Producer_ID__c FROM Lead WHERE Agency__c = :accts];
            Lead l = Leads[0];
            l.CDHID__c = 'Test123';
            l.Party_Level__c = 'Entrant';
            update l;
            Lead l1 = Leads[1];
            l.CDHID__c = 'Test1234';
            l.Party_Level__c = 'Contact';
            update l1;
            Test.stopTest();
            List<API_Failure_Log__c> logs = [SELECT Id, Error_Message__c FROM API_Failure_Log__c];
            System.debug(logs);
            System.assertEquals(0, logs.size());
        }
    }

    @isTest
    static void leadAssociationToProducersQueueableTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        AmFam_UserService.callProducerUpdateAPI = false;
        insert agent;
        System.runAs(agent) {
            AmFam_LeadAssociationToProducerTestMock mock = new AmFam_LeadAssociationToProducerTestMock();
            mock.isError = false;
            Test.setMock(HttpCalloutMock.class, mock);
            Test.startTest();
            wsPartyManage.lpeCallout = false;
            Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 1);
            List<Lead> leads = [SELECT Id, OwnerId, Agency__c, Agency__r.Producer_ID__c FROM Lead WHERE Agency__c = :accts];
            Lead l = Leads[0];
            l.CDHID__c = 'Test123';
            l.Party_Level__c = 'Entrant';
            update l;
            System.enqueueJob(new AmFam_LeadAssociationToProducerQueueable(l));
            Test.stopTest();
            List<API_Failure_Log__c> logs = [SELECT Id, Error_Message__c FROM API_Failure_Log__c];
            System.assertEquals(0, logs.size());
        }
    }

    @isTest
    static void leadAssociationToProducersErrorTest() {
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        User agent = new User (alias = 'sagent', email='test12456@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test12456',
                                    firstname='User', lastname='Agent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test12456@xyz.com.test',
                                    AmFam_User_ID__c='test12456'
                                );
        AmFam_UserService.callProducerAPI = false;
        insert agent;
        System.runAs(agent) {
            AmFam_LeadAssociationToProducerTestMock mock = new AmFam_LeadAssociationToProducerTestMock();
            mock.isError = true;
            Test.setMock(HttpCalloutMock.class, mock);
            Test.startTest();
            wsPartyManage.lpeCallout = false;
            Account[] accts = AmFam_TestDataFactory.createAccountsWithLeads(1, 1);
            List<Lead> leads = [SELECT Id, OwnerId, Agency__c, Agency__r.Producer_ID__c FROM Lead WHERE Agency__c = :accts];
            Lead l = Leads[0];
            l.CDHID__c = 'Test123';
            l.Party_Level__c = 'Entrant';
            update l;
            System.enqueueJob(new AmFam_LeadAssociationToProducerQueueable(l));
            Test.stopTest();
            List<API_Failure_Log__c> logs = [SELECT Id, Error_Message__c FROM API_Failure_Log__c];
            System.assertEquals(1, logs.size());
        }
    }
}