public class AmFam_LeadService {
    public static void lockLeadChangesAtContactPartyLevel(List<Lead> newLeadList, Map<ID,Lead> oldLeadMap) {
        List<Schema.FieldSetMember> fields = Schema.SObjectType.Lead.fieldSets.getMap().get('Locked_Fields_For_Contact_Party_Level').getFields();
        for (Lead obj : newLeadList) {
            String fieldsChanged = '';
            // Check if Party Level is being changed as part of this update, and ignore if so (Override Address scenario)
            if (oldLeadMap.get(obj.Id).Party_Level__c == 'Contact' && obj.Party_Level__c == 'Contact') {
                for (Schema.FieldSetMember fsm : fields) {
                    if (obj.get(fsm.getFieldPath()) != oldLeadMap.get(obj.Id).get(fsm.getFieldPath())) {
                        if (fieldsChanged == '') {
                            fieldsChanged = fsm.getLabel();
                        } else {
                            String str = ', ' + fsm.getLabel();
                            fieldsChanged += str;
                        }
                    }
                }
                if (fieldsChanged != '') {
                    obj.addError('You cannot make edits to the ' + fieldsChanged + ' field(s) once the lead has a Contact party level.');
                }
            }
        }
    }

    public static void checkLeadOwnerChangeBelongsToSameAgency (List<Lead> newLeads, Map<ID, Lead> newLeadsMap, Map<ID, Lead> oldLeadsMap) {
        Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
        if (p.Name != 'System Administrator' || Test.isRunningTest()) {
            // Check if owner is changed.
            Boolean ownerChanged = false;
            Set<Id> accId = new Set<Id>();
            for(Lead l : newLeads) {
                if(oldLeadsMap.get(l.id).OwnerId !=newLeadsMap.get(l.id).OwnerId) {
                    ownerChanged = true;
                    accId.add(l.Agency__c);
                }
            }
            //if Owner is Changed then only then proceed further
            if (ownerChanged) {
                String error = 'Leads cannot be assigned to a user who is not associated with the owning agency';
                if (accId.size() == 1) {
                    // Get all contacts Fed ID with related to accounts
                    User owner = [SELECT FederationIdentifier FROM User WHERE Id = : newLeads[0].OwnerId];
                    List<AccountContactRelation> contacts = [SELECT Id FROM AccountContactRelation
                                                            WHERE AccountId IN :accId AND Contact.User_Federation_ID__c =: owner.FederationIdentifier];
                    if (contacts.size() > 0) {
                        // Let update happen. No error
                    } else {
                        for(Lead l : newLeads) {
                            l.addError(error);
                        }
                    }
                } else {
                    for(Lead l : newLeads) {
                        l.addError(error);
                    }
                }
            }
        }
    }

    public static void setSharingForOwnerChangeLeads (List<Lead> newLeads, Map<Id,Lead> oldLeadsMap) {
        List<Lead> leadsWithOwnerChanges = new List<Lead>();
        for (Lead l : newLeads) {
            if (l.OwnerId != oldLeadsMap.get(l.Id).OwnerId) {
                leadsWithOwnerChanges.add(l);
            }
        }
        
        if (leadsWithOwnerChanges.size() > 0) {
            AmFam_LeadService.setSharingAccessForNewLeads(leadsWithOwnerChanges);
        }
    }
    
    public static void setSharingAccessForNewLeads(List<Lead> newLeads)
    {
        if (newLeads.size() <= 1)
        {
            List<LeadShare> newLeadShares = AmFam_LeadService.generateLeadSharesForNewLeads(newLeads);
            if (!newLeadShares.isEmpty())
            {
                insert newLeadShares;
            }
        }
        else 
        {
            database.executebatch(new AmFam_SharingAccessForNewLeadsBatch(newLeads));
        }
    }

    public static List<LeadShare> generateLeadSharesForNewLeads(List<Lead> newLeads)
    {
        //old is bulked weird so right this like normal and retrofit
        //A Lead should be shared with all Users that have access to the Producer Id of the Lead’s Agency

        List<LeadShare> newLeadShares = new List<LeadShare>();

        List<Id> agencyIds = new List<Id>();
        Set<Id> leadIds = new Set<Id>();
        for (Lead l : newLeads) 
        {
            leadIds.add(l.Id);
            agencyIds.add(l.Agency__c);
        }

        List<Account> agencyAccounts = [SELECT Id,Producer_ID__c FROM Account WHERE Id in :agencyIds];

        Map<Id,String> agencyProducerMap = new Map<Id,String>();
        Set<String> leadProducerIds =  new Set<String>();
        for (Account a : agencyAccounts)
        {
            leadProducerIds.add(a.Producer_ID__c);
            agencyProducerMap.put(a.Id,a.Producer_ID__c);
        }

        List<Producer> producers = [SELECT Id,Name,InternalUserId FROM Producer WHERE Name in :leadProducerIds];

        //want to protect against trying to use an inactive User
        List<Id> userIds = new List<Id>();
        for (Producer p : producers)
        {
            userIds.add(p.InternalUserId);
        }

        List<User> rawUsers = [SELECT Id FROM User WHERE Id in :userIds AND IsActive = true];

        Set<Id> activeUserIds = new Set<Id>();
        for (User u : rawUsers)
        {
            activeUserIds.add(u.Id);
        }

        Map<String, Set<Id>> producerIdUserMap = new Map<String, Set<Id>>();
        for (Producer p : producers)
        {
            if (activeUserIds.contains(p.InternalUserId))
            {
                if (!producerIdUserMap.containsKey(p.Name))
                {
                    producerIdUserMap.put(p.Name,new Set<Id>());
                }

                producerIdUserMap.get(p.Name).add(p.InternalUserId);
            }
        }

        List<LeadShare> existingLeadShareRecords = [SELECT LeadId, RowCause, UserOrGroupId FROM LeadShare WHERE LeadId IN: leadIds];
        Map<Id,Set<Id>> usersWithAccessAlready = new Map<Id,Set<Id>>();
        for (LeadShare ls : existingLeadShareRecords) {
            if (!usersWithAccessAlready.containsKey(ls.LeadId)) {
                usersWithAccessAlready.put(ls.LeadId,new Set<Id>());
            }
            usersWithAccessAlready.get(ls.LeadId).add(ls.UserOrGroupId);
        }

        for (Lead l : newLeads) 
        {
            String leadProducerId = agencyProducerMap.get(l.Agency__c);
            Set<Id> users = producerIdUserMap.get(leadProducerId);
            if (users != null)
            {
                for (Id uid : users)
                {
                    if (!usersWithAccessAlready.get(l.Id).contains(uid)) {
                        LeadShare leadShare = new LeadShare();
                        leadShare.LeadAccessLevel = 'Edit';
                        leadShare.LeadId = l.Id;
                        leadShare.UserOrGroupId = uid;
                        leadShare.RowCause = 'Manual';
                        newLeadShares.add(leadShare);
                    }
                }
            }
        }

        return newLeadShares;
    }

    public static void leadStageDispositionValues(List<Lead> leads) {
        for (Lead l : leads) {
            if (l.Stage__c == 'Quote' && l.Disposition_Reason__c == null) {
                l.Disposition_Reason__c = 'Quoted';
            }
            if (l.Stage__c == 'New' && l.Disposition_Reason__c == null) {
                l.Disposition_Reason__c = 'New';
            }
        }
    }

    public static void setValueOnCreation(List<Lead> leads) {
        for (Lead rec : leads) {
            // If lead being inserted, we are setting Source system Key as SF
            if (String.isBlank(rec.Source_System_Key__c)) {
                rec.Source_System_Key__c = rec.id;
            }
        }
    }

    public static void markImportedLeadsLPEStatus(List<Lead> leads) {
        for (Lead rec : leads) {
            // If lead being inserted via import wizard
            if (System.URL.getCurrentRequestUrl().getPath() != '/aura' && String.isBlank(rec.LPE_Status__c)) {
                rec.LPE_Status__c = 'Created';
            }
        }
    }
    
    public static void extendPCIFYToLead() {
        //The below code extending PCIFY to Lead object
        if (pcify.Manager.isOnline('Lead')) { 
            pcify.Processor.maskCreditCards(Trigger.new, pcify.Manager.getMaskFields('Lead'),'Lead');
        }
    }
}