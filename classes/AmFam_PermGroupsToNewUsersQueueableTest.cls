@IsTest
public with sharing class AmFam_PermGroupsToNewUsersQueueableTest {
    @IsTest
    private static void testPermGroupsToNewUser()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();
        u.Producer_ID__c = '44444';
        insert u;

        //List<User> userList = new List<User>{u};
        Test.startTest();
        
        List<Id> userIds = new List<Id>();
        userIds.add(u.Id);
        
        System.enqueueJob(new AmFam_PermGroupsToNewUsersQueueable(userIds));

        Test.stopTest();

        List<PermissionSetAssignment> psas = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :u.Id];
        System.debug('PSAS:'+psas);
        System.assert(psas.size() > 0);
    }

}