public with sharing class AmFam_LeadRefreshQuickActionController {
    
    @AuraEnabled
    public static Lead refreshLead(String recordId) {
        System.debug('recordId: ' + recordId);
        Lead lead = new Lead();
        try{
            lead = [Select id, CDHID__c, Party_Version__c, Party_Level__c, Contact_CDHID__c, Additional_Phone_1__c, Additional_Phone_2__c, Additional_Phone_3__c, Phone, Email,
                                Additional_Email_1__c, Additional_Email_2__c
                                FROM Lead where id =: recordId LIMIT 1]; 
            lead = wsPartySearchHelper.refreshLead(lead);
            update lead;
            return lead;
        }catch (Exception ex) {
        	throw new AuraHandledException('Message: ' + ex.getMessage());
        }
   }
}