public class AmFam_CleanupRecordBatchHelper {
    
    
    /*
    Makes a callout to PartySearchService for @CDHID with a static list of levelOfDetail.
    @args CDHID to retrieve
    */
    public static wsPartySearchService.RetrieveResponseType retrieveParty(String cdhId) {
    
        wsPartySearchServiceCallout.PartySearchServiceSOAP partySearchService = new wsPartySearchServiceCallout.PartySearchServiceSOAP();
        wsPartySearchService.RetrieveRequestType request = new wsPartySearchService.RetrieveRequestType();
        wsPartySearchService.RetrieveCriteriaType criteriaType = new wsPartySearchService.RetrieveCriteriaType();
        criteriaType.partyIdentifier = cdhId;
        criteriaType.LevelOfDetail = new List<String>();
        criteriaType.LevelOfDetail.add('DEMOGRAPHIC_INFORMATION');
        criteriaType.LevelOfDetail.add('ADDITIONAL_INFORMATION');
        criteriaType.LevelOfDetail.add('IDENTITY_INFORMATION');
        criteriaType.LevelOfDetail.add('PARTY_RELATIONSHIPS');
        criteriaType.LevelOfDetail.add('PRINCIPAL_RELATIONSHIPS');
        criteriaType.LevelOfDetail.add('EMPLOYMENT');
        criteriaType.LevelOfDetail.add('ADDRESSES');
        criteriaType.LevelOfDetail.add('PHONES');
        criteriaType.LevelOfDetail.add('EMAILS');
        request.RetrieveCriteria = criteriaType;
        
        wsPartySearchService.RetrieveResponseType response = partySearchService.retrieveParty_Http(request);
        
        return response;
    }
    
    public static Map<Account, List<string>> processAccountDetails(Account rec, wsPartySearchService.RetrieveResponseType partySearchResponse, string cdhId){
        
        Account acc = rec!=null? rec : new Account();
        wsPartyManageParty.PersonType person = partySearchResponse?.Party?.Contact?.Person;
        wsPartyManageParty.OrganizationType organization = partySearchResponse?.Party?.Contact?.Organization;
        wsPartyManageParty.PartyAdditionalInfoType partyAddInfo = partySearchResponse?.Party?.Contact?.PartyAdditionalInfo;
        List<wsPartyManageParty.PartyRelationshipType> partyRelationship = partySearchResponse?.Party?.contact?.PartyRelationship;
        List<wsPartyManageParty.PrincipalRelationshipType> principalRelationship = partySearchResponse?.Party?.contact?.PrincipalRelationship;
        Map<Account, List<string>> returnMap = new Map<Account, List<string>>();
        if(partySearchResponse.Party != null && cdhId == partySearchResponse?.Party?.partyIdentifier){
            
            acc.Party_Version__c = partySearchResponse.Party.partyVersion;
            acc.CDHID__c = partySearchResponse.Party.partyIdentifier;
            acc.FinServ__SourceSystemId__c = partySearchResponse?.Party?.SourceSystemInformation?.sourceSystemKey;
        
            if(partySearchResponse?.Party?.Contact != null) {
                
                if(partySearchResponse?.Party?.Contact?.PartyTypeCode == 'P'){
                    
                    if(person != null){
                      wsPartyManageParty.PersonDemographicInfoType info = person.PersonDemographicInfo;
                      wsPartyManageParty.PersonIdentityInfoType identifyInfo = person.PersonIdentityInfo;
                      wsPartyManageParty.PersonAdditionalInfoType personAddInfo = person.PersonAdditionalInfo;
                      wsPartyManageParty.EmploymentOccupationType employmentInfo = person.EmploymentOccupation;
                      wsPartyManageParty.DriversLicenseType driversLicenseInfo = identifyInfo.DriversLicense;
                      
                        if(info != null){
                            acc.FirstName = info.Name?.firstName;
                            acc.MiddleName = info?.Name?.middleName;
                            acc.LastName = info?.Name?.lastName;
                            acc.PersonBirthdate = info?.birthDate != null ? Date.valueOf(info?.birthDate):null;
                            acc.FinServ__Gender__pc = info?.Gender;
                            acc.Deceased__pc = info?.DeceasedIndicator == 'Y'? true: false;
                            acc.Deceased_Date__pc = info?.deceasedDate !=null? Date.valueOf(info?.deceasedDate):null;
                        }
                        if(identifyInfo != null){
                            if(identifyInfo.socialSecurityNumberExists != null){
                                acc.Last_4_SSN__c = identifyInfo?.last4ssn;    
                            }
                            if(identifyInfo.feinExists != null){
                                acc.Last_4_FEIN__c = identifyInfo?.last4fein;    
                            }
                            
                            if(driversLicenseInfo != null){
                                acc.Driver_s_License_Number__c  =  driversLicenseInfo?.licenseNumber!=null?driversLicenseInfo?.licenseNumber:'';
                                acc.Driver_s_License_State__c  =  driversLicenseInfo?.licenseState;
                                if(driversLicenseInfo?.OriginalLicenseDate != null){
                                    acc.Drivers_License_Issue_Month__c  =  driversLicenseInfo?.OriginalLicenseDate?.licenseMonthString;
                                    acc.Drivers_License_Issue_Year__c  =  driversLicenseInfo?.OriginalLicenseDate?.licenseYearString;
                                }
                            }
                        }
                        if(personAddInfo != null){
                            acc.Employment_Status__pc = personAddInfo?.EmploymentStatus;    
                        }
                        if(employmentInfo != null){
                            acc.FinServ__CurrentEmployer__pc = employmentInfo?.employerName;
                            acc.Occupation_Category__pc = employmentInfo?.Occupation?.OccupationCategory;
                            acc.Occupation__pc = employmentInfo?.Occupation?.OccupationCode?.code;
                            if(acc.Occupation__pc.startsWith('OTH')){
                                acc.Occupation_Comment__pc = employmentInfo?.Occupation?.OccupationCode?.description;    
                            }
                        }
                    }
                    acc.FEIN__c = null;
                    acc.SSN__c = null;
                    acc.SSN__pc = null;
                    acc.RecordTypeId = AmFam_Utility.getPersonAccountRecordType(); 
                    
                }else if(partySearchResponse?.Party?.Contact?.PartyTypeCode == 'O'){
                    
                    if(organization != null){
                        wsPartyManageParty.OrganizationDemographicInfoType orgInfo = organization.OrganizationDemographicInfo;
                        wsPartyManageParty.OrganizationIdentityInfoType orgIdentityInfo = organization.OrganizationIdentityInfo;
                        if(orgInfo != null){
                            acc.Name = orgInfo?.Name?.organizationName;
                            acc.Form_of_Business__c = orgInfo?.FormOfBusiness?.FormOfBusinessDescription;
                        }
                        if(orgIdentityInfo?.TIN?.TinType == 'S'){
                            acc.Last_4_SSN__c = orgIdentityInfo?.TIN?.tinNumber;
                            acc.Last_4_FEIN__c = null;
                        }else if(orgIdentityInfo?.TIN?.TinType == 'F'){
                            acc.Last_4_FEIN__c = orgIdentityInfo?.TIN?.tinNumber;
                            acc.Last_4_SSN__c = null;
                        }
                    }
                    acc.FEIN__c = null;
                    acc.SSN__c = null;
                    acc.RecordTypeId = AmFam_Utility.getAccountBusinessRecordType();
                }
                
                if(partyAddInfo != null){
                    acc.Greeting__c = partyAddInfo?.greetingName;
                    acc.Mailing_Name__c = partyAddInfo?.mailingName;
                    acc.Primary_Spoken_Language__c = partyAddInfo?.PrimaryLanguage?.code;
                    acc.Secondary_Spoken_Language__c = partyAddInfo?.SecondaryLanguage?.code;
                    acc.Written_Language__c = partyAddInfo?.WrittenLanguage;
                    acc.Primary_Spoken_Language_Other__c = partyAddInfo?.PrimaryLanguage?.description == 'OTHER' ? partyAddInfo?.PrimaryLanguage?.description: '';
                    acc.Secondary_Spoken_Language_Other__c = partyAddInfo?.SecondaryLanguage?.description == 'OTHER' ? partyAddInfo?.SecondaryLanguage?.description: '';
                 }
             }
             
             List<string> relatedPartyIdentifiers = parseRelationShips(partyRelationship, principalRelationship);
             returnMap.put(acc, relatedPartyIdentifiers);
    	}    
        return returnMap;
    }
    
    public static List<String> parseRelationShips(List<wsPartyManageParty.PartyRelationshipType> PartyRelationship, List<wsPartyManageParty.PrincipalRelationshipType> PrincipalRelationship) {
            List<String> relatedPartyIdentifierList = new List<String>();
            if(PartyRelationship != null){
                for(wsPartyManageParty.PartyRelationshipType partyRel : PartyRelationship){
                    String relatedPartyIdentifier = partyRel?.RelatedParty?.partyIdentifier;
                    relatedPartyIdentifierList.add(relatedPartyIdentifier);
                }
            }
            if(PrincipalRelationship != null){
                for(wsPartyManageParty.PrincipalRelationshipType principalRel : PrincipalRelationship){
                    String relatedPartyIdentifier = principalRel?.RelatedParty?.partyIdentifier;
                    relatedPartyIdentifierList.add(relatedPartyIdentifier);
                }
            }
            return relatedPartyIdentifierList;	    
    }
    
    public static Map<String, List<ContactPointPhone>> createContactPointPhone(List<wsPartyManageParty.PartyPhoneType> PartyPhone, Account acc, List<ContactPointPhone> existingContactPointPhoneList) {
        
            //Existing phone and usages for this account
            Map<String, List<ContactPointPhone>> returnPhoneMap = new Map<String, List<ContactPointPhone>>();
            Map<string, List<string>> existingPhoneAndUsageListMap = new Map<string, List<string>>();
            Map<string, ContactPointPhone> existingNumberContactPointPhoneMap = new Map<string, ContactPointPhone>();
            if(existingContactPointPhoneList != null && existingContactPointPhoneList?.size()>0 ){
                for(ContactPointPhone existingPhone : existingContactPointPhoneList){
                    existingNumberContactPointPhoneMap.put(existingPhone.TelephoneNumber+existingPhone.UsageType, existingPhone);
                    if(existingPhoneAndUsageListMap.containsKey(existingPhone.TelephoneNumber)){
                        List<String> existingUsages = existingPhoneAndUsageListMap.get(existingPhone.TelephoneNumber);
                        if(!existingUsages.contains(existingPhone.UsageType)){
                            existingPhoneAndUsageListMap.get(existingPhone.TelephoneNumber).add(existingPhone.UsageType);   
                        }
                        }else{
                            existingPhoneAndUsageListMap.put(existingPhone.TelephoneNumber, new List<string>{existingPhone.UsageType});    
                        }	    
                    }    
            }else {existingPhoneAndUsageListMap = null;}
            
            List<ContactPointPhone> newPhoneList = new List<ContactPointPhone>();
            if (PartyPhone != null) {
                for (wsPartyManageParty.PartyPhoneType phone : PartyPhone) {
                    if (phone.Phone != null) {
                        Map<string,string> phoneUsageAndCommentsMap = parsePhoneUsageAndComments(phone);
                        for(string usg : phoneUsageAndCommentsMap.keySet()){
                            string comment = phoneUsageAndCommentsMap.get(usg);
                            if (phone.Phone.PhoneNumber != null) {
                                string phoneNum = parsePhoneNumber(phone);
                                if(existingPhoneAndUsageListMap != null ? !existingPhoneAndUsageListMap?.containsKey(phoneNum) && 
                                   existingPhoneAndUsageListMap?.get(phoneNum) != null ? !existingPhoneAndUsageListMap?.get(phoneNum)?.contains(usg): true : true){
                                    ContactPointPhone cpp = new ContactPointPhone();
                                    cpp.ParentId = acc.id;
                                    cpp.TelephoneNumber = phoneNum;
                                    cpp.UsageType = usg;
                                    cpp.Usage_Type_Comment__c = string.isNotBlank(comment)?comment:'';
                                    cpp.IsPrimary = phone?.TrustManagedPrimaryIndicator == 'Y'?true : false;
                                    cpp.BestTimeToContactTimezone__c = phone?.Phone?.TimeZone;
                                    cpp.ExtensionNumber = phone?.Phone?.extension!=null? phone?.Phone?.extension :'';
                                    cpp.Special_Instruction__c = phone?.specialInstructions!=null? phone?.specialInstructions :'';
                                    newPhoneList.add(cpp);
                                }else if(existingNumberContactPointPhoneMap != null && existingPhoneAndUsageListMap != null && existingPhoneAndUsageListMap?.containsKey(phoneNum) && existingPhoneAndUsageListMap?.get(phoneNum)?.contains(usg)){
                                    existingNumberContactPointPhoneMap.remove(phoneNum+usg);
                                }
                            }
                        }
                    }
                }         
            }
            if(newPhoneList.size()>0) returnPhoneMap.put('Insert', newPhoneList);
            if(existingNumberContactPointPhoneMap.values().size()>0) returnPhoneMap.put('Delete',existingNumberContactPointPhoneMap.values());
            return returnPhoneMap;
    }
    
    private static String parsePhoneNumber(wsPartyManageParty.PartyPhoneType currentPhone) {
        return (currentPhone?.Phone?.PhoneNumber?.areaCode == null ? '' : currentPhone?.Phone?.PhoneNumber?.areaCode) 
            + (currentPhone?.Phone?.PhoneNumber?.number_x == null ? '' : currentPhone?.Phone?.PhoneNumber?.number_x);
    }

    private static Map<string,string> parsePhoneUsageAndComments(wsPartyManageParty.PartyPhoneType currentPhone) {
        String usageType = '';
        String usageComment = '';
        Map<string,string> phoneUsageAndComments = null;
        for (wsPartyManageParty.PartyPhoneUsageAndDescriptionType usageAndDescription : currentPhone.PartyPhoneUsageAndDescription) {
            if(usageAndDescription?.PartyPhoneUsage != null){
            	phoneUsageAndComments = new Map<string,string>();
                usageType = usageAndDescription?.PartyPhoneUsage;
                if(usageAndDescription?.otherDescription != null){
                    usageComment= usageAndDescription.otherDescription;   
                }
                phoneUsageAndComments.put(usageType, usageComment);
            }
        }
        return phoneUsageAndComments;
    }
    
   public static Map<String, List<ContactPointEmail>> createContactPointEmail(List<wsPartyManageParty.PartyEmailType> PartyEmail, Account acc, List<ContactPointEmail> existingContactPointEmailList) {
            //Existing email and usages for this account
            Map<String, List<ContactPointEmail>> returnEmailMap = new Map<String, List<ContactPointEmail>>();
            Map<string, List<string>> existingEmailAndUsageListMap = new Map<string, List<string>>();
            Map<string, ContactPointEmail> existingEmailContactEmailPhoneMap = new Map<string, ContactPointEmail>();
            if(existingContactPointEmailList != null && existingContactPointEmailList?.size()>0 ){
                for(ContactPointEmail existingEmail : existingContactPointEmailList){
                    existingEmailContactEmailPhoneMap.put(existingEmail.EmailAddress+existingEmail.UsageType, existingEmail);
                    if(existingEmailAndUsageListMap.containsKey(existingEmail.EmailAddress)){
                        List<String> existingUsages = existingEmailAndUsageListMap.get(existingEmail.EmailAddress);
                        if(!existingUsages.contains(existingEmail.UsageType)){
                            existingEmailAndUsageListMap.get(existingEmail.EmailAddress).add(existingEmail.UsageType);   
                        }
                        }else{
                            existingEmailAndUsageListMap.put(existingEmail.EmailAddress, new List<string>{existingEmail.UsageType});    
                        }	    
                    }    
            }else {existingEmailAndUsageListMap = null;}
           List<ContactPointEmail> newEmailList = new List<ContactPointEmail>();
           if (PartyEmail != null) {
                for (wsPartyManageParty.PartyEmailType email : PartyEmail) {
                    List<String> emailUsageTypes = parseEmailUsage(email);
                    for(string emailUsage : emailUsageTypes){
                        if (email?.Email?.emailAddressText != null) {
                            string emailAddress = email?.Email?.emailAddressText;
                            if(existingEmailAndUsageListMap !=null ? !existingEmailAndUsageListMap?.containsKey(emailAddress) && 
                               existingEmailAndUsageListMap?.get(emailAddress) != null ? !existingEmailAndUsageListMap?.get(emailAddress)?.contains(emailUsage): true : true){
                                ContactPointEmail cpe = new ContactPointEmail();
                                cpe.ParentId = acc.id;
                                cpe.EmailAddress = email?.Email?.emailAddressText;
                                cpe.UsageType = emailUsage;
                                cpe.IsPrimary = email?.TrustManagedPrimaryIndicator == 'Y'?true : false;
                                cpe.Special_Instruction__c = email?.specialInstructions!=null? email?.specialInstructions :'';
                                newEmailList.add(cpe);     
                            }else if(existingEmailContactEmailPhoneMap != null && existingEmailAndUsageListMap != null && existingEmailAndUsageListMap?.containsKey(emailAddress) && existingEmailAndUsageListMap?.get(emailAddress)?.contains(emailUsage)){
                                existingEmailContactEmailPhoneMap.remove(emailAddress+emailUsage);
                            }
                        }    
                    }            
                }            
            }
            if(newEmailList.size()>0)returnEmailMap.put('Insert', newEmailList);
            if(existingEmailContactEmailPhoneMap.values().size()>0)returnEmailMap.put('Delete', existingEmailContactEmailPhoneMap.values());
            return returnEmailMap;
     }    
       
    private static List<String> parseEmailUsage(wsPartyManageParty.PartyEmailType currentEmail) {
        List<String> usageTypes = new List<String>();
        for(wsPartyManageParty.PartyEmailUsageAndDescriptionType usageAndDescription : currentEmail.PartyEmailUsageAndDescription) {
                if(usageAndDescription?.PartyEmailUsage != 'OTHER') {
                    usageTypes.add(usageAndDescription.PartyEmailUsage);
                }
        }
        return usageTypes;
    }
    
    public static List<Schema.Location> createLocationRecords(List<wsPartyManageParty.PartyAddressType> PartyAddress, Account acc, List<AssociatedLocation> existingAssociatedLocationList) {
     //Existing location, associatedLocation and usages for this account
            Map<string, List<string>> existingLocationAndUsageListMap = new Map<string, List<string>>();
            if(existingAssociatedLocationList != null && existingAssociatedLocationList?.size()>0 ){
                for(AssociatedLocation location : existingAssociatedLocationList){
                    if(existingLocationAndUsageListMap.containsKey(location.Location.Name)){
                        List<String> existingUsages = existingLocationAndUsageListMap.get(location.Location.Name);
                        if(!existingUsages.contains(location.Type)){
                            existingLocationAndUsageListMap.get(location.Location.Name).add(location.Type);   
                        }
                        }else{
                            existingLocationAndUsageListMap.put(location.Location.Name, new List<string>{location.Type});    
                        }	    
                    }    
            }else {existingLocationAndUsageListMap = null;}
            
            system.debug('PartyAddress >>>' + PartyAddress);
            List<Schema.Location> locationsToInsert =new List<Schema.Location>();
            if(PartyAddress != null){
                for(wsPartyManageParty.PartyAddressType address : PartyAddress) {
                        Schema.Location loc = new Schema.Location();
                        if(address?.Address != null){
                            String streetAddress = address.Address.addressLine1;
                            if (address.Address.addressLine2 != null) {
                                streetAddress += ' '+address.Address.addressLine2;
                            }
                            string locName = streetAddress + ', ' + address?.Address?.city;
                            if(existingLocationAndUsageListMap != null ? !existingLocationAndUsageListMap?.keyset()?.contains(locName) : true){
                                loc = new Schema.Location();
                                loc.Name = locName;
                                locationsToInsert.add(loc); 
                            
                            }
                        }
                    }     
            }
            
        
            system.debug('locationsToInsert >>>' + locationsToInsert);
            return locationsToInsert;
                
                
       
    }
    
    public static Map<String, List<SObject>> createAssociatedLocationAndAddressRecords(List<wsPartyManageParty.PartyAddressType> PartyAddress, Account acc, List<AssociatedLocation> existingAssociatedLocationList, Map<String, Id> locNameLocationIdMap, Map<Id, String> existingLocationNameMap) {
       
            //Existing location, associatedLocation and usages for this account
            Map<string, List<string>> existingLocationAndUsageListMap = new Map<string, List<string>>();
            List<AssociatedLocation> deleteRecords = new List<AssociatedLocation> ();
            if(existingAssociatedLocationList != null && existingAssociatedLocationList?.size()>0 ){
                for(AssociatedLocation location : existingAssociatedLocationList){
                    if(existingLocationAndUsageListMap.containsKey(location.Location.Name)){
                        List<String> existingUsages = existingLocationAndUsageListMap.get(location.Location.Name);
                        if(!existingUsages.contains(location.Type)){
                            existingLocationAndUsageListMap.get(location.Location.Name).add(location.Type);   
                        }
                        }else{
                            existingLocationAndUsageListMap.put(location.Location.Name, new List<string>{location.Type});    
                        }	    
                    }    
            }else {existingLocationAndUsageListMap = null;}
            List<sObject> addressesToInsert =new List<sObject>();
            List<sObject> associatedLocationsToInsert =new List<sObject>();    
            Map<String, Map<String, String>> locNameUsageAndPurposeMap = new Map<String, Map<String, String>>();
            Map<String, wsPartyManageParty.PartyAddressType> locNamePartyAddressMap = new Map<String, wsPartyManageParty.PartyAddressType>();
                
            if(PartyAddress != null){
                for(wsPartyManageParty.PartyAddressType address : PartyAddress) {
                    
                    Schema.Location loc = new Schema.Location();
                    Map<string, string> usageTypeAndPurposeMap = new Map<string, string>();
                    List<String> usageList = new List<String>();
                    if(address?.PartyAddressPurposeAndUsage != null) {
                        usageTypeAndPurposeMap = addressUsageType(address?.PartyAddressPurposeAndUsage);
                    }
                    
                    if(address?.Address != null){
                        String streetAddress = address.Address.addressLine1;
                        if (address.Address.addressLine2 != null) {
                            streetAddress += ' '+address.Address.addressLine2;
                        }
                        string locName = streetAddress + ', ' + address?.Address?.city;
                        locNameUsageAndPurposeMap.put(locName, usageTypeAndPurposeMap);
                        if(existingLocationAndUsageListMap != null ? !existingLocationAndUsageListMap?.containskey(locName) : true){
                            locNamePartyAddressMap.put(locName, address);
                        }
                    }
                } 
                if(existingAssociatedLocationList != null && existingAssociatedLocationList?.size()>0){
                    for(AssociatedLocation assLoc : existingAssociatedLocationList){
                        if(!locNameUsageAndPurposeMap.containskey(assLoc?.Location?.Name)){
                            deleteRecords.add(assLoc);  
                        }
                    }    
                }
                system.debug('deleteRecords' + deleteRecords);
            }
            //insert locationsToInsert;
            if(acc != null){
                for(String loc : locNameUsageAndPurposeMap.keySet()) {
                       Id locId = locNameLocationIdMap.get(loc);
                       Map<string, string> usageTypePurposeMap = locNameUsageAndPurposeMap?.get(loc);
                       if(usageTypePurposeMap?.keyset() != null){
                           for(string us: usageTypePurposeMap?.keyset()){
                               AssociatedLocation associated = new AssociatedLocation();
                               associated.LocationId = locId;
                               associated.ParentRecordId = acc.Id;
                               associated.PersonAccount__c = acc.Id;
                               associated.Purpose__c = usageTypePurposeMap?.get(us);
                               associated.Type = us;
                               associatedLocationsToInsert.add(associated);            
                           }     
                       }
                       if(locNamePartyAddressMap.get(loc)!= null && !existingLocationNameMap.containsKey(locId) ){
                           wsPartyManageParty.PartyAddressType partyAddressType = locNamePartyAddressMap.get(loc);
                           String streetAddress = partyAddressType?.Address?.addressLine1;
                           if (partyAddressType?.Address?.addressLine2 != null) {
                                streetAddress += ' '+partyAddressType.Address.addressLine2;
                           }
                           String countryCode = partyAddressType?.Address?.countryIdentifier == 'USA'? 'US':partyAddressType?.Address?.countryIdentifier;
                           Schema.Address address1 = AmFam_Utility.buildAddressRecord(streetAddress, partyAddressType.Address.city, partyAddressType.Address.stateCode, partyAddressType.Address.zip5Code, countryCode);
                           address1.ParentId = locId;
                           address1.carrierRoute__c = partyAddressType?.Address?.carrierRoute;
                           address1.censusBlockMD__c = partyAddressType?.Address?.censusBlockMD;
                           address1.censusTractNumber__c = partyAddressType?.Address?.censusTractNumber;
                           address1.fipsCountyNumber__c = partyAddressType?.Address?.fipsCountyNumber;
                           address1.fipsStateNumber__c = partyAddressType?.Address?.fipsStateNumber;
                           address1.geoMatchLevelCode__c = partyAddressType?.Address?.geoMatchLevelCode;
                           address1.Short_City_Name__c = partyAddressType?.Address?.shortCity;
                           address1.Zip_Code_Plus_2__c = partyAddressType?.Address?.zip2Code;
                           address1.Zip_Code_Plus_4__c = partyAddressType?.Address?.zip4Code;
                           addressesToInsert.add(address1);       	    
                       }
                     }  
            }
            Map<String, List<SObject>> returnMap = new Map<String, List<SObject>>();
            returnMap.put('AssociatedLocation', associatedLocationsToInsert);
            returnMap.put('Address', addressesToInsert);
            returnMap.put('delete', deleteRecords);
            
        return returnMap;
        
    }
    
    Public static Map<string, string> addressUsageType(List<wsPartyManageParty.PartyAddressPurposeAndUsageType> usageList){
        Map<string, string> usageTypeAndPurposeMap = new Map<string, string>();
        for(wsPartyManageParty.PartyAddressPurposeAndUsageType purposeAndUsage : usageList) {
            string purpose, usage;
            if(purposeAndUsage?.PartyAddressUsage != null) {
            	usage = purposeAndUsage?.PartyAddressUsage;		
            }
            if(purposeAndUsage?.PartyAddressPurpose != null) {
            	purpose = purposeAndUsage?.PartyAddressPurpose;	
            }
            usageTypeAndPurposeMap.put(usage, purpose);
        }
        return usageTypeAndPurposeMap;
     }
    
    Public static Map<String, List<SObject>>createRelationshipRecords(List<wsPartyManageParty.PartyRelationshipType> PartyRelationship, List<wsPartyManageParty.PrincipalRelationshipType> PrincipalRelationship, Account rec, Map<string, 
          FinServ__ReciprocalRole__c> RelCodeReciprocalRoleMap, Map<Id, List<FinServ__AccountAccountRelation__c>> accountIdAccAccRelListMap, Map<Id, List<FinServ__ContactContactRelation__c>> accountIdConConRelListMap, Map<string, Account> cdhIdAllAccountMap){
              
              Map<String, List<SObject>> returnMapRelationships = new Map<String, List<SObject>>();
              List<sObject> listOfRelationshipForInsert = new List<sObject>();
              List<sObject> listOfRelationshipForDelete = new List<sObject>();
              List<String> checkUniqueForDeletionList = new List<String>();
              
              Map<string, string> partyIdentifierCodeMap = new Map<string, string>();
              string identifier = '';
              string uniqueRole = '';
              
              List<String> existingUniqueKeyForContact = new List<String>(); 
              List<String> existingUniqueKeyForAccount = new List<String>();
              
              List<FinServ__AccountAccountRelation__c> accRList = accountIdAccAccRelListMap?.get(rec?.id);
              List<FinServ__ContactContactRelation__c> conRList = accountIdConConRelListMap?.get(rec?.id);
              if(accRList != null){
                  for(FinServ__AccountAccountRelation__c aa: accRList){
                    string ur = aa.FinServ__Role__r.CDH_Relationship_Code__c+'-'+aa.FinServ__Role__r.CDH_Relationship_Code_Inverse__c; 
                    existingUniqueKeyForAccount.add(ur);
                  }
              }
              
              if(conRList != null){
                  for(FinServ__ContactContactRelation__c cc: conRList){
                    string urc = cc.FinServ__Role__r.CDH_Relationship_Code__c+'-'+cc.FinServ__Role__r.CDH_Relationship_Code_Inverse__c; 
                    existingUniqueKeyForContact.add(urc);    
                  }
              }
              
              
              //checking whether the relationship already exists
              if(PartyRelationship != null){
                  for(wsPartyManageParty.PartyRelationshipType partyRel : PartyRelationship){
                     identifier = partyRel?.RelatedParty?.partyIdentifier !=null?partyRel?.RelatedParty?.partyIdentifier:'';
                     if(partyRel?.RelatedPartyRole?.code != null && partyRel.PartyRole?.code != null){
                        uniqueRole = partyRel?.RelatedPartyRole?.code+'-'+partyRel.PartyRole?.code;
                        checkUniqueForDeletionList.add(partyRel?.RelatedPartyRole?.code+'-'+partyRel.PartyRole?.code);
                        checkUniqueForDeletionList.add(partyRel?.PartyRole?.code+'-'+partyRel.RelatedPartyRole?.code);
                     }
                     if(!existingUniqueKeyForContact.contains(uniqueRole) && !existingUniqueKeyForAccount.contains(uniqueRole) && string.isNotBlank(identifier) && string.isNotBlank(uniqueRole)){
                        partyIdentifierCodeMap.put(identifier, uniqueRole);    
                     }
                   }    
              }
              
              if(PrincipalRelationship != null){
                  for(wsPartyManageParty.PrincipalRelationshipType partyRel : PrincipalRelationship){
                     identifier = partyRel?.RelatedParty?.partyIdentifier !=null?partyRel?.RelatedParty?.partyIdentifier:'';
                     if(partyRel?.RelatedPartyRole?.code != null && partyRel.PartyRole?.code != null){
                        uniqueRole = partyRel?.RelatedPartyRole?.code+'-'+partyRel.PartyRole?.code;
                        checkUniqueForDeletionList.add(partyRel?.RelatedPartyRole?.code+'-'+partyRel.PartyRole?.code);
                        checkUniqueForDeletionList.add(partyRel?.PartyRole?.code+'-'+partyRel.RelatedPartyRole?.code);
                     }
                     if(!existingUniqueKeyForContact.contains(uniqueRole) && !existingUniqueKeyForAccount.contains(uniqueRole) && string.isNotBlank(identifier) && string.isNotBlank(uniqueRole)){
                        partyIdentifierCodeMap.put(identifier, uniqueRole);    
                     }
                   }
              }
              
              //preparing for creation
              string uniqueKey;
              for(String party: partyIdentifierCodeMap.keySet()){
                  
                  Account relatedAccount  = cdhIdAllAccountMap?.get(party);
                  uniqueKey = partyIdentifierCodeMap?.get(party);
                  FinServ__ReciprocalRole__c role = RelCodeReciprocalRoleMap?.get(uniqueKey);
                  
                  if(rec.RecordTypeId == AmFam_Utility.getPersonAccountRecordType()){
                      if(rec?.PersonContactId != null && relatedAccount?.PersonContactId != null && role != null){
                          FinServ__ContactContactRelation__c ccr = new FinServ__ContactContactRelation__c();
                          ccr.FinServ__Contact__c = rec.PersonContactId;
                          ccr.FinServ__RelatedContact__c = relatedAccount.PersonContactId;
                          ccr.FinServ__Role__c = role.id;
                          listOfRelationshipForInsert.add(ccr);    
                      }
                  }else if(rec.RecordTypeId == AmFam_Utility.getAccountBusinessRecordType()){
                      if(rec?.id != null && relatedAccount?.id != null && role != null){
                          FinServ__AccountAccountRelation__c aar = new FinServ__AccountAccountRelation__c();
                          aar.FinServ__Account__c  = rec.Id;
                          aar.FinServ__RelatedAccount__c = relatedAccount.id;
                          aar.FinServ__Role__c = role.id;
                          listOfRelationshipForInsert.add(aar);    
                      }
                  }	     
              }
              
              system.debug('rec>>>>' + rec);
              //preparing for deletion
              if(accRList != null){
                  for(FinServ__AccountAccountRelation__c aa: accRList){
                      if(aa.FinServ__Account__c == rec.id) {
                      string ur = aa.FinServ__Role__r.CDH_Relationship_Code__c+'-'+aa.FinServ__Role__r.CDH_Relationship_Code_Inverse__c; 
                          if(!checkUniqueForDeletionList.contains(ur)){
                          listOfRelationshipForDelete.add(aa);    
                          }
                      }
                   }
               }
              
              if(conRList != null){
                for(FinServ__ContactContactRelation__c cc: conRList){
                    string cdhIdRec = rec?.CDHID__c;
                    string personContId = cdhIdAllAccountMap?.get(cdhIdRec)?.PersonContactId; 
                    
                    if(cc?.FinServ__Contact__c != null ? cc?.FinServ__Contact__c.equals(personContId) : false) {
                      string urc = cc.FinServ__Role__r.CDH_Relationship_Code__c+'-'+cc.FinServ__Role__r.CDH_Relationship_Code_Inverse__c;
                          if(!checkUniqueForDeletionList.contains(urc)){
                          listOfRelationshipForDelete.add(cc);    
                          }
                      }
                   }
               } 
              
               if(listOfRelationshipForInsert.size()>0) returnMapRelationships.put('insert', listOfRelationshipForInsert);
               if(listOfRelationshipForDelete.size()>0) returnMapRelationships.put('delete', listOfRelationshipForDelete);
               return returnMapRelationships;
    }

}