@IsTest
public class AmFam_NewUserAccessToPolicyBatchTest {

    @IsTest
    static public void testPolicyAccessForNewUser() 
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '77230';
        insert agentUser;

        Producer prod1 = new Producer();
        prod1.Producer_ID__c = '73320';
        prod1.Name = '73320';
        prod1.InternalUserId = UserInfo.getUserId();
        prod1.Type = 'Agency';
        insert prod1;

        Producer prod2 = new Producer();
        prod2.Producer_ID__c = '99999';
        prod2.Name = '99999';
        prod2.InternalUserId = UserInfo.getUserId();
        prod2.Type = 'Agency';
        insert prod2;

        List<InsurancePolicy> newPols = new List<InsurancePolicy>();
        InsurancePolicy pol1 = new InsurancePolicy();
        pol1.Name = '1234';
        pol1.PolicyName = 'Policy1';
        pol1.NameInsuredId = person1.Id;
        pol1.PolicyType = 'Life';
        pol1.ProducerId = prod1.Id;
        newPols.add(pol1);

        InsurancePolicy pol2 = new InsurancePolicy();
        pol2.Name = '5678';
        pol2.PolicyName = 'Policy2';
        pol2.NameInsuredId = person2.Id;
        pol2.PolicyType = 'Auto';
        pol2.ProducerId = prod2.Id;
        newPols.add(pol2);

        InsurancePolicy pol3 = new InsurancePolicy();
        pol3.Name = '1010';
        pol3.PolicyName = 'Policy3';
        pol3.NameInsuredId = person2.Id;
        pol3.PolicyType = 'Auto';
        pol3.ProducerId = prod2.Id;
        newPols.add(pol3);
        insert newPols;

        Test.startTest();

        AmFam_NewUserAccessToPolicyBatch obj = new AmFam_NewUserAccessToPolicyBatch(new List<Id>{agentUser.Id});
        DataBase.executeBatch(obj); 
            
        Test.stopTest();

        List<InsurancePolicyShare> shares = [SELECT Id,ParentId,UserOrGroupId FROM InsurancePolicyShare WHERE UserOrGroupId = :agentUser.Id];
        System.debug('shares:'+shares);

        //only share should be the autocreated one that will get wiped on any share recalc
        System.assert(shares.size() == 1);
    }

    @IsTest
    static public void testAlreadyDeleted()
    {
        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'AmFam Agent'];
        Profile p2 = [SELECT Id, Name FROM Profile WHERE Name = 'Agent CSR'];

        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');

        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');
        User agentUser = new User (alias = 'sagent', email='test1245@xyz.com',
                                    emailencodingkey='UTF-8', FederationIdentifier='test1245',
                                    lastname='TestAGent',
                                    languagelocalekey='en_US',
                                    localesidkey='en_US',
                                    profileid=p.Id,
                                    timezonesidkey='America/Chicago',
                                    username='test1245@xyz.com.test',
                                    AmFam_User_ID__c = '12345');
                                    
        agentUser.Producer_ID__c = '77230';
        insert agentUser;

        Producer prod1 = new Producer();
        prod1.Producer_ID__c = '73320';
        prod1.Name = '73320';
        prod1.InternalUserId = UserInfo.getUserId();
        prod1.Type = 'Agency';
        insert prod1;

        Producer prod2 = new Producer();
        prod2.Producer_ID__c = '99999';
        prod2.Name = '99999';
        prod2.InternalUserId = UserInfo.getUserId();
        prod2.Type = 'Agency';
        insert prod2;

        List<InsurancePolicy> newPols = new List<InsurancePolicy>();
        InsurancePolicy pol1 = new InsurancePolicy();
        pol1.Name = '1234';
        pol1.PolicyName = 'Policy1';
        pol1.NameInsuredId = person1.Id;
        pol1.PolicyType = 'Life';
        pol1.ProducerId = prod1.Id;
        newPols.add(pol1);

        InsurancePolicy pol2 = new InsurancePolicy();
        pol2.Name = '5678';
        pol2.PolicyName = 'Policy2';
        pol2.NameInsuredId = person2.Id;
        pol2.PolicyType = 'Auto';
        pol2.ProducerId = prod2.Id;
        newPols.add(pol2);

        InsurancePolicy pol3 = new InsurancePolicy();
        pol3.Name = '1010';
        pol3.PolicyName = 'Policy3';
        pol3.NameInsuredId = person2.Id;
        pol3.PolicyType = 'Auto';
        pol3.ProducerId = prod2.Id;
        newPols.add(pol3);
        insert newPols;

        InsurancePolicyShare share = new InsurancePolicyShare();
        share.UserOrGroupId = agentUser.Id;
        share.ParentId = pol2.Id;
        share.AccessLevel = 'Read';
        insert share;

        delete pol2;

        Test.startTest();

        AmFam_NewUserAccessToPolicyBatch obj = new AmFam_NewUserAccessToPolicyBatch(new List<Id>{agentUser.Id});
        DataBase.executeBatch(obj); 
            
        Test.stopTest();

        List<InsurancePolicyShare> shares = [SELECT Id,ParentId,UserOrGroupId,RowCause FROM InsurancePolicyShare WHERE UserOrGroupId = :agentUser.Id];
        System.debug('shares:'+shares);

        //my created share and the recalced share
        System.assert(shares.size() == 2);
    } 

}