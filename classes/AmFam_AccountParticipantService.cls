public without sharing class AmFam_AccountParticipantService {
    
	/* DISABLED UNTIL NEW PRODUCER ACCESS Grant gets updated
	
	public static void shareAccessToRelatedRecords(List<AccountParticipant> newRecords) {

		//Extract the list of account Ids. The same set of accounts will be used on each individual call to related records
		Set<Id> accountIds = new Set<Id>();
		for (AccountParticipant ap : newRecords) {
			accountIds.add(ap.AccountId);
		}		

		shareAccessToChildAccounts(newRecords, accountIds);
		shareAccessToInsurancePolicies(newRecords, accountIds);

	}

	public static void removeAccessFromRelatedRecords(List<AccountParticipant> oldRecords) {

		//Extract the list of account Ids. The same set of accounts will be used on each individual call to related records
		Set<Id> accountIds = new Set<Id>();
		for (AccountParticipant ap : oldRecords) {
			accountIds.add(ap.AccountId);
		}		


		removeAccessFromChildAccounts(oldRecords, accountIds);
		removeAccessFromInsurancePolicies(oldRecords, accountIds);

	}



	//**********************************************
	//Individual Related records
	//**********************************************

	private static void shareAccessToChildAccounts(List<AccountParticipant> newRecords, Set<Id> accountIds) {

		System.debug('### shareAccessToChildAccounts - accountIds: ' + accountIds);


		//Variables and Collections
		Map<Id, Id> 				accountToAccountMap = 			new Map<Id, Id>();
		Map<Id, Set<Id>> 			accountParticipantPerAccount = 	new Map<Id, Set<Id>>();
		List<AccountParticipant> 	newAccountParticipants = 		new List<AccountParticipant>();


		
		//Get the list of AccountContact relation where Account will be in accountIds
		List<AccountContactRelation> acrList = [SELECT AccountId,ContactId, Contact.AccountId FROM AccountContactRelation WHERE AccountId IN: accountIds ];


		//Build a Map of child Account IDs indexed by parent AccountId
		for (AccountContactRelation acr : acrList) {

			//Ensure the Account Contact relationship for the same couple (Account XYZ - Contact XYZ) is excluded from the Map
			if (acr.AccountId != null && acr.ContactId != null && acr.Contact.AccountId != null && acr.AccountId != acr.Contact.AccountId) {
				accountToAccountMap.put(acr.AccountId, acr.Contact.AccountId);
			}
		}



		//Get the AccountParticipant already shared with the child Accounts and build a Map indexed by child Account Id
		List<AccountParticipant> childAccountParticipats = [SELECT Id, AccountId, ParticipantId FROM AccountParticipant WHERE AccountId IN: accountToAccountMap.values()];

		for (AccountParticipant ap : childAccountParticipats) {
			Set<Id> particpantsPerAccount = accountParticipantPerAccount.get(ap.AccountId);
			if (particpantsPerAccount == null) {
				particpantsPerAccount = new Set<Id>();
			}

			particpantsPerAccount.add(ap.ParticipantId);
			accountParticipantPerAccount.put(ap.AccountId, particpantsPerAccount);
		}

		System.debug('### shareAccessToChildAccounts - accountParticipantPerAccount: ' + accountParticipantPerAccount);




		//Build the list of new AccountParticipant records to be created for child Accounts
		for (AccountParticipant ap : newRecords){

			System.debug('### shareAccessToChildAccounts - ap: ' + ap);

			Id accountId = ap.AccountId;
			Id participantId = ap.ParticipantId;
			Id participantRoleId = ap.ParticipantRoleId;

			Id childAccountId = accountToAccountMap.get(accountId);
			System.debug('### shareAccessToChildAccounts - childAccountId: ' + childAccountId);

			if (childAccountId != null) {
				Set<Id> childParticipants = accountParticipantPerAccount.get(childAccountId);

				if (childParticipants == null || !childParticipants.contains(participantId)) {
					AccountParticipant newAP = new AccountParticipant();
					newAP.ParticipantId = participantId;
					newAP.AccountId = childAccountId;
					newAP.IsActive = true;
					newAP.ParticipantRoleId = ap.ParticipantRoleId;

					newAccountParticipants.add(newAP);
				}
			}
		}

		insert newAccountParticipants;

	}





	private static void removeAccessFromChildAccounts(List<AccountParticipant> oldRecords, Set<Id> accountIds) {

		System.debug('### shareAccessToChildAccounts - accountIds: ' + accountIds);


		//Variables and Collections
		Map<Id, Id> 						accountToAccountMap = 			new Map<Id, Id>();
		List<AccountParticipant> 			accountParticipantsToDelete = 	new List<AccountParticipant>();
		Map<Id, Set<AccountParticipant>> 	accountParticipantPerAccount = 	new Map<Id, Set<AccountParticipant>>();


		
		//Get the list of AccountContact relation where Account will be in accountIds
		List<AccountContactRelation> acrList = [SELECT AccountId,ContactId, Contact.AccountId FROM AccountContactRelation WHERE AccountId IN: accountIds ];


		//Build a Map of child Account IDs indexed by parent AccountId
		for (AccountContactRelation acr : acrList) {

			//Ensure the Account Contact relationship for the same couple (Account XYZ - Contact XYZ) is excluded from the Map
			if (acr.AccountId != null && acr.ContactId != null && acr.Contact.AccountId != null && acr.AccountId != acr.Contact.AccountId) {
				accountToAccountMap.put(acr.AccountId, acr.Contact.AccountId);
			}
		}



		//Get the AccountParticipant already shared with the child Accounts and build a Map indexed by child Account Id
		List<AccountParticipant> childAccountParticipats = [SELECT Id, AccountId, ParticipantId FROM AccountParticipant WHERE AccountId IN: accountToAccountMap.values()];

		for (AccountParticipant ap : childAccountParticipats) {
			Set<AccountParticipant> particpantsPerAccount = accountParticipantPerAccount.get(ap.AccountId);
			if (particpantsPerAccount == null) {
				particpantsPerAccount = new Set<AccountParticipant>();
			}

			particpantsPerAccount.add(ap);
			accountParticipantPerAccount.put(ap.AccountId, particpantsPerAccount);
		}

		System.debug('### shareAccessToChildAccounts - accountParticipantPerAccount: ' + accountParticipantPerAccount);




		//Build te list ot share records to delete
		for (AccountParticipant ap : oldRecords) {

			Id accountId = ap.AccountId;
			Id childAccoutID = accountToAccountMap.get(accountId);
			Id participantId = ap.ParticipantId;

			Set<AccountParticipant> particpantsPerAccount = accountParticipantPerAccount.get(childAccoutID);


			if (particpantsPerAccount != null) {

				for (AccountParticipant childAccountParticipant : particpantsPerAccount) {

					Id childParticipantId = childAccountParticipant.ParticipantId;

					if (childParticipantId == participantId) {
						accountParticipantsToDelete.add(childAccountParticipant);
					}

				}
			}

		}

		delete accountParticipantsToDelete;
	}




	private static void shareAccessToInsurancePolicies(List<AccountParticipant> newRecords, Set<Id> accountIds) {


		Map<Id, Set<Id>> 			policyByAccountMap = 	new Map<Id, Set<Id>>();
		List<SObject> 				newShares = 			new List<SObject>();


		//Retrieve All policies related to insurec accounts
		List<InsurancePolicy> policies = [SELECT Id, NameInsuredId FROM InsurancePolicy WHERE NameInsuredId IN: accountIds];


		//Build a Map with the Policy Ids index by Account Id
		for (InsurancePolicy ip : policies ) {

			Id accountId = ip.NameInsuredId;

			Set<Id> policiesForAccountSet = policyByAccountMap.get(accountId);
			if (policiesForAccountSet == null)
				policiesForAccountSet = new Set<Id>();

			policiesForAccountSet.add(ip.Id);

			policyByAccountMap.put(accountId, policiesForAccountSet);

		}



		//Create the Policy Share records with the same Participants associated to the insured Account
		for (AccountParticipant ap : newRecords) {

			Id accountId = ap.AccountId;
			Id participantId = ap.ParticipantId;

			Set<Id> policyIdSet = policyByAccountMap.get(accountId);


			if (policyIdSet != null) {

				for (Id policyId : policyIdSet) {

					SObject newShareObj = AmFam_Utility.createNewShare('InsurancePolicyShare', policyId, participantId);

					newShares.add(newShareObj);

				}
			}

		}

		insert newShares;


	}





	private static void removeAccessFromInsurancePolicies(List<AccountParticipant> oldRecords, Set<Id> accountIds) {

		Map<Id, Set<InsurancePolicyShare>> 			policyShareByAccountMap = 	new Map<Id, Set<InsurancePolicyShare>>();
		List<SObject> 								sharesToDelete = 			new List<SObject>();


		//Retrieve All policy shares related to insurec accounts
		List<InsurancePolicyShare> policieShares = [SELECT Id, Parent.NameInsuredId, ParentId, UserOrGroupId FROM InsurancePolicyShare WHERE Parent.NameInsuredId IN: accountIds];


		//Build a Map with the Policy Share Ids index by Account Id
		for (InsurancePolicyShare ips : policieShares ) {

			Id accountId = ips.Parent.NameInsuredId;

			Set<InsurancePolicyShare> shareForAccountSet = policyShareByAccountMap.get(accountId);
			if (shareForAccountSet == null)
				shareForAccountSet = new Set<InsurancePolicyShare>();

			shareForAccountSet.add(ips);

			policyShareByAccountMap.put(accountId, shareForAccountSet);

		}



		//Build te list ot share records to delete
		for (AccountParticipant ap : oldRecords) {

			Id accountId = ap.AccountId;
			Id participantId = ap.ParticipantId;

			Set<InsurancePolicyShare> shareForAccountSet = policyShareByAccountMap.get(accountId);


			if (shareForAccountSet != null) {

				for (InsurancePolicyShare policyShare : shareForAccountSet) {

					Id userOrGroupId = policyShare.UserOrGroupId;

					if (userOrGroupId == participantId) {
						sharesToDelete.add(policyShare);
					}

				}
			}

		}

		delete sharesToDelete;







	}

	*/


}