public without sharing class AmFam_InsurancePolicyService {


    public static void updateInsurancePolicyOwnership(List<InsurancePolicy> policies) {
        
        Set<String> producerIds = new Set<String>();
        for (InsurancePolicy ip : policies) {
            if ( String.isNotEmpty(ip.ProducerId) ) {
                producerIds.add(ip.producerId);
            }
        }

        Map<Id, Producer> producerMap = new Map<Id, Producer>([SELECT Id, InternalUserId, InternalUser.IsActive FROM Producer WHERE Id IN: producerIds]);


        for (InsurancePolicy ip : policies) {
            if ( String.isNotEmpty(ip.ProducerId) ) {
                Producer pr = producerMap.get(ip.ProducerId);
                if (pr != null && String.isNotEmpty(pr.InternalUserId) && pr.InternalUser.IsActive) {
                    ip.OwnerId = pr.InternalUserId;
                }
            }
        }
    }


    public static void removeConvertedPolicies(List<InsurancePolicy> policies) {

        Id quoteRT = AmFam_Utility.getInsurancePolicyQuoteRecordType();
        Id applicationRT = AmFam_Utility.getInsurancePolicyApplicationRecordType();

        List<InsurancePolicy> recordsToDelete = new List<InsurancePolicy>();

        Set<String> sumbissionIdSet = new Set<String>();
        for (InsurancePolicy ip : policies) {
            if ( String.isNotEmpty(ip.SubmissionID__c) ) {
                sumbissionIdSet.add(ip.SubmissionID__c);
            }
        }

        List<InsurancePolicy> existingPolicies = [SELECT Id, Name, SubmissionID__c, RecordTypeId 
                                                    FROM InsurancePolicy 
                                                    WHERE Name IN: sumbissionIdSet AND (RecordTypeId =: quoteRT OR RecordTypeId =: applicationRT)];

        Map<String, List<InsurancePolicy>> policiesBySumbissionId = new Map<String, List<InsurancePolicy>>();

        for (InsurancePolicy ip : existingPolicies) {
            String policyId = ip.Name;

            List<InsurancePolicy> policiesForId = policiesBySumbissionId.get(policyId);
            if (policiesForId == null) {
                policiesForId = new List<InsurancePolicy>();
            }

            policiesForId.add(ip);
            policiesBySumbissionId.put(policyId, policiesForId);
        }


        for (InsurancePolicy ip : policies) {
            String policyId = ip.SubmissionID__c;
            List<InsurancePolicy> policiesForId = policiesBySumbissionId.get(policyId);

            if (policiesForId != null) {

                for (InsurancePolicy existingIP : policiesForId) {

                    if (existingIP.RecordTypeId != ip.RecordTypeId) {
                        recordsToDelete.add(existingIP);
                    }
                }
            }
        }

        
        if (recordsToDelete.size() > 0 ) {
            delete recordsToDelete;
        }
    

    }

    public static void updateInsurancePolicyShares(List<InsurancePolicy> policies)
    {
        List<InsurancePolicyShare> newShares = new List<InsurancePolicyShare>();
        List<InsurancePolicyShare> deleteShares = new List<InsurancePolicyShare>();
        Map<Id,InsurancePolicyShare> existingMap;

        List<Id> policyProducerIds = new List<Id>();
        List<Id> policyIds = new List<Id>();

        //there are cases where a policy comes in without Producer Id and if that is the case we
        //can't do shares as it depends on that.  So cull the incoming list to only process 
        //policies with Producer Id

        List<InsurancePolicy> actionablePolicies = new List<InsurancePolicy>();
        for (InsurancePolicy policy : policies)
        {
            if (policy.ProducerId != null)
            {
                actionablePolicies.add(policy);
                policyProducerIds.add(policy.ProducerId);
                policyIds.add(policy.Id);
            }
        }

        //list of producer objects associated with policies
        List<Producer> policyProducers = [SELECT Id,Name,InternalUserId,Producer_ID__c FROM Producer WHERE Id IN :policyProducerIds AND InternalUser.IsActive = true];

        //map of producers keyed by producer id
        Map<Id,Producer> policyProducerMap = new Map<Id,Producer>();

        List<String> policyProducerProdIds = new List<String>(); //list of string producer ids
        for (Producer prod : policyProducers)
        {
            policyProducerProdIds.add(prod.Name);
            policyProducerMap.put(prod.Id,prod);
        }

        //List of producers that reference the ProducerIds from the policyProducers
        List<Producer> producersForProdIds = [SELECT Id,Name,InternalUserId,Producer_ID__c FROM Producer WHERE Name IN :policyProducerProdIds AND InternalUser.IsActive = true];

        //List of all IPS for the policies
        List<InsurancePolicyShare> existingIPS = [SELECT Id,ParentId,UserOrGroupId,AccessLevel,RowCause 
                                                  FROM InsurancePolicyShare 
                                                  WHERE ParentId IN :policyIds 
                                                  AND Parent.IsDeleted = false ALL ROWS];

        if (actionablePolicies.size() == 0)
        {
            return;
        }

        //create map keyed by policy id of maps of IPS keyed by User Id
        Map<Id, Map<Id,InsurancePolicyShare>> policyExistingShareMap = new Map<Id, Map<ID,InsurancePolicyShare>>();
        for(InsurancePolicyShare ips : existingIPS)
        {
            if (policyExistingShareMap.get(ips.ParentId) == null)
            {
                policyExistingShareMap.put(ips.ParentId,new Map<Id,InsurancePolicyShare>());
            }

            Map<Id,InsurancePolicyShare> ipsMap = policyExistingShareMap.get(ips.ParentId);
            ipsMap.put(ips.UserOrGroupId,ips);
        }

        //create map of producers keyed by producer id 
        Map<String, List<Producer>> prodIdProducersMap = new Map<String, List<Producer>>();

        for (Producer prod : producersForProdIds)
        {
            if (prodIdProducersMap.get(prod.Name) == null)
            {
                prodIdProducersMap.put(prod.Name,new List<Producer>());
            }

            List<Producer> prodList = prodIdProducersMap.get(prod.Name);
            prodList.add(prod);
        }

        for (InsurancePolicy policy : actionablePolicies)
        {
            existingMap = policyExistingShareMap.get(policy.Id);
            Producer policyProducer = policyProducerMap.get(policy.ProducerId);

            if (policyProducer != null)
            {
                List<Producer> allProd = prodIdProducersMap.get(policyProducer.Name);

                for (Producer currProd : allProd)
                {
                    if (existingMap != null && existingMap.containsKey(currProd.InternalUserId))
                    {
                        //already exists so leave it
                        existingMap.remove(currProd.InternalUserId);
                    }
                    else 
                    {
                        InsurancePolicyShare newShare = new InsurancePolicyShare();
                        newShare.ParentId = policy.Id;
                        newShare.UserOrGroupId = currProd.InternalUserId;
                        newShare.AccessLevel = 'Read';
                        newShares.add(newShare);
                    }
                }
            }
        }

        //anything left in existingMap was not referenced and should be removed
        //deleteShares.addAll(existingMap.values());
        if (existingMap != null) {
            for (InsurancePolicyShare share : existingMap.values())
            {
                if (share.AccessLevel != 'All' && share.RowCause == 'Manual') //cant delete system created for owner
                {
                    deleteShares.add(share);
                }
            }
        }

        if (!newShares.isEmpty())
        {
            insert newShares;
        }

        if (!deleteShares.isEmpty())
        {
            delete deleteShares;
        }
    }

    public static void createPolicySharesForNewProducers(List<Producer> newProducers)
    {
        Map<String, List<Producer>> producerIdMap = new Map<String, List<Producer>>();

        for (Producer prod : newProducers)
        {
            if (!producerIdMap.containsKey(prod.Name))
            {
                producerIdMap.put(prod.Name,new List<Producer>());
            }
            producerIdMap.get(prod.Name).add(prod);
        }

        //get agency producers for the incoming producers
        List<Producer> agencyProducers = [SELECT Id,InternalUserId FROM Producer 
                                            WHERE Name IN :producerIdMap.keySet() 
                                                AND Type = 'Agency'];

        List<Id> agencyProducerIds = new List<Id>();
        for (Producer prod : agencyProducers)
        {
            agencyProducerIds.add(prod.Id);
        }

        List<InsurancePolicy> policies = [SELECT Id,ProducerId,Producer.Name,OwnerId FROM InsurancePolicy WHERE ProducerId IN :agencyProducerIds];

        List<InsurancePolicyShare> shares = new List<InsurancePolicyShare>();

        for (InsurancePolicy policy : policies) 
        {
            List<Producer> producersForPolicy = producerIdMap.get(policy.Producer.Name);

            for (Producer prod : producersForPolicy)
            {
                if (policy.OwnerId != prod.InternalUserId)
                {
                    InsurancePolicyShare share = new InsurancePolicyShare();
                    share.ParentId = policy.Id;
                    share.UserOrGroupId = prod.InternalUserId;
                    share.AccessLevel = 'Read';
                    shares.add(share);
                }
            }
        }

        if (!shares.isEmpty())
        {
            insert shares;
        }
    }

    public static void removeInsurancePolicyShares(List<Producer> producers)
    {
        List<InsurancePolicyShare> sharesToDelete = new List<InsurancePolicyShare>();

        //collect users, prodsid, map prodid to user ???this wrong, need to be a map to list?
        Set<String> incomingProducerIds = new Set<String>();
        Set<Id> incomingUsers = new Set<Id>();
        Map<String,List<Id>> prodIdUserMap = new Map<String,List<Id>>();
        for (Producer prod : producers)
        {
            incomingProducerIds.add(prod.Name);
            incomingUsers.add(prod.InternalUserId);

            if (!prodIdUserMap.containsKey(prod.Name))
            {
                prodIdUserMap.put(prod.Name,new List<Id>());
            }
            prodIdUserMap.get(prod.Name).add(prod.InternalUserId);
        }

        //get the Agency Producers for the incoming ProdIds
        List<Producer> agencyProducers = [SELECT Id FROM Producer WHERE Name in :incomingProducerIds AND Type = 'Agency'];

        //collect agency Producer Ids
        List<Id> agencyProducerIds = new List<Id>();
        for (Producer prod : agencyProducers)
        {
            agencyProducerIds.add(prod.Id);
        }

        //fetch all shares for the incoming users where the Producer is in the agency Producers
        //???sure at this point the shares that need to go are in this response
        List<InsurancePolicyShare> shares = [SELECT Id,Parent.ProducerId,Parent.Producer.Name,UserOrGroupId FROM InsurancePolicyShare 
                                                WHERE Parent.ProducerId in :agencyProducerIds AND UserOrGroupId in :incomingUsers];

        for (InsurancePolicyShare share : shares) 
        {
            //see if share has the same user & prod id combination as an incoming
            //prod id through shares policy agency prod id
            //???again is map need to have list of Users instead
            if (prodIdUserMap.get(share.Parent.Producer.Name).contains(share.UserOrGroupId))
            {
                sharesToDelete.add(share);
            }
        }	
        
        if (!sharesToDelete.isEmpty())
        {
            delete sharesToDelete;
        }
    }
    
}