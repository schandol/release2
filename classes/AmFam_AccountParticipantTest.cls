@isTest
private class AmFam_AccountParticipantTest {

	@testSetup static void setup() {

		Account agencyAccount = insertAgencyAccount('Agency Account');
		Account agentAccount = insertAgentAccount('Agent Account');
		Account personAccount = insertPersonAccount('Test 1', 'Person Account 1', 'Group');


	} 




    static testMethod void testRemoveAccountParticipant() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Id agentRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agencyRTID LIMIT 1] ;
	    	Account agentAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;
	    	Contact agentContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: agentAccount.Id LIMIT 1] ;

			InsurancePolicy ip = insertInsurancePolicyWithProducer(personAccount.Id);
			AccountContactRelation acr1 = insertAccountContactRelation(agencyAccount.Id, agentContact.Id);
			AccountContactRelation acr2 = insertAccountContactRelation(agentAccount.Id, personAccount.PersonContactId);

	    	Test.startTest();

            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') && AmFam_Utility.isTriggerSwitchActive('AccountParticipant_Object')) {
                List<AccountParticipant> assertBefore = [SELECT Id FROM AccountParticipant WHERE AccountId =: agentAccount.Id];
                System.assertNotEquals(0, assertBefore.size(), 'Account Participant was not created properly');
            }

	    	delete acr1;

            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') && AmFam_Utility.isTriggerSwitchActive('AccountParticipant_Object')) {
                List<AccountParticipant> assertAfter = [SELECT Id FROM AccountParticipant WHERE AccountId =: agentAccount.Id];
                System.assertEquals(0, assertAfter.size(), 'Account Participant was not deleted properly');
            }
	    	Test.stopTest();

		}
	}




    static testMethod void testCreateAccountParticipant() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Id agentRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agencyRTID LIMIT 1] ;
	    	Account agentAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;
	    	Contact agentContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: agentAccount.Id LIMIT 1] ;

	    	Test.startTest();

			InsurancePolicy ip = insertInsurancePolicyWithProducer(personAccount.Id);
			AccountContactRelation acr1 = insertAccountContactRelation(agencyAccount.Id, agentContact.Id);
			AccountContactRelation acr2 = insertAccountContactRelation(agentAccount.Id, personAccount.PersonContactId);

            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') && AmFam_Utility.isTriggerSwitchActive('AccountParticipant_Object')) {
                List<AccountParticipant> assertAfter = [SELECT Id FROM AccountParticipant WHERE AccountId =: agentAccount.Id];
                System.assertNotEquals(0, assertAfter.size(), 'Account Participant was not created properly');
            }

	    	Test.stopTest();

		}
	}


    static testMethod void testUpdateAccountParticipant() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agenctyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agenctyRTID LIMIT 1] ;

			InsurancePolicy ip = insertInsurancePolicyWithProducer(personAccount.Id);
			AccountContactRelation acr = insertAccountContactRelation(agencyAccount.Id, personAccount.PersonContactId);


	    	Test.startTest();

            List<AccountParticipant> assertBefore = [SELECT Id FROM AccountParticipant WHERE AccountId =: personAccount.Id];


	    	update acr;

            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') && AmFam_Utility.isTriggerSwitchActive('AccountParticipant_Object')) {
                List<AccountParticipant> assertAfter = [SELECT Id FROM AccountParticipant WHERE AccountId =: personAccount.Id];
                System.assertEquals(assertBefore.size(), assertAfter.size(), 'There was a change that was not supposed to be');
            }

	    	Test.stopTest();

		}
	}	


	private static InsurancePolicy insertInsurancePolicy(String insuredId) {
		InsurancePolicy ip = new InsurancePolicy();
		ip.NameInsuredId = insuredId;
		ip.Name = 'IP004TEST';
		insert ip;
		return ip;
	}

	private static InsurancePolicy insertInsurancePolicyWithProducer(String insuredId) {
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];

        Producer prod = new Producer();
        prod.Producer_ID__c = '77770';
        prod.Name = '77770';
        prod.InternalUserId = objUser.Id;
        prod.Type = 'Agency';
        insert prod;

        InsurancePolicy ip = new InsurancePolicy();
		ip.NameInsuredId = insuredId;
		ip.Name = 'IP004TEST';
        ip.ProducerId = prod.Id;
        System.debug(ip);
		insert ip;
		return ip;
	}

    private static Account insertPersonAccount(String firstName, String lastName, String individualType){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.FinServ__IndividualType__c = individualType;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;
    }
    
        
    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        return objAcc;
    }

    
    private static Account insertAgentAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;

        return objAcc;
    }


    private static AccountContactRelation insertAccountContactRelation(String accountId, String contactId) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
        return acr;
    }





        
    
}