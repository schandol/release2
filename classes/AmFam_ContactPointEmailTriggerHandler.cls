/**
*   @description trigger handler for ContactPointEmail object. Note: this should be the only place
*	where ALL AssociatedLocation related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 06/04/2020
*   @group Trigger
*/

public with sharing class AmFam_ContactPointEmailTriggerHandler {


    public void OnBeforeInsert (List<ContactPointEmail> newContactPointEmail) {
        //the below method is specially designed to prevent insert/update if the related Account lastmodified date is greater than CDH Queue Enabled Date
        AmFam_CDHQueueLoadValidation.validate(newContactPointEmail, null);
    }

    public void OnAfterInsert (List<ContactPointEmail> newContactPointEmail,Map<ID, ContactPointEmail> newContactPointEmailMap) {
        AmFam_ContactPointEmailService.updateHouseholdPrimaryEmail(newContactPointEmail,newContactPointEmailMap);
    }

    public void OnAfterUpdate (List<ContactPointEmail> newContactPointEmail,
                               List<ContactPointEmail> oldContactPointEmail,
                               Map<ID, ContactPointEmail> newContactPointEmailMap,
                               Map<ID, ContactPointEmail> oldContactPointEmailMap) {
                                AmFam_ContactPointEmailService.updateHouseholdPrimaryEmail(newContactPointEmail,newContactPointEmailMap);
    }

    public void OnBeforeUpdate(List<ContactPointEmail> newContactPointEmail,
                                List<ContactPointEmail> oldContactPointEmail,
                                Map<ID, ContactPointEmail> newContactPointEmailMap,
                                Map<ID, ContactPointEmail> oldContactPointEmailMap) {
                                 //the below method is specially designed to prevent insert/update if the related Account lastmodified date is greater than CDH Queue Enabled Date
                                 AmFam_CDHQueueLoadValidation.validate(newContactPointEmail, null);

    }
}