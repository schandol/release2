/**
*   @description: the batch class is designed to handle bulk Producer Access records update on SF
    Batch_Processed__c checkbox field would be set to true at the end of process.
*	@author: Anbu Chinnathambi
*   @date: 11/02/2021
*   @group: Batch Apex
*/

global class AmFam_ProducerAccessUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    public final Set<String> producerIds;
    
    public AmFam_ProducerAccessUpdateBatch(Set<String> Ids){
        this.producerIds = Ids;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        if (producerIds != null) {
            query = 'SELECT Id, Name, Producer_Identifier__c, RelatedId__c, Batch_Processed__c, Reason__c, CDHID__c FROM ProducerAccess__c where Batch_Processed__c = false AND Producer_Identifier__c IN :producerIds ORDER BY RelatedId__c, Producer_Identifier__c'; 
        } else {
            query = 'SELECT Id, Name, Producer_Identifier__c, RelatedId__c, Batch_Processed__c, Reason__c, CDHID__c FROM ProducerAccess__c where Batch_Processed__c = false ORDER BY RelatedId__c, Producer_Identifier__c';
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<ProducerAccess__c> records){
      
        AmFam_ProducerAccessTriggerService.createNewAccountShares(records);
        
        List<ProducerAccess__c> listOfRecordsToUpdate = new List<ProducerAccess__c>();
        for(ProducerAccess__c pa: records){
        	pa.Batch_Processed__c = true;
            listOfRecordsToUpdate.add(pa);
        }
        if(listOfRecordsToUpdate.size()>0) update listOfRecordsToUpdate;
    }    
    global void finish(Database.BatchableContext bc){
        
    }    
}