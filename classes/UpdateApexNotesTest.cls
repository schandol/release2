@isTest
private class UpdateApexNotesTest{
    @testSetup 
    static void setup() {
       AmFam_TestDataFactory.createAccountsWithLeads(2, 2);
       Lead [] leads = [SELECT ID, Contact_CDHID__c, Status, LastModifiedDate FROM Lead];
       Lead lead = leads[0];
       
       lead.Status = 'Completed';
       lead.Contact_CDHID__c= '123';
       
    }
    static testmethod void test() {        
        Test.startTest();
        UpdateApexNotes batchJob= new UpdateApexNotes();
        Id batchId = Database.executeBatch(batchJob);
        Test.stopTest();
        
        System.assertEquals(batchJob.recordsProcessed, 0);
        // after the testing stops, assert records were updated properly
    }
    
}