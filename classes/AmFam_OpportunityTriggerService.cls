public without sharing class AmFam_OpportunityTriggerService {
    
    public static void extendPCIFYToOpportunity() {
        //The below code extending PCIFY to Opportunity object
		if (pcify.Manager.isOnline('Opportunity')) { 
            pcify.Processor.maskCreditCards(Trigger.new, pcify.Manager.getMaskFields('Opportunity'),'Opportunity');
        }
    }

}