@IsTest
public class MobileTelephonyEventTriggerTest {
    @IsTest
    public static void invokeTrigger() {
        MobileTelephonyEvent event = new MobileTelephonyEvent(UserId = UserInfo.getUserId(), 
                                                     DeviceIdentifier = 'dac5bbffc407f470',
                                                     AppVersion = '1.0(1)',
                                                     AppPackageIdentifier  = 'com.salesforce.internal.securesdksampleapp',
                                                     OsName = 'Android',
                                                     OsVersion = '7.1.1',
                                                     DeviceModel = 'Android SDK built for x86',
                                                     PhoneNumber = '+1-555-555-5555',
                                                     OPERATION = 'SMS');
        
        Test.startTest();
        Database.SaveResult result = Database.insertImmediate(event);
        Test.stopTest();
        
        System.assert(result.isSuccess());
        
        Integer customCount = [Select Count() FROM MobileTelephonyObject__c WHERE UserId__c = :UserInfo.getUserId()];
        System.assertEquals(1, customCount);
        
    }
}