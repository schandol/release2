@IsTest
public with sharing class AmFam_UserUpdateFromCalloutQueueableTest {

    //!!!works if you disable call to AmFam_UserService.userAccessToAccountLead(newUser);
    //in UserTriggerHandler OnAfterUpdate

    @IsTest
    public static void testUpdateUser()
    {
        AmFam_UserUpdateFromCalloutQueueable.chainNextQueueable = false;
        AmFam_CreateUserAccountsQueueable.chainNextQueueable = false;

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();

        insert u;

        Test.startTest();

        List<Id> userList = new List<Id>{u.Id};
        AmFam_UserUpdateFromCalloutQueueable qOp = new AmFam_UserUpdateFromCalloutQueueable(userList);
        qOp.execute(null);

        Test.stopTest();

        User updated = [SELECT Producer_ID__c FROM User WHERE Id = :u.Id];
        System.assert(updated.Producer_ID__c != null);
    }

    @IsTest
    public static void testUpdateUserNoChange()
    {
        //AmFam_UserUpdateFromCalloutQueueable.chainNextQueueable = false;
        AmFam_CreateUserAccountsQueueable.chainNextQueueable = false;

        Test.setMock(HttpCalloutMock.class, new AmFam_ProducerTypeServiceHttpCalloutMock(true));

        User u = AmFam_UserUpdateFromCalloutQueueableTest.createTestUser();
        u.MiddleName = 'Jaime';
        u.Phone = '6028291211';
        u.Producer_ID__c = '73320';
        insert u;

        Test.startTest();

        List<Id> userList = new List<Id>{u.Id};
        AmFam_UserUpdateFromCalloutQueueable qOp = new AmFam_UserUpdateFromCalloutQueueable(userList);
        qOp.execute(null);

        Test.stopTest();

        User updated = [SELECT Producer_ID__c FROM User WHERE Id = :u.Id];
        System.assert(updated.Producer_ID__c != null);
    }

    //!!! going to need different profiles
    public static User createTestUser()
    {
        User u = new User();
        u.Email = 'dummy@email.com';
        u.Username = 'dummy@email.com.001';
        u.FederationIdentifier = 'fedid';
        u.LastName = 'Nobody';
        u.FirstName = 'Bob';
        u.AmFam_User_ID__c = 'BOB001';
        u.Alias = 'Dummy001';

        String uid = UserInfo.getUserId();
        User currentUser =
            [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        u.TimeZoneSidKey = currentUser.TimeZoneSidKey; 
        u.LocaleSidKey = currentUser.LocaleSidKey;
        u.EmailEncodingKey = currentUser.EmailEncodingKey;
        u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        
        String profileName = 'AmFam Agent';
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name=:profileName limit 1]; //Get profile id based on Name recieved from AD
        List<UserRole> roles = [SELECT Id, Name FROM UserRole];

        if(profiles.size()>0) {
            u.ProfileId = profiles[0].Id;
            for (UserRole r : roles) 
            {
                if (r.Name == 'CSR' && profileName == 'Agent CSR' ){
                    u.UserRoleId = r.Id;
                } 
                else if (r.Name == 'Agent' && profileName == 'AmFam Agent') 
                {
                    u.UserRoleId = r.Id;
                }
            }
        }

        return u;
    }
}