global class AmFam_SharingAccessForNewLeadsBatch implements database.batchable<sobject> {
    List<Lead> newLeads;
    public AmFam_SharingAccessForNewLeadsBatch (List<Lead> newLeads) {
        this.newLeads = newLeads;
    }

    global List<LeadShare> start(database.batchableContext bc){

        Set<Id> accIds = new Set<Id>();
        for (Lead l : newLeads) {
            accIds.add(l.Agency__c);
        }
        List<AccountContactRelation> contacts = [SELECT Id, AccountId, Contact.User_Federation_ID__c
                                                    FROM AccountContactRelation
                                                    WHERE AccountId IN :accIds AND Contact.User_Federation_ID__c != Null];

        Map<Id, List<String>> mapAccUser = new Map<id, List<String>>();
        for(AccountContactRelation acc: contacts) {
            if(mapAccUser.containskey(acc.AccountId)) {
                List<String> tempLst = mapAccUser.get(acc.AccountId);
                tempLst.add(acc.Contact.User_Federation_ID__c);
                mapAccUser.put(acc.AccountId,tempLst);
            }
            else {
                List<String> tempLst = new List<String>();
                tempLst.add(acc.Contact.User_Federation_ID__c);
                mapAccUser.put(acc.AccountId,tempLst);
            }
        }
        Map<String, Id> mapFedUserId = new Map<String, Id>();
        for(User usr :[SELECT Id, FederationIdentifier FROM User
                        WHERE (Profile.Name = 'Agent CSR' OR Profile.Name = 'AmFam Agent') AND FederationIdentifier != Null]) {
            mapFedUserId.put(usr.FederationIdentifier, usr.id);
         }
        //Add apex sharing records to Lead
        List<LeadShare> leadSharingAdd = new List<LeadShare>();
        if (mapAccUser.size() > 0) {
            for (Lead l : newLeads) {
                if (mapAccUser.get(l.Agency__c) != null){
                    for(String str : mapAccUser.get(l.Agency__c)) {
                        if (mapFedUserId.containsKey(str) && mapFedUserId.get(str) != l.OwnerId ) {
                            LeadShare leadShare = new LeadShare();
                            leadShare.LeadAccessLevel = 'Edit';
                            leadShare.LeadId = l.Id;
                            leadShare.UserOrGroupId = mapFedUserId.get(str);
                            leadSharingAdd.add(leadShare);
                        }
                    }
                }
            }
        }
        return leadSharingAdd;
    }

    global void execute(database.batchablecontext bd, List<LeadShare> leadSharingAdd) {
        if(leadSharingAdd !=null && leadSharingAdd.size() > 0) {
            Database.insert(leadSharingAdd, false);
        }
    }

    Public void finish(database.batchableContext bc) {
    }
}