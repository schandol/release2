/**
*   @description trigger handler for User object. Note: this should be the only place
*	where ALL User related trigger logic should be implemented.
*   @author Salesforce Services
*   @date 05/13/2020
*   @group Trigger
*/

public class AmFam_UserTriggerHandler {

    @TestVisible
    private static Boolean disableTriggerHandler = false;

    public void OnBeforeInsert (List<User> newUser) {

    }

    public void OnAfterInsert (List<User> newUser,Map<ID, User> newUserMap) {
        if (disableTriggerHandler) {
            return;
        }

        if (!Test.isRunningTest()) {
            System.enqueueJob(new AmFam_UserUpdateFromCalloutQueueable(new List<Id>(newUserMap.keySet())));
        }
    }

    
    public void OnAfterUpdate (List<User> newUser,
                               List<User> oldUser,
                               Map<ID, User> newUserMap,
                               Map<ID, User> oldUserMap) {
        if (!Test.isRunningTest()) {
            System.enqueueJob(new AmFam_UserUpdateFromCalloutQueueable(new List<Id>(newUserMap.keySet())));
        }
    }

    public void OnBeforeUpdate(List<User> newUser,
                                List<User> oldUser,
                                Map<ID, User> newUserMap,
                                Map<ID, User> oldUserMap) {

    }


}