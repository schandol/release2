public class AmFam_LeadAssociationToProducer {

    public static void leadAssociationToProducers(List<Lead> leadRecs) {
        List<Lead> leads = [Select Id, CDHID__c, Contact_CDHID__c, LastModifiedBy.Name, Agency__r.Is_Corporate_Account__c FROM Lead WHERE Id In : leadRecs];
        for (Lead l : leads) {
            if (l.LastModifiedBy.Name == 'User Integration'  && !l.Agency__r.Is_Corporate_Account__c && !System.isFuture() && !System.isBatch() && !System.isQueueable()) {
                if (l.CDHID__c != null || l.Contact_CDHID__c != null) {
                    leadAssociationToProducer(l.Id);
                }
            }
        }
    }

    @future(callout = true)
    public static void leadAssociationToProducer(Id leadId){
        Lead rec = [Select Id, Agency__r.Producer_Id__c, CDHID__c, Contact_CDHID__c, OwnerId, Party_Level__c FROM Lead WHERE Id =: leadId];
        associationProducerCallOut(rec);
    }

    public static void associationProducerCallOut(Lead rec){
        String CDHID = rec.Party_Level__c == 'Contact' ? rec.Contact_CDHID__c : rec.CDHID__c;
        User userRec = [SELECT AmFam_User_ID__c FROM User WHERE Id =: rec.ownerId Limit 1];
        wsPartyManageServiceCallout.PartyManageServiceSOAP associateProd = new wsPartyManageServiceCallout.PartyManageServiceSOAP();
        wsPartyManageService.AssociateProducerRequestType associateProducerRequest = new wsPartyManageService.AssociateProducerRequestType();
        associateProducerRequest.partyIdentifier = CDHID;
        associateProducerRequest.producerId = rec.Agency__r.Producer_Id__c;
        associateProducerRequest.PartyProducerReasonCode = 'LEAD';
        associateProducerRequest.sourceSystemName = 'SALESFORCE';
        associateProducerRequest.userId = userRec.AmFam_User_ID__c;
        associateProducerRequest.consumer = 'SALESFORCE';
        wsPartyManageService.AssociateProducerResultType resp;
        String errorLog = '';
            //call out
        try{
            resp = associateProd.associateProducer_Http(associateProducerRequest);
            if(resp.Errors != null) {
                for(wsPartyManageParty.ErrorType err : resp.Errors.Error) {
                    if(err.TypeOfError == 'ERROR') {
                        errorLog += err.ErrorCode + ' on field ' + err.field + ': ' + err.errorMessage + ' (CDH_ID :' + resp.Errors.partyIdentifierKey + ')';
                        AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
                        failureLog.createAPIFailureLogNoProducerReturned('AmFam_LeadAssociationToProducer', 'http://service.amfam.com/partymanageservice', resp.Errors.partyIdentifierKey, errorLog);
                    }
                }
            }
        } catch (Exception e) {
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLog('AmFam_LeadAssociationToProducer', null, 'http://service.amfam.com/partymanageservice', CDHID, e.getMessage());
        }
    }
}