public class wsPartySearchHelper {

    /*
    Makes a callout to PartySearchService for @CDHID with a static list of levelOfDetail.
    @args CDHID to retrieve
    */
    public static wsPartySearchService.RetrieveResponseType retrieveParty(String cdhId) {
    
        wsPartySearchServiceCallout.PartySearchServiceSOAP partySearchService = new wsPartySearchServiceCallout.PartySearchServiceSOAP();
        wsPartySearchService.RetrieveRequestType request = new wsPartySearchService.RetrieveRequestType();
        wsPartySearchService.RetrieveCriteriaType criteriaType = new wsPartySearchService.RetrieveCriteriaType();
        criteriaType.partyIdentifier = cdhId;
        criteriaType.LevelOfDetail = new List<String>();
        criteriaType.LevelOfDetail.add('DEMOGRAPHIC_INFORMATION');
        criteriaType.LevelOfDetail.add('IDENTITY_INFORMATION');
        criteriaType.LevelOfDetail.add('ADDRESSES');
        criteriaType.LevelOfDetail.add('PHONES');
        criteriaType.LevelOfDetail.add('EMAILS');
        request.RetrieveCriteria = criteriaType;
        
        wsPartySearchService.RetrieveResponseType response = partySearchService.retrieveParty_Http(request);
        
        return response;
    }
    
     public static Lead refreshLead(String leadRecordId) {
         Lead lead;
         if(leadRecordId != null){
           System.debug('LeadId: ' + leadRecordId);
           lead = [Select id, CDHID__c, Party_Version__c, Party_Level__c, Contact_CDHID__c, Additional_Phone_1__c, Additional_Phone_2__c, Additional_Phone_3__c, Phone, Email,
                        Additional_Email_1__c, Additional_Email_2__c
                        FROM Lead where id =: leadRecordId LIMIT 1]; 
            lead = refreshLead(lead);
             
         }
           return lead;
     }
    
        /*
        This method will use the appropriate CDHID from a lead record to callout PartySearchService and map over the appropriate CDH data.
        @args leadRecord with CDHID or ContactCDHID filled in.
        */
     public static Lead refreshLead(Lead leadRecord) {
         String lead_cdhId= leadRecord.Contact_CDHID__c != null ? leadRecord.Contact_CDHID__c : leadRecord.CDHID__c;
         //if CDHID is null either this lead record is not valid or the value was not queried for
         if(lead_cdhId == null) {
             return leadRecord;
         }
            
         wsPartySearchService.RetrieveResponseType partySearchResponse = retrieveParty(lead_cdhId);
         if(partySearchResponse.Party == null){
             return leadRecord;
         }
         //Update System Fields
         leadRecord.Party_Version__c = partySearchResponse.Party.partyVersion;
         leadRecord.Party_Level__c = partySearchResponse.Party.PartyLevelCode == 'ENTRANT' ? 'Entrant' : 'Contact';
         leadRecord.Partner__c = partySearchResponse.Party.partnerIdentifier;
         if((partySearchResponse?.Party?.PartyLevelCode == 'Contact' || partySearchResponse?.Party?.PartyLevelCode == 'CONTACT') && leadRecord.Contact_CDHID__c == null){
            leadRecord.Contact_CDHID__c = partySearchResponse.Party.partyIdentifier;
         }
            
         if (partySearchResponse.Party.Entrant != null)
         {
            leadRecord = mapEntrantToLead(partySearchResponse.Party.Entrant, leadRecord);
         } else if (partySearchResponse.Party.Contact != null) {
            leadRecord = mapContactToLead(partySearchResponse.Party.Contact, leadRecord);
         }
         
         return leadRecord;

     }
    public static Lead mapContactToLead(wsPartyManageParty.ContactType contactResponse, lead leadRecord) {
        if(contactResponse.Person != null)
        {
            wsPartyManageParty.PersonDemographicInfoType demographicInfo = contactResponse.Person.PersonDemographicInfo;
            if(demographicInfo != null) {
                if(demographicInfo.Name != null){
                    leadRecord.FirstName = demographicInfo.Name.firstName;
                    leadRecord.MiddleName = demographicInfo.Name.middleName;
                    leadRecord.LastName = demographicInfo.Name.lastName;
                }
                leadRecord.Gender__c = demographicInfo.Gender;
                leadRecord.Marital_Status__c = demographicInfo.MaritalStatus;
                leadRecord.Date_of_Birth__c = demographicInfo.birthDate == null ? null : Date.valueOf(demographicInfo.birthDate);
            }
            
            //Phones
            leadRecord = AmFam_Utility.mapPhones(contactResponse.PartyPhone, leadRecord);
            
            //Emails
            leadRecord = AmFam_Utility.mapEmails(contactResponse.PartyEmail, leadRecord);
            
            //Addresses
            leadRecord = AmFam_Utility.mapAddresses(contactResponse.PartyAddress, leadRecord);
        }
        return leadRecord;
    }
        
    public static Lead mapEntrantToLead (wsPartyManageParty.EntrantType entrantResponse, Lead leadRecord) {
        // Demographic Info 
        wsPartyManageParty.EntrantDemographicInfoType demographicInfo = entrantResponse.EntrantDemographicInfo;
        if(demographicInfo != null) {
            if (demographicInfo.PersonName != null) {
                if(demographicInfo?.PersonName?.firstName != null){
                    leadRecord.FirstName = demographicInfo.PersonName.firstName;
                }
                if(demographicInfo?.PersonName?.lastName != null){
                    leadRecord.LastName = demographicInfo.PersonName.lastName;    
                }
                if(demographicInfo?.PersonName?.middleName != null){
                    leadRecord.MiddleName = demographicInfo?.PersonName?.middleName;    
                }
            }
            leadRecord.Gender__c = demographicInfo.Gender;
            leadRecord.Marital_Status__c = demographicInfo.MaritalStatus;
            leadRecord.Date_of_Birth__c = demographicInfo.birthDate == null ? null : Date.valueOf(demographicInfo.birthDate);
        
            String companyName = '';
            if(demographicInfo.OrganizationName != null) {
                companyName = demographicInfo.OrganizationName.organizationName; 
            }
            leadRecord.Company = companyName;
        }
        
        //Identity Info
        wsPartyManageParty.EntrantIdentityInfoType identityInfo = entrantResponse.EntrantIdentityInfo;
        if(identityInfo != null) {
            wsPartyManageParty.DriversLicenseType driversLicenseInfo = identityInfo.DriversLicense;
            String licenseNumber = '';
            String licenseState = '';
            String licenseMonth = '';
            String licenseYear = '';
            
            if(driversLicenseInfo != null) {
                licenseNumber = driversLicenseInfo.licenseNumber;
                licenseState = driversLicenseInfo.licenseState;
                if(driversLicenseInfo.OriginalLicenseDate != null) {
                    licenseMonth = driversLicenseInfo.OriginalLicenseDate.licenseMonthString;
                    licenseYear = driversLicenseInfo.OriginalLicenseDate.licenseYearString;
                }
            }
            
            leadRecord.Drivers_License_Issue_Month__c = licenseMonth;
            leadRecord.Drivers_License_Issue_Year__c = licenseYear;
            leadRecord.Drivers_License_Number__c = licenseNumber;
            leadRecord.Driver_s_License_State__c = licenseState;
        }
        //Phones
        leadRecord = AmFam_Utility.mapPhones(entrantResponse.PartyPhone, leadRecord);
        //Emails
        leadRecord = AmFam_Utility.mapEmails(entrantResponse.PartyEmail, leadRecord);
        //Addresses
        leadRecord = AmFam_Utility.mapAddresses(entrantResponse.PartyAddress, leadRecord);
        return leadRecord;
    }

}