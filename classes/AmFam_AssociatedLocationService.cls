/**
*   @description  Service class for the AssociatedLocation trigger handler
*
*   @author Salesforce Services
*   @date 5/28/2021
*   @group Service
*/
public with sharing class AmFam_AssociatedLocationService {
    public AmFam_AssociatedLocationService() {

    }

    public static void updatePrimaryAddress(List<AssociatedLocation> locations,Map<Id,AssociatedLocation> locationsMap)
    {
        List<Account> updatedAccounts = new List<Account>();
        Map<Id,Id> person2LocMap = new Map<Id,Id>();
        Map<Id, Map<String,Schema.Address>> addressMap = createAddressMap(locations);

        //create map of PersonAcctId->AssLoc
        for (AssociatedLocation loc : locations)
        {
            //create map of personacctId -> associatedlocId for AssLoc Purpose marked primary (as we only care about updating for Primary), 
            if (loc.Purpose__c == 'PRM' && (loc.Type == 'M' || loc.Type == 'R'))
            {
                person2LocMap.put(loc.PersonAccount__c,loc.Id);
            }
        }

        //System.debug('PERSON2LOC:'+person2LocMap);

        //get the contacts for the referenced accounts
        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId IN :person2LocMap.keySet()];
        List<Id> contactIds = new List<Id>();
        for (Contact contact : contacts)
        {
            contactIds.add(contact.Id);
        }
        //System.debug('CONTACTIDS:'+contactIds);

        //if we do have something that we should try to update
        if (!person2LocMap.isEmpty())
        {
            List<AccountContactRelation> relations  =  [SELECT Id,AccountId,ContactId,Contact.AccountId,Contact.Account.Primary_Contact__pc
                                                        FROM AccountContactRelation
                                                        WHERE Contact.Account.Primary_Contact__pc = true AND ContactId IN :contactIds];


            //System.debug('RELATIONS:'+relations);
            for (AccountContactRelation relation : relations)
            {
                AssociatedLocation assLoc = locationsMap.get(person2LocMap.get(relation.Contact.AccountId));
                if (assLoc != null)
                {
                    //get the appropriate Address matching the the AssLoc Type
                    Map<String,Schema.Address> addrEntries = addressMap.get(assLoc.Id);
                    if (addrEntries != null) {
                        //System.debug('ADDRENTRIES:'+addrEntries);
                        Schema.Address addr = addrEntries.get(assLoc.Type);

                        if (addr != null)
                        {
                            Account acct = new Account();
                            acct.Id = relation.AccountId;
                            if (assLoc.Type == 'M')
                            {
                                acct.ShippingStreet = addr.Street;
                                acct.ShippingCity = addr.City;
                                acct.ShippingState = addr.State;
                                acct.ShippingPostalCode = addr.PostalCode;
                                acct.ShippingGeocodeAccuracy = addr.GeocodeAccuracy;
                                acct.ShippingCountry = addr.Country;
                                acct.ShippingLatitude = addr.Latitude;
                                acct.ShippingLongitude = addr.Longitude;
                                acct.ShippingStateCode = addr.StateCode;
                            }
                            else
                            {
                                acct.BillingStreet = addr.Street;
                                acct.BillingCity = addr.City;
                                acct.BillingState = addr.State;
                                acct.BillingPostalCode = addr.PostalCode;
                                acct.BillingGeocodeAccuracy = addr.GeocodeAccuracy;
                                acct.BillingCountry = addr.Country;
                                acct.BillingLatitude = addr.Latitude;
                                acct.BillingLongitude = addr.Longitude;
                                acct.BillingStateCode = addr.StateCode;
                            }
                            updatedAccounts.add(acct);
                        }
                    }
                }
            }
        }

        //System.debug('UPDATEDACCOUNTS:'+updatedAccounts);
        if (!updatedAccounts.isEmpty())
        {
            update updatedAccounts;
        }
    }

    // map of associated location id and map with available addresses keyed on type, purpose is to be able to directly
    // get to address without having to go through Location

    //<AssociatedLocationId, Map<AddressType,Address>>
    
    public static Map<Id, Map<String,Schema.Address>> createAddressMap(List<AssociatedLocation> associatedLocations)
    {
        List<Id> locationIds = new List<Id>();
        Map<Id,AssociatedLocation> associatedLocationsIdMap = new Map<Id,AssociatedLocation>();
        Map<Id,Id> loc2AssLocMap = new Map<Id,Id>();

        for (AssociatedLocation al : associatedLocations)
        {
            locationIds.add(al.LocationId);
            associatedLocationsIdMap.put(al.Id,al);
            loc2AssLocMap.put(al.LocationId,al.Id);
        }

        List<Schema.Address> addresses = [SELECT Id,ParentId,AddressType,Street,City,State,PostalCode,Country,StateCOde,GeocodeAccuracy,Latitude,Longitude,Parent.LocationType 
                                            FROM Address WHERE ParentId in :locationIds];

        Map<Id, Map<String,Schema.Address>> addressMap = new Map<Id, Map<String,Schema.Address>>();
        for (Schema.Address address : addresses)
        {
            //get assloc associated with address
            Id alId = loc2AssLocMap.get(address.ParentId);
            Map<String,Schema.Address> addressTypeMap;

            if (!addressMap.containsKey(alId))
            {
                addressTypeMap = new Map<String,Schema.Address>();
                addressMap.put(alId, addressTypeMap);
            }
            else
            {
                addressTypeMap = addressMap.get(alId);
            }

            //addressTypeMap.put(address.AddressType,Address);
            addressTypeMap.put(address.Parent.LocationType,Address);
        }

        return addressMap;
    }
}