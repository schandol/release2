/**
*   @description: the scheduler class helps to schedule the AmFam_ProducerAccessUpdateBatch batch 
*	@author: Anbu Chinnathambi
*   @date: 11/02/2021
*   @group: Scheduler Apex
*/

global class AmFam_ProducerAccessUpdateBatchScheduler implements Schedulable {

    global void execute(SchedulableContext ctx) {
        AmFam_ProducerAccessUpdateBatch batch = new AmFam_ProducerAccessUpdateBatch(null);
        Database.ExecuteBatch(batch,200);
    } 
}