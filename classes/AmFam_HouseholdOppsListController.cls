public with sharing class AmFam_HouseholdOppsListController {

    @AuraEnabled(cacheable=true)
    public static List<Opportunity> getHouseholdOpportunities(Id householdAccountId) 
    {
        List<AccountContactRelation> relations = [SELECT Id,AccountId,ContactId
                                                                FROM AccountContactRelation
                                                                WHERE AccountId = :householdAccountId];

        List<Id> memberIds = new List<Id>();
        for (AccountContactRelation relation : relations)
        {
            memberIds.add(relation.ContactId);
        }

        List<Contact> contacts = [SELECT Id,AccountId FROM Contact WHERE Id IN :memberIds];

        List<Id> acctIds = new List<Id>();
        for (Contact contact : contacts)
        {
            acctIds.add(contact.AccountId);
        }

        List<Opportunity> opps = [SELECT Id,AccountId,Account.Name,Name,Amount,StageName,Type,Lines_of_Business__c 
                                    FROM Opportunity 
                                    WHERE AccountId IN :acctIds
                                    ORDER BY Name];
        return opps;
    }
}