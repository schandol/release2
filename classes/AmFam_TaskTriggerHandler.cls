public class AmFam_TaskTriggerHandler {
    public void OnBeforeInsert (List<Task> newTasks) {
    }
    public void OnAfterInsert (List<Task> newTasks) {
        AmFam_TaskService.incrementContactAttemptOnLead(newTasks);
    }
}