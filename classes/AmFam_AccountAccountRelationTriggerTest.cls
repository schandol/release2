/**
*   @description: the apex class is test class for AmFam_AccountAccountTrigger trigger and related classes
*	@author: Anbu Chinnathambi
*   @date: 08/10/2021
*   @group: Apex Class
*/
@isTest
private class AmFam_AccountAccountRelationTriggerTest {
    
   @TestSetup
    static void bypassProcessBuilderSettings() {
        ByPass_Process_Builder__c settings = ByPass_Process_Builder__c.getOrgDefaults();
        settings.Bypass_Process_Builder__c = true;
        upsert settings ByPass_Process_Builder__c.Id;
    }
    
    //Method to test deleteDuplicateAccountAccountRel method on AmFam_AccountAccountRelationshipService class
    static testMethod void testdeleteDuplicateAAR(){
        
        //Create HH and Person Accounts
        Account hhAccount = AmFam_TestDataFactory.insertHouseholdAccount('TestHHOne','112244');
        Account personAccount = AmFam_TestDataFactory.insertPersonAccount('TestOne','PersonOne','Individual');
        
        Id rrContactRTId = Schema.SObjectType.FinServ__ReciprocalRole__c.getRecordTypeInfosByDeveloperName().get('AccountRole').getRecordTypeId();
        FinServ__ReciprocalRole__c rrCon = new FinServ__ReciprocalRole__c();
        rrCon.Name = 'Test ';  
        rrCon.FinServ__InverseRole__c = 'Inverse Role';
        rrCon.RecordTypeId = rrContactRTId;
        insert rrCon;
        
        FinServ__AccountAccountRelation__c conConRelation = new FinServ__AccountAccountRelation__c();
        conConRelation.FinServ__Account__c = hhAccount.id;
        conConRelation.FinServ__RelatedAccount__c = personAccount.id;
        conConRelation.FinServ__Role__c = rrCon.id;
        insert conConRelation;
        
        
        Test.startTest();
        AmFam_checkRecursive.firstCallAmFam_AccountAccountRelationshipService = false;
        FinServ__AccountAccountRelation__c conConRelation2 = new FinServ__AccountAccountRelation__c();
        conConRelation2.FinServ__Account__c = personAccount.id;
        conConRelation2.FinServ__RelatedAccount__c = hhAccount.id;
        conConRelation2.FinServ__Role__c = rrCon.id;
        insert conConRelation2;
        Test.stopTest();
        
        
        List<FinServ__AccountAccountRelation__c> aarList = [select id, FinServ__Role__c from FinServ__AccountAccountRelation__c Limit 100];
        //total Contact-Contact Relationship would be 4(2 sets), where 2 duplicates should have been deleted then number should be 2.
        system.debug('aarList' + aarList);
        System.assert(aarList.size()==2);
     }
}