@IsTest
public with sharing class AmFam_ContactPointPhoneTriggerHndlrTest {

    private class SetupObjects
    {
        Id householdId;
        Id person1Id;
        Id person2Id;
    }

    private static SetupObjects setup() {

        SetupObjects so = new SetupObjects();

        System.debug('RUNNING SETUP');
        Account household = AmFam_TestDataFactory.insertHouseholdAccount('Test Household','112233');
        household.Phone = '9876543210';
        update household;
        so.householdId = household.Id;


        //Create PersonAccounts
        Account person1 = AmFam_TestDataFactory.insertPersonAccount('Bob','Person1','Individual');
        person1.Primary_Contact__pc = true;
        update person1;
        so.person1Id = person1.Id;

        Account person2 = AmFam_TestDataFactory.insertPersonAccount('Phil','Person2','Individual');
        so.person2Id = person2.Id;

        List<Contact> contacts = [SELECT Id FROM Contact];

        List<AccountContactRelation> rels = new List<AccountContactRelation>();
        for (Contact contact : contacts)
        {
            AccountContactRelation rel = new AccountContactRelation();
            rel.AccountId = household.Id;
            rel.ContactId = contact.Id;
            rel.Roles = 'Client';
            rel.IsActive = true;
            rels.add(rel);
        }
        
        insert rels;

        return so;
	} 

    public static testmethod void testPositive()
    {
        SetupObjects so = AmFam_ContactPointPhoneTriggerHndlrTest.setup();

        String phoneNumber = '2223334444';
        ContactPointPhone cpPhone = new ContactPointPhone();
        cpPhone.TelephoneNumber = phoneNumber;
        cpPhone.ParentId = so.person1Id;
        cpPhone.IsPrimary = true;
        cpPhone.UsageType = 'Home';
        cpPhone.RecordTypeId = AmFam_Utility.getContactPointPhonePersonRecordType();
        insert cpPhone;
        System.debug('INSERTED PHONE');

        Account hh = [SELECT Phone FROM Account WHERE Id = :so.householdId LIMIT 1];
        System.debug(hh);
        System.assert(hh.Phone == phoneNumber);
   }

    public static testmethod void testNegative()
    {
        SetupObjects so = AmFam_ContactPointPhoneTriggerHndlrTest.setup();

        String phoneNumber = '2223334444';
        ContactPointPhone cpPhone = new ContactPointPhone();
        cpPhone.TelephoneNumber = phoneNumber;
        cpPhone.ParentId = so.person2Id;
        cpPhone.IsPrimary = true;
        cpPhone.UsageType  = 'Home';
        cpPhone.RecordTypeId = AmFam_Utility.getContactPointPhonePersonRecordType();
        insert cpPhone;

        Account hh = [SELECT Phone FROM Account WHERE Id = :so.householdId LIMIT 1];
        System.debug(hh);
        System.assert(hh.Phone != phoneNumber);
      }
}