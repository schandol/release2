@IsTest
public class MobileScreenshotEventTriggerTest {
    @IsTest
    public static void invokeTrigger() {
        MobileScreenshotEvent event = new MobileScreenshotEvent(UserId = UserInfo.getUserId(), 
                                                     DeviceIdentifier = 'dac5bbffc407f470',
                                                     AppVersion = '1.0(1)',
                                                     AppPackageIdentifier  = 'com.salesforce.internal.securesdksampleapp',
                                                     OsName = 'Android',
                                                     OsVersion = '7.1.1',
                                                     DeviceModel = 'Android SDK built for x86',
                                                     ScreenDescription = 'Test description');
        
        Test.startTest();
        Database.SaveResult result = Database.insertImmediate(event);
        Test.stopTest();
        
        System.assert(result.isSuccess());
        
        Integer customCount = [Select Count() FROM MobileScreenshotObject__c WHERE UserId__c = :UserInfo.getUserId()];
        System.assertEquals(1, customCount);
        
    }
}