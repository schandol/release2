public class AmFam_LeadAssociationToProducerQueueable implements Queueable, Database.AllowsCallouts {
    public Lead leadRec;

    public AmFam_LeadAssociationToProducerQueueable(Lead leadRec) {
        this.leadRec = leadRec;
    }

    public void execute (QueueableContext context) {
        AmFam_LeadAssociationToProducer.associationProducerCallOut(leadRec);
    }
}