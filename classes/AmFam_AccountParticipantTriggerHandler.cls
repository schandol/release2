public without sharing class AmFam_AccountParticipantTriggerHandler {
    
    /*
    public void OnBeforeInsert (List<AccountParticipant> newRecords) {


    }
    */

    public void OnAfterInsert (List<AccountParticipant> newRecords) {

        //DISABLED UNTIL NEW PRODUCER ACCESS Grant gets updated
    	//AmFam_AccountParticipantService.shareAccessToRelatedRecords(newRecords);
    }

    /*
    public void OnAfterUpdate (List<AccountParticipant> newRecords,
                               List<AccountParticipant> oldRecords,
                               Map<ID, AccountParticipant> newARecordsMap,
                               Map<ID, AccountParticipant> oldRecordsMap) {

    }


    public void OnBeforeUpdate(List<AccountParticipant> newRecord,
                               List<AccountParticipant> oldRecord,
                               Map<ID, AccountParticipant> newRecordsMap,
                               Map<ID, AccountParticipant> oldRecordsMap) {

    }

    public void OnBeforeDelete(List<AccountParticipant> oldRecords,
    						   Map<ID, AccountParticipant> oldRecordsMap) {

    }
    */
    
    public void OnAfterDelete(List<AccountParticipant> oldRecords,
    						   Map<ID, AccountParticipant> oldRecordsMap) {

        //DISABLED UNTIL NEW PRODUCER ACCESS Grant gets updated
    	//AmFam_AccountParticipantService.removeAccessFromRelatedRecords(oldRecords);


    }



}