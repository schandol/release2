//Generated by FuseIT WSDL2Apex (http://www.fuseit.com/Solutions/SFDC-Explorer/Help-WSDL-Parser.aspx)

@isTest
private class ApexActServiceWebSvcTest {
	
	@isTest static void coverGeneratedCodeTypes() {
		Test.setMock(WebServiceMock.class, new ApexActServiceMockImpl());
		ApexActService parentObject = new ApexActService();
		DOM.Document doc = new DOM.Document();
		DOM.XmlNode mockNode = doc.createRootElement('Envelope', 'http://schemas.xmlsoap.org/soap/envelope/', 'env');
		new schemaAmfamComXsdMessageApexact.ActCategoryType(mockNode);				
		new schemaAmfamComXsdMessageApexact.ActResultType(mockNode);				
		new schemaAmfamComXsdMessageApexact.ActSubCategoryType(mockNode);				
		new schemaAmfamComXsdMessageApexact.ActTemplateType(mockNode);				
		new schemaAmfamComXsdMessageApexact.AuditType(mockNode);				
		new schemaAmfamComXsdMessageApexact.CreateActDetailType(mockNode);				
		new schemaAmfamComXsdMessageApexact.CreateActRequestType(mockNode);				
		new schemaAmfamComXsdMessageApexact.createActResponse(mockNode);				
		new schemaAmfamComXsdMessageApexact.CreateActResultType(mockNode);				
		new schemaAmfamComXsdMessageApexact.CreateNoteDetailType(mockNode);				
		new schemaAmfamComXsdMessageApexact.EntityParticipantResultType(mockNode);				
		new schemaAmfamComXsdMessageApexact.EntityParticipantType(mockNode);				
		new schemaAmfamComXsdMessageApexact.RetrieveCategoriesRequestType(mockNode);				
		new schemaAmfamComXsdMessageApexact.retrieveCategoriesResponse(mockNode);				
		new schemaAmfamComXsdMessageApexact.RetrieveCategoriesResultType(mockNode);				
		new schemaAmfamComXsdMessageApexact.serviceFault(mockNode);				
		new schemaAmfamComXsdMessageApexact.UserParticipantResultType(mockNode);				
		schemaAmfamComXsdMessageApexact.ActCategoryType Obj1 = new schemaAmfamComXsdMessageApexact.ActCategoryType();
		Obj1.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.ParseDateTime('2014-11-05T13:15:30Z');
		schemaAmfamComXsdMessageApexact.ActResultType Obj2 = new schemaAmfamComXsdMessageApexact.ActResultType();
		Obj2.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.ActSubCategoryType Obj3 = new schemaAmfamComXsdMessageApexact.ActSubCategoryType();
		Obj3.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.ActTemplateType Obj4 = new schemaAmfamComXsdMessageApexact.ActTemplateType();
		Obj4.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.AuditType Obj5 = new schemaAmfamComXsdMessageApexact.AuditType();
		Obj5.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.createAct Obj6 = new schemaAmfamComXsdMessageApexact.createAct();
		Obj6.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.CreateActDetailType Obj7 = new schemaAmfamComXsdMessageApexact.CreateActDetailType();
		Obj7.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.CreateActRequestType Obj8 = new schemaAmfamComXsdMessageApexact.CreateActRequestType();
		Obj8.populateXmlNode(mockNode);
		new schemaAmfamComXsdMessageApexact.createActResponse();
		schemaAmfamComXsdMessageApexact.CreateActResultType Obj10 = new schemaAmfamComXsdMessageApexact.CreateActResultType();
		Obj10.populateXmlNode(mockNode);
		new schemaAmfamComXsdMessageApexact.CreateActTemplateType();
		schemaAmfamComXsdMessageApexact.CreateNoteDetailType Obj12 = new schemaAmfamComXsdMessageApexact.CreateNoteDetailType();
		Obj12.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.EntityParticipantResultType Obj13 = new schemaAmfamComXsdMessageApexact.EntityParticipantResultType();
		Obj13.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.EntityParticipantType Obj14 = new schemaAmfamComXsdMessageApexact.EntityParticipantType();
		Obj14.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.retrieveCategories Obj15 = new schemaAmfamComXsdMessageApexact.retrieveCategories();
		Obj15.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.RetrieveCategoriesRequestType Obj16 = new schemaAmfamComXsdMessageApexact.RetrieveCategoriesRequestType();
		Obj16.populateXmlNode(mockNode);
		new schemaAmfamComXsdMessageApexact.retrieveCategoriesResponse();
		schemaAmfamComXsdMessageApexact.RetrieveCategoriesResultType Obj18 = new schemaAmfamComXsdMessageApexact.RetrieveCategoriesResultType();
		Obj18.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.serviceFault Obj19 = new schemaAmfamComXsdMessageApexact.serviceFault();
		Obj19.populateXmlNode(mockNode);
		schemaAmfamComXsdMessageApexact.UserParticipantResultType Obj20 = new schemaAmfamComXsdMessageApexact.UserParticipantResultType();
		Obj20.populateXmlNode(mockNode);
	}
	@isTest static void coverCodeForcreateAct(){
		Test.setMock(WebServiceMock.class, new ApexActServiceMockImpl());
		ApexActService.Act testObject = new ApexActService.Act();
		System.assertEquals(null, testObject.createAct(null));
	}
	@isTest static void coverCodeForretrieveCategories(){
		Test.setMock(WebServiceMock.class, new ApexActServiceMockImpl());
		ApexActService.Act testObject = new ApexActService.Act();
		System.assertEquals(null, testObject.retrieveCategories(null));
	}
	@isTest static void coverCodeForcreateAct_Http(){
		Test.setMock(HttpCalloutMock.class, new ApexActServiceHttpMock());
		ApexActService.Act testObject = new ApexActService.Act();
		System.assertEquals(null, testObject.createAct_Http(null));		
	}
	@isTest static void coverCodeForretrieveCategories_Http(){
		Test.setMock(HttpCalloutMock.class, new ApexActServiceHttpMock());
		ApexActService.Act testObject = new ApexActService.Act();
		System.assertEquals(null, testObject.retrieveCategories_Http(null));		
	}
}