@isTest 
public class AmFam_AddressTrigger_Test {

    static testMethod void testAddAddressUpdate_Mailing(){
        Account agencyAccount = insertBusinessAccount('Agency Account');

        Schema.Address address = [SELECT Id, City, Street FROM Address WHERE LocationType = 'Location' LIMIT 1];

        Test.startTest();
        address.City = 'Orlando';
        address.Street = '123 Main Boulevard';

        update address;
        Test.stopTest();

        Account acc = [SELECT Id, ShippingCity, BillingCity, ShippingStreet, BillingStreet FROM Account WHERE Id =: agencyAccount.Id LIMIT 1];
        //System.assertEquals(acc.ShippingCity, address.City, 'Account Address not updated properly');
        //System.assertEquals(acc.ShippingStreet, address.Street, 'Account Address not updated properly');
    }

    static testMethod void testAddAddressUpdate_Residence(){
        Account agencyAccount = insertBusinessAccount('Agency Account');

        system.debug([SELECT Id, City, Street, LocationType FROM Address]);

        Schema.Address address = [SELECT Id, City, Street FROM Address WHERE LocationType = 'Location' LIMIT 1];

        Test.startTest();
        address.City = 'Orlando';
        address.Street = '123 Main Boulevard';

        update address;
        Test.stopTest();

        Account acc = [SELECT Id, ShippingCity, BillingCity, ShippingStreet, BillingStreet FROM Account WHERE Id =: agencyAccount.Id LIMIT 1];
        //System.assertEquals(acc.BillingCity, address.City, 'Account Address not updated properly');
        //System.assertEquals(acc.BillingStreet, address.Street, 'Account Address not updated properly');
    }

    static testMethod void testAddAddressInsert_Residence(){
        Test.startTest();
        Account agencyAccount = insertBusinessAccount('Agency Account');
        Test.stopTest();

        Schema.Address address = [SELECT Id, City, Street FROM Address WHERE LocationType = 'Location' LIMIT 1];

        Account acc = [SELECT Id, ShippingCity, BillingCity, ShippingStreet, BillingStreet FROM Account WHERE Id =: agencyAccount.Id LIMIT 1];
        //System.assertEquals(acc.BillingCity, address.City, 'Account Address not updated properly');
        //System.assertEquals(acc.BillingStreet, address.Street, 'Account Address not updated properly');
    }

    static testMethod void testAddAddressInsert_Mailing(){
        Test.startTest();
        Account agencyAccount = insertBusinessAccount('Agency Account');
        Test.stopTest();

        Schema.Address address = [SELECT Id, City, Street FROM Address WHERE LocationType = 'Location' LIMIT 1];

        Account acc = [SELECT Id, ShippingCity, BillingCity, ShippingStreet, BillingStreet FROM Account WHERE Id =: agencyAccount.Id LIMIT 1];
        //System.assertEquals(acc.ShippingCity, address.City, 'Account Address not updated properly');
        //System.assertEquals(acc.ShippingStreet, address.Street, 'Account Address not updated properly');
    }

    private static Account insertBusinessAccount(String accName){
        Id devRecordTypeId =   AmFam_Utility.getAccountBusinessRecordType();
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        objAcc.OwnerId = UserInfo.getUserId();
        insert objAcc;

        objAcc.ShippingStreet = '123 Main Street';
        objAcc.ShippingCity = 'Miami';
        objAcc.ShippingStateCode = 'FL';
        objAcc.ShippingPostalCode = '123454';
        objAcc.ShippingCountryCode = 'US';

        objAcc.BillingStreet = '123 Main Street';
        objAcc.BillingCity = 'Miami';
        objAcc.BillingStateCode = 'FL';
        objAcc.BillingPostalCode = '123454';
        objAcc.BillingCountryCode = 'US';

        List<wsPartyManageParty.PartyAddressType> addresses = wsPartyManage.getPartyAddressAccount(objAcc, false, null, null);
        AmFam_AccountService.createsAddressRecord(objAcc.Id, addresses);

        return objAcc;
    }
}