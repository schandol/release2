public without sharing class AmFam_ProducerTriggerService {
    
    public static void createNewAccountShares(List<Producer> newRecords) {

        Map<String, List<Producer>> mapProducer = new Map<String, List<Producer>>();

        for (Producer pr : newRecords) {

            String prIdentifier = pr.Name;

            List<Producer> prList = mapProducer.get(prIdentifier);
            if (prList == null) {
                prList = new List<Producer>();
            }

            prList.add(pr);
            mapProducer.put(prIdentifier, prList);

        }

        // Retieve all ProducerAccess related to the Producer Identifier
        List<ProducerAccess__c> producerAccessList = [SELECT Id, RelatedId__c, Producer_Identifier__c, Reason__c FROM ProducerAccess__c WHERE Producer_Identifier__c IN:  mapProducer.keySet() ];


        //Create new AccountShares

        List<AccountShare> accountShareList = new List<AccountShare>();
        List<AccountShare> newAccountShareListToInsert = new List<AccountShare>();

        Set<Id> userdIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        for (ProducerAccess__c pa : producerAccessList) {

            String producerIdentifier = pa.Producer_Identifier__c;
            String reasonCode = pa.Reason__c;
            if (String.isNotEmpty(reasonCode)) {
                reasonCode = reasonCode.toLowerCase();
            }

            List<Producer> producerList = mapProducer.get(producerIdentifier);

            if (producerList != null) {
                for (Producer pr : producerList) {
                    AccountShare accShare = new AccountShare();
                    accShare.AccountId = pa.RelatedId__c;
                    accShare.UserOrGroupId = pr.InternalUserId;
                    accShare.AccountAccessLevel = reasonCode == 'lead' ? 'Read' : 'Edit';
                    accShare.OpportunityAccessLevel = accShare.AccountAccessLevel;

                    accountShareList.add(accShare);


                    accountIds.add(pa.RelatedId__c);
                    userdIds.add(pr.InternalUserId);


                }
            }
        }

        //Remove existing AccountShares from the list to insert
        List<AccountShare> existingShares = [SELECT Id, AccountId, UserOrGroupId FROM AccountShare WHERE AccountId IN: accountIds AND UserOrGroupId IN: userdIds];

        Set<String> existingKeys = new Set<String>();
        for (AccountShare accs : existingShares ) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            existingKeys.add(key);
        }

        for (AccountShare accs : accountShareList) {
            String key = accs.AccountId + '-' + accs.UserOrGroupId;
            if (!existingKeys.contains(key) ) {

                //Add it to avoid creating duplicates
                existingKeys.add(key);

                accs.CaseAccessLevel = 'None';
                accs.RowCause = 'Manual';

                newAccountShareListToInsert.add(accs);
            }
        }

        if (newAccountShareListToInsert.size() > 0) {
            insert newAccountShareListToInsert;


            //Cal the InteractionSummaryService to extend the access to Interaction Summaries
            AmFam_InteractionSummaryService.grantSameAccessFromAccount(newAccountShareListToInsert);
        }        

    }


    public static void removeAccountShares(List<Producer> oldRecords, Map<ID, Producer> oldRecordsMap) { 

        Map<String, List<Producer>> mapProducer = new Map<String, List<Producer>>();
        Map<String, Set<String>> userMap = new Map<String, Set<String>>();
        Set<Id> userdIds = new Set<Id>();


        for (Producer pr : oldRecords) {

            String prIdentifier = pr.Name;

            List<Producer> prList = mapProducer.get(prIdentifier);
            if (prList == null) {
                prList = new List<Producer>();
            }

            prList.add(pr);
            mapProducer.put(prIdentifier, prList);
            userdIds.add(pr.InternalUserId);

            String userId = pr.InternalUserId;

            Set<String> prListForUser = userMap.get(userId);
            if (prListForUser == null) {
                prListForUser = new Set<String>();
            }

            prListForUser.add(pr.Name);
            userMap.put(userId, prListForUser);            
        }

        List<String> prIdList = new List<String>(mapProducer.keySet());
        if (mapProducer != null && mapProducer.size() > 0) {
            AmFam_ProcessProducerDelete rsb = new AmFam_ProcessProducerDelete(userMap, prIdList, mapProducer, null, null, null);
            database.executebatch(rsb);
        }
    }
    
    public static void createNewProducerShares(List<Producer> newRecords) {

        //Create new ProducerShares
        List<ProducerShare> producerShareList = new List<ProducerShare>();
        List<ProducerShare> newProducerShareListToInsert = new List<ProducerShare>();

        List<Producer> producers = [SELECT Id,Name FROM Producer WHERE Type = 'Agency'];
        Map<String,Id> agencyProducerMap =  new Map<String,Id>();
        for(Producer prod : producers) 
        {
            agencyProducerMap.put(prod.Name,prod.Id);
        }

        Set<Id> userdIds = new Set<Id>();
        Set<Id> producerIds = new Set<Id>();
        
        system.debug('newRecords' + newRecords);
        
        for (Producer pr : newRecords) {
            
            ProducerShare prodShare = new ProducerShare();
            prodShare.parentId = pr.id;
            prodShare.UserOrGroupId = pr.InternalUserId;
            prodShare.AccessLevel = 'Read';
                        
            producerShareList.add(prodShare);
            userdIds.add(pr.InternalUserId);
            producerIds.add(pr.Id); 

            Id agencyProdId = agencyProducerMap.get(pr.Name);
            if (agencyProdId != null)
            {
                prodShare = new ProducerShare();
                prodShare.parentId = agencyProdId;
                prodShare.UserOrGroupId = pr.InternalUserId;
                prodShare.AccessLevel = 'Read';

                producerShareList.add(prodShare);
                producerIds.add(agencyProdId);          
            }
        }
        
        system.debug('newRecords' + newRecords);
        //Remove existing ProducerShares
        List<ProducerShare> existingProducerShares = [SELECT Id, parentId, UserOrGroupId FROM ProducerShare WHERE parentId IN: producerIds AND UserOrGroupId IN: userdIds];
        
        system.debug('producerShareList' + producerShareList);

        Set<String> existingKeys = new Set<String>();
        for (ProducerShare prod : existingProducerShares ) {
            String key = prod.parentId + '-' + prod.UserOrGroupId;
            existingKeys.add(key);
        }

        for (ProducerShare prod : producerShareList) {
            String key = prod.parentId + '-' + prod.UserOrGroupId;
            if (!existingKeys.contains(key) ) {
                //Add it to avoid creating duplicates
                existingKeys.add(key);
                newProducerShareListToInsert.add(prod);
            }
        }
        system.debug('newProducerShareListToInsert' + newProducerShareListToInsert);
        if (newProducerShareListToInsert.size() > 0) {
            insert newProducerShareListToInsert;
        }        
    }
    
    //******************************************* */
    //******************************************* */
    // Utility Methods    
    //******************************************* */
    //******************************************* */



    private static Map<String, Map<String, Integer>> getTotalReferences(Set<Id> userIds) {

        Map<String, Set<String>> mapProducerIncdexedByUser = new Map<String, Set<String>>();
        Map<String, Set<String>> mapAccountsIndexedByProducer = new Map<String, Set<String>>();

        Set<String> setOfProducer = new Set<String>();


        //Get the List of Producers related to the UserIds 

        List<Producer> producerList = [SELECT Id, Name, InternalUserId FROM Producer WHERE InternalUserId IN: userIds];


        for (Producer pr : producerList) {

            String internalUser = pr.InternalUserId;
            Set<String> listOfProducers = mapProducerIncdexedByUser.get(internalUser);
            if (listOfProducers == null) {
                listOfProducers = new Set<String>();
            }

            listOfProducers.add(pr.Name);
            mapProducerIncdexedByUser.put(internalUser, listOfProducers);

            setOfProducer.add(pr.Name);
        }


        System.debug('### mapProducerIncdexedByUser: ' + mapProducerIncdexedByUser);


        //Get the List of Account related to the Producers

        List<ProducerAccess__c> producerAccessList = [SELECT Id, RelatedId__c, Producer_Identifier__c FROM ProducerAccess__c WHERE Producer_Identifier__c IN: setOfProducer];

        for (ProducerAccess__c pa : producerAccessList) {

            String producerName = pa.Producer_Identifier__c;
            Set<String> listOfAccounts = mapAccountsIndexedByProducer.get(producerName);
            if (listOfAccounts == null) {
                listOfAccounts = new Set<String>();
            }

            listOfAccounts.add(pa.RelatedId__c);
            mapAccountsIndexedByProducer.put(producerName, listOfAccounts);

        }

        System.debug('### mapAccountsIndexedByProducer: ' + mapAccountsIndexedByProducer);


        Map<String, Map<String, Integer>> mapTotalAccountByUser = new Map<String, Map<String, Integer>>();

        for (String userId : mapProducerIncdexedByUser.keySet()) {

            Set<String> producers = mapProducerIncdexedByUser.get(userId);

            Map<String, Integer> accountTotals = mapTotalAccountByUser.get(userId);
            if (accountTotals == null) {
                accountTotals = new Map<String, Integer>();
            }

            for (String producerName : producers) {
                Set<String> accounts = mapAccountsIndexedByProducer.get(producerName);
                if (accounts != null) {
                    for (String accountId : accounts) {
                        Integer total = accountTotals.get(accountId);
                        if (total == null) {
                            total = 0;
                        }

                        total++;

                        accountTotals.put(accountId, total);
                    }
                }

            }

            mapTotalAccountByUser.put(userId, accountTotals);

        }


        System.debug('### mapTotalAccountByUser: ' + mapTotalAccountByUser);


        return mapTotalAccountByUser;


    }
    //////////////////////
    //Leads
    //////////////////////
    public static void createNewLeadShares(List<Producer> newProducers) 
    {
        List<LeadShare> leadShares = new List<LeadShare>();

        //collect the producer ids from the incoming producers
        Set<String> prodIds = new Set<String>();
        for (Producer p : newProducers) 
        {
            prodIds.add(p.Name);
        }

        //get agencies for incoming producer ids
        Id agencyRT = AmFam_Utility.getAccountAgencyRecordType();
        List<Account> agencies = [SELECT Id,Producer_ID__c FROM Account 
                                    WHERE RecordTypeId = :agencyRT AND Producer_ID__c IN :prodIds];

        //create a map of producer id -> agency account
        Map<String,Id> prodIdAgencyMap = new Map<String,Id>();
        for (Account acct : agencies)
        {
            prodIdAgencyMap.put(acct.Producer_ID__c,acct.Id);
        }

        //collect agencies 
        List<Id> agencyIds = new List<Id>();
        for (Account acct : agencies)
        {
            agencyIds.add(acct.Id);
        }             

        //get leads for the collected agencies
        List<Lead> leads = [SELECT Id,Agency__c FROM Lead WHERE Agency__c in :agencyIds AND isConverted = false];

        //create a map of leads for each agency
        Map<Id,List<Lead>> agencyLeadMap = new Map<Id,List<Lead>>();
        for (Lead l : leads) 
        {
            if (!agencyLeadMap.containsKey(l.Agency__c))
            {
                agencyLeadMap.put(l.Agency__c,new List<Lead>());
            }
            agencyLeadMap.get(l.Agency__c).add(l);
        }

        //now cycle the incoming producers
        for (Producer prod : newProducers) 
        {
            //get the agency for the current producer producer Id
            Id agencyId = prodIdAgencyMap.get(prod.Name);

            //get leads for the agency
            List<Lead> agencyLeads = agencyLeadMap.get(agencyId);
            if (agencyLeads != null)
            {
                //process the leads creating shares 
                for (Lead l : agencyLeads)
                {
                    LeadShare ls = new LeadShare();
                    ls.UserOrGroupId = prod.InternalUserId;
                    ls.LeadId = l.Id;
                    ls.RowCause = 'Manual';
                    ls.LeadAccessLevel = 'Edit';
                    leadShares.add(ls);
                }
            }
        }
        
        //System.debug('LEADSSHARES:'+leadShares);        

        if (!leadShares.isEmpty())
        {
            insert leadShares;
        }
    }

    public static void removeLeadShares(List<Producer> deletedProducers) 
    {
        List<LeadShare> deleteShares = new List<LeadShare>();

        //collect the producer ids from the incoming producers
        Set<String> prodIds = new Set<String>();
        Map<Id,List<String>> userProdIdMap = new Map<Id,List<String>>();

        for (Producer p : deletedProducers) 
        {
            prodIds.add(p.Name);
            if (!userProdIdMap.containsKey(p.InternalUserId))
            {
                userProdIdMap.put(p.InternalUserId,new List<String>());
            }
            userProdIdMap.get(p.InternalUserId).add(p.Name);
        }

        //get agencies for incoming producer ids
        Id agencyRT = AmFam_Utility.getAccountAgencyRecordType();
        List<Account> agencies = [SELECT Id,Producer_ID__c FROM Account 
                                    WHERE RecordTypeId = :agencyRT AND Producer_ID__c IN :prodIds];
                
        //collect agencies 
        List<Id> agencyIds = new List<Id>();
        for (Account acct : agencies)
        {
            agencyIds.add(acct.Id);
        }             

        //unfortunately this is a very coarse filter and we can get LeadShares for user/prodid combination
        //not in the incoming producers
        List<LeadShare> leadShares = [SELECT Id,UserOrGroupId,LeadId,Lead.Agency__c,Lead.Agency__r.Producer_ID__c FROM LeadShare 
                                        WHERE UserOrGroupId in :userProdIdMap.keySet() 
                                        AND Lead.Agency__c in : agencyIds
                                        AND RowCause = 'Manual'];
        
        for (LeadShare ls : leadShares) 
        {
            //check if User/ProdId combo matches so that we don't delete erroneous records
            if (userProdIdMap.get(ls.UserOrGroupId).contains(ls.Lead.Agency__r.Producer_ID__c))
            {
                deleteShares.add(ls);
            }
        }

        if (!deleteShares.isEmpty())
        {
            delete deleteShares;
        }
    }

    public static void cleaupPolicyShares(List<Producer> oldRecords, Map<Id,Producer> oldRecordsMap)
    {
        AmFam_InsurancePolicyService.removeInsurancePolicyShares(oldRecords);
    }

    public static void createPolicySharesForNewProducer(List<Producer> newRecords)
    {
        List<Id> producerIds = new List<Id>();
        for (Producer prod : newRecords) 
        {
            producerIds.add(prod.Id);
        }
        database.executebatch(new AmFam_NewProducerInsPolicyShareBatch(producerIds));
    }
}