//Yes, it looks a lot like AmFam_ProducerTypeSvcHttpCalloutMock but serves a different response for booksofbusiness
//Did this because I could not get the test to work changing the modifier in AmFam_ProducerTypeSvcHttpCalloutMock, both
//future calls used the same response which did not help with the update test.
@IsTest
global class AmFam_ProducerTypeSvcHttpCalloutMockB implements HttpCalloutMock {
    Boolean useAgentJSON;

    public AmFam_ProducerTypeSvcHttpCalloutMockB(Boolean agentJSON) {
        useAgentJSON = agentJSON;
    }

    global HTTPResponse respond(HTTPRequest req) {
        System.debug('AmFam_ProducerTypeServiceHttpCalloutMockB');
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if (req.getEndpoint().contains('alternatekeys')) {
            res.setBody(AmFam_ProducerTypeServiceTest.alternateKeysJSON);
        } else if (req.getEndpoint().contains('booksofbusiness')) {
            if (useAgentJSON) {
                res.setBody(AmFam_ProducerTypeServiceTest.agentBookOfBusiness2JSON);
            } else {
                res.setBody(AmFam_ProducerTypeServiceTest.csrBookOfBusinessJSON);
            }
        } else if (req.getEndpoint().contains('producers')) {
            if (useAgentJSON) {
                res.setBody(AmFam_ProducerTypeServiceTest.agentProducerJSON);
            } else {
                res.setBody(AmFam_ProducerTypeServiceTest.csrProducerJSON);
            }
        } else if (req.getEndpoint().contains('agencies')) {
            res.setBody(AmFam_ProducerTypeServiceTest.agencyJSON);
        }
        res.setStatusCode(200);
        return res;
    }
}