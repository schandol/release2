public class AmFam_CleanUpRecordBatchHelperForHH {
    
    /*
    Makes a callout to wsHouseholdService for @CDHID.
    @args CDHID to retrieve
    */
    public static wsHouseholdService.RetrieveResponseType retrieveHouseHold(String cdhId) {
    
        wsHouseholdServiceCallout.HouseholdServiceSOAP houseHoldSearchService = new wsHouseholdServiceCallout.HouseholdServiceSOAP();
        wsHouseholdService.RetrieveHouseholdRequestType request = new wsHouseholdService.RetrieveHouseholdRequestType();
        request.householdIdentifier = cdhId;
        request.consumer = 'SALESFORCE';
        
        wsHouseholdService.RetrieveResponseType response = houseHoldSearchService.retrieveHousehold_Http(request);
        
        return response;
    }
    
    //process HouseHold Account Details
    public static Map<Account, List<string>> processHouseHoldAccountDetails(Account rec, wsHouseholdService.RetrieveResponseType houseHoldSearchResponse){
        
    	Account acc = rec!=null? rec : new Account();
        Map<Account, List<string>> returnMap = new Map<Account, List<string>>();
        
        wsHouseholdServiceParty.HouseholdType household = houseHoldSearchResponse.Household;
        
        if(household != null){
            acc.Party_Version__c = household?.householdVersion;
            acc.CDHID__c = household?.householdIdentifier;
            acc.Name = household?.householdName;
            acc.Greeting__c = household?.householdGreetingName;
            acc.RecordTypeId = AmFam_Utility.getAccountHouseholdRecordType();
        }
             
        List<string> relatedHouseHoldMembers = parseRelationShips(household);
        returnMap.put(acc, relatedHouseHoldMembers);
    	    
        return returnMap;
    }
    
    public static List<String> parseRelationShips(wsHouseholdServiceParty.HouseholdType household) {
        
       
        List<String> memberIdentifierList = new List<String>();
        List<wsHouseholdServiceParty.HouseholdMemberType> membersList = household.HouseholdMembersList;
        for(wsHouseholdServiceParty.HouseholdMemberType mm : membersList){
            string memberIdentifier = mm.partyIdentifier;
            memberIdentifierList.add(memberIdentifier);
        }
        return memberIdentifierList;
    }
    
    //identify primary member of HouseHold Members
    public static Map<String, Boolean> parseRelationShipsWithPrimary(wsHouseholdServiceParty.HouseholdType household) {
        
        Map<String, Boolean> memberIdentifierAndPrimaryMap = new Map<String, Boolean>();
        List<wsHouseholdServiceParty.HouseholdMemberType> membersList = household.HouseholdMembersList;
        for(wsHouseholdServiceParty.HouseholdMemberType mm : membersList){
            string memberIdentifier = mm.partyIdentifier;
            Boolean primary = mm.primaryMember;
            memberIdentifierAndPrimaryMap.put(memberIdentifier, primary);
        }
        return memberIdentifierAndPrimaryMap;
    }
    
    //process Account-Contact Relationships 
    public static Map<String, List<AccountContactRelation>> createRelationshipRecords(wsHouseholdService.RetrieveResponseType resp, Account rec, Map<Id, List<AccountContactRelation>> hhIDAccountContactRelMap, Map<string, Account> cdhIdAccountMap, Map<Id, string> personContactIdAccCDHIDMap){
        
     Map<String, List<AccountContactRelation>> returnMap = new Map<String, List<AccountContactRelation>>();
         
     wsHouseholdServiceParty.HouseholdType household = resp.Household;
     
     List<string> relatedHouseHoldMembers = parseRelationShips(household);
     Map<String, Boolean> memberIdentifierAndPrimaryMap = parseRelationShipsWithPrimary(household);
     List<string> existingRelatedHouseHoldMembers = new List<string>();
         
     List<AccountContactRelation> existingAccountContactRelList = hhIDAccountContactRelMap != null ? hhIDAccountContactRelMap?.get(rec.id): new List<AccountContactRelation>(); 
     List<AccountContactRelation> accountContactRelDelList = new List<AccountContactRelation>();
     List<AccountContactRelation> accountContactRelInsertList = new List<AccountContactRelation>();
        
        if(existingAccountContactRelList != null){
            for(AccountContactRelation rel : existingAccountContactRelList){
                string cdhIdOfParty = personContactIdAccCDHIDMap.get(rel?.ContactId);
                if(!relatedHouseHoldMembers?.contains(cdhIdOfParty)){
                    accountContactRelDelList.add(rel);   
                }
                existingRelatedHouseHoldMembers.add(cdhIdOfParty);
            }
        }
        
        
        for(string memId : relatedHouseHoldMembers){
            Account relatedParty = cdhIdAccountMap?.get(memId);
            if(relatedParty != null && existingRelatedHouseHoldMembers != null? !existingRelatedHouseHoldMembers?.contains(memId): true){
                AccountContactRelation acr = new AccountContactRelation();
                acr.AccountId = rec.id;
                acr.ContactId = relatedParty?.PersonContactId;
                acr.FinServ__Primary__c =  memberIdentifierAndPrimaryMap?.get(memId); 
                accountContactRelInsertList.add(acr);    
            }    
        }
        
        if(accountContactRelInsertList.size()>0) returnMap.put('insert', accountContactRelInsertList);
        if(accountContactRelDelList.size()>0) returnMap.put('delete', accountContactRelDelList);
        return returnMap;
       
    }

}