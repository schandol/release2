@isTest
private class AmFam_AccountContactRelationshipTest {
	@testSetup static void setup() {

		Account agencyAccount = insertAgencyAccount('Agency Account');
		Account agentAccount = insertAgentAccount('Agent Account');
		Account personAccount = insertPersonAccount('Test', 'Person Account', 'Group');
		Account householdAccount = insertHouseholdAccount('Test', 'Person Account', 'Group');
        

	} 


    static testMethod void testRemoveAccountContactRelation() {


        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();     
        Id householdRTId = AmFam_Utility.getAccountHouseholdRecordType();
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Id agentRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        

        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account householdAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;

            Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agencyRTID LIMIT 1] ;
	    	Account agentAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;
	    	Contact agentContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: agentAccount.Id LIMIT 1] ;
	    	Contact personContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: personAccount.Id LIMIT 1] ;



			AccountContactRelation acr1 = insertAccountContactRelation(agencyAccount.Id, agentContact.Id);
			//AccountContactRelation acr2 = insertAccountContactRelation(agentAccount.Id, personAccount.PersonContactId);
			AccountContactRelation acr3 = insertAccountContactRelation(householdAccount.Id, personAccount.PersonContactId);

	    	Test.startTest();


            //NOTE:
            //The household has an AccountContactRelation created out of the box for its own contact before the manual ACR creation, so the Asserts should count +1 to not be fooled by this out of the box ACR with it own contact

            List<AccountContactRelation> assertBefore = [SELECT Id FROM AccountContactRelation WHERE AccountId =: householdAccount.Id];
            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') ) {
                System.assertEquals(2, assertBefore.size());
            }
	
            delete acr3;

            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') ) {
                List<AccountContactRelation> assertAfter = [SELECT Id FROM AccountContactRelation WHERE AccountId =: householdAccount.Id];
                System.assertEquals(1, assertAfter.size());
            }
	    	Test.stopTest();

		}
	}




    static testMethod void testCreateAccountContactRelation() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agencyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Id agentRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account householdAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;

	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agencyRTID LIMIT 1] ;
	    	Account agentAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agentRTID LIMIT 1] ;
	    	Contact agentContact = [SELECT Id, Name, AccountId FROM Contact WHERE AccountId =: agentAccount.Id LIMIT 1] ;

	    	Test.startTest();

			AccountContactRelation acr1 = insertAccountContactRelation(agencyAccount.Id, agentContact.Id);
			AccountContactRelation acr2 = insertAccountContactRelation(householdAccount.Id, personAccount.PersonContactId);

            List<AccountContactRelation> assertAfter = [SELECT Id FROM AccountContactRelation WHERE AccountId =: agentAccount.Id];
            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object') ) {
                System.assertEquals(2, assertAfter.size());
            }


	    	Test.stopTest();

		}
	}

    static testMethod void testUpdateAccountContactRelation() {


        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();        
        Id agenctyRTID =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        


        //******************
        //Setup initial data
        User objUser = [Select Id from User where Id = :UserInfo.getUserId()];


		System.runAs(objUser) {		

	    	Account personAccount = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE RecordTypeId =: personAccountRTId LIMIT 1] ;
	    	Account agencyAccount = [SELECT Id, Name FROM Account WHERE RecordTypeId =: agenctyRTID LIMIT 1] ;

			AccountContactRelation acr = insertAccountContactRelation(agencyAccount.Id, personAccount.PersonContactId);


	    	Test.startTest();

            List<AccountParticipant> assertBefore = [SELECT Id FROM AccountParticipant WHERE AccountId =: personAccount.Id];


	    	update acr;


            if (AmFam_Utility.isTriggerSwitchActive('AccountContactRelation_Object')) {
                List<AccountParticipant> assertAfter = [SELECT Id FROM AccountParticipant WHERE AccountId =: personAccount.Id];
                System.assertEquals(assertBefore.size(), assertAfter.size(), 'There was a change that was not supposed to be');
            }

	    	Test.stopTest();

		}
	}	


    private static Account insertHouseholdAccount(String firstName, String lastName, String individualType){

        Id devRecordTypeId = AmFam_Utility.getAccountHouseholdRecordType();
        Account objAcc = new Account();
        objAcc.Name = firstName + ' ' + lastName + 'HH';
        objAcc.FinServ__IndividualType__c = individualType;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;

    }
       
       
 

    private static Account insertPersonAccount(String firstName, String lastName, String individualType){
        Id devRecordTypeId = AmFam_Utility.getPersonAccountRecordType();       
        Account objAcc = new Account();
        objAcc.FirstName = firstName;
        objAcc.LastName = lastName;
        objAcc.FinServ__IndividualType__c = individualType;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        objAcc = [SELECT Id, PersonContactId, FirstName, LastName, RecordTypeId FROM Account WHERE Id =: objAcc.id];
        return objAcc;
    }
    
        
    private static Account insertAgencyAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Agency').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;
        return objAcc;
    }

    
    private static Account insertAgentAccount(String accName){
        Id devRecordTypeId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();        
        Account objAcc = new Account();
        objAcc.Name = accName;
        objAcc.RecordTypeId = devRecordTypeId;
        insert objAcc;

        Contact objContact = new Contact();
        objContact.FirstName = accName;
        objContact.LastName = accName;
        objContact.AccountId = objAcc.Id;
        insert objContact;

        return objAcc;
    }


    private static AccountContactRelation insertAccountContactRelation(String accountId, String contactId) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = contactId;
        insert acr;
        return acr;
    }


}