/**
*   @description: the apex class is the service class for AmFam_AccountAccountRelationshipHandler class
*	@author: Anbu Chinnathambi
*   @date: 09/12/2021
*   @group: Apex Class
*/

public class AmFam_AccountAccountRelationshipService {
    
    
    /**
     * @description: finds the duplicate Account-Account relationship records and remove it. It's particularly gonna help SF clean when CDH creates duplicate records on composite request
     * @date: 09/12/2021
     * @author: Anbu Chinnathambi
     **/
    
    public static void deleteDuplicateAccountAccountRel(List<FinServ__AccountAccountRelation__c> newRecords, Map<ID, FinServ__AccountAccountRelation__c> newRecordsMap) {
        //To avoid the recursion
        if(!AmFam_checkRecursive.firstCallAmFam_AccountAccountRelationshipService){
            
            AmFam_checkRecursive.firstCallAmFam_AccountAccountRelationshipService = true;
            
            Map<Id, Id> accountRelatedAccountMap = new Map<Id, Id>();
            Set<Id> listOfRecordsForDelete = new set<id>();
            
            for(FinServ__AccountAccountRelation__c aa : newRecords){
               accountRelatedAccountMap.put(aa.FinServ__Account__c, aa.FinServ__RelatedAccount__c);
            }
            Set<String> ccUnqiueKey =new Set<String>();
            //condition to check duplicates against the database
            for(FinServ__AccountAccountRelation__c aa : [select id,FinServ__RelatedAccount__c, FinServ__Account__c, FinServ__Role__c, FinServ__Role__r.Name From FinServ__AccountAccountRelation__c where FinServ__Account__c IN:accountRelatedAccountMap.keyset() OR FinServ__RelatedAccount__c IN:accountRelatedAccountMap.values() OR FinServ__Account__c IN:accountRelatedAccountMap.values() OR FinServ__RelatedAccount__c IN:accountRelatedAccountMap.keyset() Order by CreatedDate ASC ]){ 
                String uniqueKey =  aa.FinServ__Account__c+'-'+aa.FinServ__RelatedAccount__c+'-'+aa.FinServ__Role__c;
                if(ccUnqiueKey.contains(uniqueKey)){
                    system.debug('uniqueKey' + uniqueKey);
                    listOfRecordsForDelete.add(aa.id);
                }else{
                    ccUnqiueKey.add(aa.FinServ__Account__c+'-'+aa.FinServ__RelatedAccount__c+'-'+aa.FinServ__Role__c);
                    ccUnqiueKey.add(aa.FinServ__RelatedAccount__c+'-'+aa.FinServ__Account__c+'-'+aa.FinServ__Role__c);   
                }
            }
            system.debug('listOfRecordsForDelete' + listOfRecordsForDelete);
            if(listOfRecordsForDelete.size()>0){
            	deleteAccountAccountRelRecords(listOfRecordsForDelete);
            }
         }
    }
    
    //delete the duplicate Account-Account relationship
    @future
    public static void deleteAccountAccountRelRecords(set<Id> aarIdList) {
        
        List<FinServ__AccountAccountRelation__c> deleteList = [select id,FinServ__RelatedAccount__c, FinServ__Account__c From FinServ__AccountAccountRelation__c where Id IN: aarIdList];
        system.debug('deleteList' + deleteList);
        try{delete deleteList;}catch(Exception e){}
    }
    
    
}