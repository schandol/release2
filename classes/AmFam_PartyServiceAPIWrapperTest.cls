@IsTest
public class AmFam_PartyServiceAPIWrapperTest {
	public Static String json = '{'+
		'	\"status\": {'+
		'		\"maxMessageLevel\": \"INFO\",'+
		'		\"code\": 200,'+
		'		\"reason\": \"Ok\",'+
		'		\"transactionId\": \"246969\"'+
		'	},'+
		'	\"products\": ['+
		'		{'+
		'			\"typeOfProductCode\": \"QUOTE\",'+
		'			\"sourceSystemProductIdentifier\": \"1983729\",'+
		'			\"productCode\": \"FamilyAuto\",'+
		'			\"productDescription\": \"Vehicle\",'+
		'			\"policyTypeCode\": \"FPPA\",'+
		'			\"sourceSystemName\": \"AutoPlus\",'+
		'			\"contractState\": \"AZ\",'+
		'			\"servicingAgentProducerId\": \"23121\",'+
		'			\"policyInForceOn\": \"2012-01-31\",'+
		'			\"risk\": ['+
		''+
		'					{'+
		''+
		'					\"riskId\" : \"484381\",'+
		''+
		'					\"riskTypeCode2\" : \"auto\",'+
		''+
		'					\"riskTypeDesc\" : \"Car/Light Truck\",'+
		''+
		'					\"riskDescription\" : \":2006,Ford Truck,F350 Crew C Pu 4x4\"'+
		''+
		'					}'+
		'			]'+
		'		},'+
		'		{'+
		'			\"typeOfProductCode\": \"POLICY\",'+
		'			\"sourceSystemProductIdentifier\": \"410000025533\",'+
		'			\"productCode\": \"Homeowners\",'+
		'			\"productDescription\": \"Home\",'+
		'			\"policyTypeCode\": \"HO4\",'+
		'			\"sourceSystemName\": \"PolicyCenter\",'+
		'			\"contractState\": \"UT\",'+
		'			\"servicingAgentProducerId\": \"22600\",'+
		'			\"policyInForceIndicator\": false,'+
		'			\"policyInForceOn\": \"2014-08-20\",'+
		'			\"policyExpiresOn\": \"2015-08-20\",'+
		'			\"risk\": ['+
		''+
		'					{'+
		''+
		'					\"riskId\" : \"484381\",'+
		''+
		'					\"riskTypeCode2\" : \"auto\",'+
		''+
		'					\"riskTypeDesc\" : \"Car/Light Truck\",'+
		''+
		'					\"riskDescription\" : \":2006,Ford Truck,F350 Crew C Pu 4x4\"'+
		''+
		'					}'+
		'			]'+
		'		}'+
		'	]'+
		'}';
	static testMethod void testParse() {
		AmFam_PartyServiceAPIWrapper obj = AmFam_PartyServiceAPIWrapper.parse(json);
		System.assert(obj != null);
	}
}