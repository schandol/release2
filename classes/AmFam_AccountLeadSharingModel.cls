public class AmFam_AccountLeadSharingModel {
    // Create/find contact federation id
        // if contact is found, update middle name, producer id, title and phone
        // if contact is not found, create contact with Name, email, fed id,  middle name, producer id, title and phone

    // Producer Wrapper is for encapsulating attributes from the calling class
    public class ProducerWrapper {
        public String producerFirstName {get;set;}
        public String producerLastName {get;set;}
        public String producerProfile {get;set;}
        public String producerId {get;set;}
        public String amfamUserId {get;set;}
        public Id salesforceId {get;set;}
        public List<LeadShare> leadSharingRecs {get;set;}
        public Set<String> bookOfBusinessProducerIds {get;set;}

        public ProducerWrapper(){
            leadSharingRecs = new List<LeadShare>();
            bookOfBusinessProducerIds = new Set<String>();
        }

        public override String toString(){
            String classInfo = '';

            classInfo = ' ProducerWrapper Info: amfamUserid: ' + this.amfamUserId + ' Producer Id: ' + this.producerId + ' Salesforce Id: ' + this.salesforceId + 
                        ' Profile:' + this.producerProfile + ' Producer Name: ' + this.producerFirstName + ' ' + this.producerLastName + ' LeadShare Size: ' + this.leadSharingRecs.size() +
                        ' BOB Size: ' + this.bookOfBusinessProducerIds.size() ;

            return classInfo;

        }
    }    

    public static ProducerWrapper producerServiceResponse (AmFam_ProducerTypeService prodSvc, String userId) {
        User userRec = [SELECT Id, Email, FirstName, FederationIdentifier, AFI_External_User_ID__c, AmFam_User_ID__c, LastName, MiddleName, Phone, Producer_ID__c, Profile.Name, Title, UserRole.Name
                        FROM User WHERE AmFam_User_ID__c =: userId AND IsActive =: true LIMIT 1];
        Set<String> producerIDs = new Set<String>();
        ProducerWrapper pw = new ProducerWrapper();
        if (prodSvc.Status.code == 200) {
            //Get all producer ids from Book of Business
            if (prodSvc.producer != null) {
                // Check if we need to create/update related Account details
                //AmFam_ProducerTypeService pTypeSvc = AmFam_ProducerTypeServiceHandler.upsertRelatedAccount(prodSvc.producer.producerId);
                AmFam_ProducerTypeService pTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + prodSvc.producer.producerId + '/booksofbusiness');
                if (pTypeSvc?.bookOfBusiness != null) {
                    for (AmFam_ProducerTypeService.BookOfBusiness bob : pTypeSvc?.bookOfBusiness) {
                        if (bob?.producerCodes != null) {
                            for (AmFam_ProducerTypeService.ProducerCodes pc : bob?.producerCodes){
                                producerIDs.add(pc.producerCode);
                                pw.bookOfBusinessProducerIds.add(pc.producerCode);
                            }
                        }
                    }
                }
                userRec.Title = pTypeSvc.bookOfBusiness[0].title;

                // Set the rest of Producer Wrapper Attributes
                pw.producerFirstName = userRec.FirstName;
                pw.producerLastName = userRec.LastName;
                pw.producerProfile = userRec.Profile.name;
                pw.producerId = userRec.Producer_ID__c;
                pw.amfamUserId = userRec.AmFam_User_ID__c;
                pw.salesforceId = userRec.Id;

                List<LeadShare> ls = AmFam_AccountLeadSharingModel.createUpdateContactWithUserDetails(producerIDs, userRec);
                if (ls != null){
                    pw.leadSharingRecs.addAll(ls);
                }
                return pw;
            } else {
                return null;
            }
        }
        return null;
    }

    public static List<LeadShare> createUpdateContactWithUserDetails (Set<String> producerIDs, User userRec) {

        List<Contact> contacts = [SELECT Id,AccountId FROM Contact WHERE User_Producer_ID__c = :userRec.Producer_ID__c LIMIT 1];
        if (contacts.size() == 0)
        {
            //contact not created yet, bail
            return null;
        }
        Contact con = contacts[0];

        // for AmFam Internal Support User & S&SO. Dont do anything with Accounts and Related Lead. Access will be given using Roles/Public Groups.
        if (userRec.Profile.Name == 'Internal Support' || userRec.Profile.Name == 'Sales & Service Operations Rep') {
            //Give read access to - their own individual account
            AccountShare accShr = new AccountShare();
            accShr.AccountAccessLevel = 'Read';
            accShr.AccountId = con.AccountId;
            accShr.UserOrGroupId = userRec.Id;
            accShr.OpportunityAccessLevel = accShr.AccountAccessLevel;
            insert accShr;
            //return con.Id;
        }

        //for book of business agencies, give access to Accounts & Leads for Agent & CSR
        if (producerIDs.size() > 0 && (userRec.Profile.Name == 'Agent CSR' || userRec.Profile.Name == 'AmFam Agent')) {
            accountLeadSharing(producerIDs, con.Id, userRec, con.AccountId);
            return null;
        } else {
            //Log an exception that Book Of Business is empty
            //store exception details on Failure Logs with user details and error details
            String errorMessage = 'No Producer ID returned by Producer API response';
            String API_URL = '/producers/' + userRec.Producer_ID__c + '/booksofbusiness';
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLogNoProducerReturned('AmFam_AccountLeadSharingModel', API_URL, userRec.AFI_External_User_ID__c, errorMessage);
        }
        return null;
    }

    public static void accountLeadSharing (Set<String> producerIDs, ID contactId, User userRec, ID accountId) {
        ID userID = userRec.Id;
        Set<Id> accountIDs = new Set<Id>(new Map<Id, Account>([SELECT Id FROM Account WHERE Producer_ID__c IN :producerIDs]).keySet());
        if (accountIDs.size() > 0 ) {
            // Asscoiate the contact with all accounts
            List<AccountContactRelation> accountsRelatedToContact = [SELECT Id, ContactId, AccountId
                                                                        FROM AccountContactRelation
                                                                        WHERE ContactId = :contactId AND IsDirect = false];
            //Check if account contact relation need to be removed
            List<AccountContactRelation> accountsRelatedToContactDelete = new List<AccountContactRelation>();
            Set<Id> accountsIdRelatedToContactNotChanged = new Set<Id>();
            for (AccountContactRelation acr : accountsRelatedToContact) {
                if (accountIDs.contains(acr.AccountId)) {
                    accountsIdRelatedToContactNotChanged.add(acr.AccountId);
                } else if (!accountIDs.contains(acr.AccountId)) {
                    accountsRelatedToContactDelete.add(acr);
                }
            }
            //Check if account contact relation need to be added
            List<AccountContactRelation> accountsRelatedToContactAdd = new List<AccountContactRelation>();
            for (Id accId : accountIDs) {
                if (!accountsIdRelatedToContactNotChanged.contains(accId)) {
                    AccountContactRelation accCon = new AccountContactRelation();
                    accCon.AccountId = accId;
                    accCon.ContactId = contactId;
                    accCon.IsActive = true;
                    accCon.Roles = userRec.UserRole.Name;
                    accountsRelatedToContactAdd.add(accCon);
                }
            }

            if (accountsRelatedToContactAdd.size() > 0) {
                database.insert(accountsRelatedToContactAdd, false);
            }
            if (accountsRelatedToContactDelete.size()>0) {
                database.delete(accountsRelatedToContactDelete, false);
            }

            List<AccountShare> accountSharingRecs = [SELECT Id, AccountId, Account.OwnerId FROM AccountShare WHERE UserOrGroupId = :userID];

            //Remove apex sharing records from accounts if required (when user does not belong to existing agency)
            List<AccountShare> accountSharingDelete = new List<AccountShare>();
            Set<Id> accountsIdSharingNotChanged = new Set<Id>();
            for (AccountShare accShare : accountSharingRecs) {
                if (accountIDs.contains(accShare.AccountId)) {
                    accountsIdSharingNotChanged.add(accShare.AccountId);
                } else if (!accountIDs.contains(accShare.AccountId)) {
                    accountSharingDelete.add(accShare);
                }
            }
            //Add apex sharing records to accounts
            List<AccountShare> accountSharingAdd = new List<AccountShare>();
            for (Id accId : accountIDs) {
                if (!accountsIdSharingNotChanged.contains(accId)) {
                    AccountShare accShr = new AccountShare();
                    accShr.AccountAccessLevel = 'Read';
                    accShr.AccountId = accId;
                    accShr.UserOrGroupId = userID;
                    accShr.OpportunityAccessLevel = accShr.AccountAccessLevel;
                    accountSharingAdd.add(accShr);
                }
            }
            //Give read access to agents - their own individual account
            AccountShare accShr = new AccountShare();
            accShr.AccountAccessLevel = 'Read';
            accShr.AccountId = accountId;
            accShr.UserOrGroupId = userID;
            accShr.OpportunityAccessLevel = accShr.AccountAccessLevel;
            accountSharingAdd.add(accShr);
            if (accountSharingDelete.size() > 0) {
                Database.delete(accountSharingDelete, false) ;
            }
            if (accountSharingAdd.size() > 0) {
                Database.insert(accountSharingAdd, false);
            }
        } else {
            // IF no account is loaded in SF then log API Failure Lofg
            String errorMessage = 'No Accounts found in SF related to producer API response';
            String API_URL = '/v1/producers/' + userRec.AFI_External_User_ID__c + '/?idIsExternalId=true...';
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLogNoProducerReturned('AmFam_AccountLeadSharingModel', API_URL, userRec.AFI_External_User_ID__c, errorMessage);
        }
    }
}