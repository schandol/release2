@isTest
public class AmFam_LPECalloutTest {
    @testSetup static void methodName() {
        Account a = new Account(Name = 'Test Agency', Producer_ID__c = '12345');
        insert a;
    }
    
    static testMethod void testCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new AmFam_LPECalloutMock(true));
        
        Account a = [SELECT Id FROM Account WHERE Name = 'Test Agency' LIMIT 1];
        
        List<Lead> leadList = new List<Lead>();
        for (Integer i = 0; i < 200; i++) {
            Lead l = new Lead();
            l.Agency__c = a.Id;
            l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
            l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5, 'nln');
            l.Phone = '1231231234';
            l.Street = i+' Main Street';
            l.City = 'Anytown';
            l.StateCode = 'WI';
            l.CountryCode = 'US';
            l.PostalCode = '12345';
            l.Email = 'testing'+i+'@test.com';
            l.Email_Usage__c = 'HOME';
            l.Marital_Status__c = 'M';
            l.Gender__c = 'M';
            l.Date_Of_Birth__c = Date.today();
            l.LeadSource = 'Amfam.com';
            l.Company = 'Test';
            l.Lines_of_Business__c = 'Personal Vehicle;Property';
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('AmFam Agency').getRecordTypeId();
            leadList.add(l);
        }
        
        Test.startTest();
        insert leadList;
        Test.stopTest();
        
        List<Id> leadIds = new List<Id>();
        for (Lead l : leadList) {
            leadIds.add(l.id);
        }
        
        List<Lead> leadsAfterBatch = [SELECT Id, LPE_Status__c, Lead_External_ID__c FROM Lead WHERE Id IN :leadIds];
        for (Lead l : leadsAfterBatch) {
            system.debug(l);
            system.assertEquals('Processing', l.LPE_Status__c);
        }
    }
}