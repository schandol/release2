public with sharing class AmFam_ProducerUpdateQueueable implements Queueable,Database.AllowsCallouts{
    @TestVisible
    private static Boolean chainNextQueueable = true;

    private List<Id> userRecIds;

    public AmFam_ProducerUpdateQueueable(List<Id> userRecIds)
    {
        this.userRecIds = userRecIds;
    }

    public void execute(QueueableContext context) 
    {
        AmFam_UserService.producerUpdate(userRecIds);

        if (chainNextQueueable && !Test.isRunningTest())
        {
            System.enqueueJob(new AmFam_PermGroupsToNewUsersQueueable(userRecIds));
        }
    }

}