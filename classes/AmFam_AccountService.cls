public without sharing class AmFam_AccountService {

    public static void changeAccountOwner(List<Account> newRecords) {
        Set<Id> parentAccountIds = new Set<Id>();
        Set<String> producerIds = new Set<String>();

        for (Account acc : newRecords) {
            if (String.isNotEmpty(acc.Agency__c)) {
                parentAccountIds.add(acc.Agency__c);
            }
        }

        Map<Id, Account> parentAccountsMap = new Map<Id, Account>([SELECT Id, ParentId, Agency__c, Producer_ID__c FROM Account WHERE Id IN: parentAccountIds]); 
        
        for (Account acc : parentAccountsMap.values()) {
            if (String.isNotEmpty(acc.Producer_ID__c)) {
                producerIds.add(acc.Producer_ID__c);
            }
        }

        List<User> userList = [SELECT Id, Name, Producer_ID__c FROM User WHERE IsActive = true AND Producer_ID__c IN: producerIds];

        //Craetes a Map of userIds indexed by ProducerIdentifier
        //NOTE: if more than one user has the same ProducerIdentifier, then the last one returned on the query will be the one retained on the map.
        Map<String, Id> producerIdToUserMap = new Map<String, Id>();
        for (User user : userList) {
            producerIdToUserMap.put(user.Producer_ID__c, user.Id);
        }

        for (Account acc : newRecords) {
            if (String.isNotEmpty(acc.Agency__c)) {
                Account agencyAccount = parentAccountsMap.get(acc.Agency__c);
                if (agencyAccount != null && string.isNotEmpty(agencyAccount.Producer_ID__c)) {
                    
                    Id userId = producerIdToUserMap.get(agencyAccount.Producer_ID__c);

                    if (userId != null) {
                        acc.OwnerId = userId;
                    }
                }
            }
        }
    }

    public static void createsAddressRecord(String accId, List<wsPartyManageParty.PartyAddressType> addresses) {
        //Collection definitions
        Map<Id, Account>                accRecordMap =                  new Map<Id, Account>();
        List<Schema.Address>            addressesToInsert =             new List<Schema.Address>();
        //List<Schema.Location>           locationsToInsert =             new List<Schema.Location>();
        List<AssociatedLocation>        associatedLocationsToInsert =   new List<AssociatedLocation>();

        //Definition of location list associated to single account
        List<Schema.Location> locations = new List<Schema.Location>();

        if (addresses == null) {
            return;
        }

        //Build Address records based on Shipping and Billing information on Account record
        for (wsPartyManageParty.PartyAddressType addrType : addresses) {
            Schema.Address addrInserted = AmFam_Utility.getAddressRecord(addrType.Address);
            for (wsPartyManageParty.PartyAddressPurposeAndUsageType purpose : addrType.PartyAddressPurposeAndUsage) {
                AssociatedLocation associated = new AssociatedLocation();
                associated.LocationId = addrInserted.ParentId;
                associated.ParentRecordId = accId;
                associated.PersonAccount__c = accId;
                associated.Purpose__c = purpose.PartyAddressPurpose;
                associated.Type = purpose.PartyAddressUsage;
                associatedLocationsToInsert.add(associated);
            }
        }
        if (associatedLocationsToInsert.size() > 0) {
            insert associatedLocationsToInsert;
        }

        System.debug('### associated location to insert: ' + associatedLocationsToInsert);
    }


 
    public static void createAndAssociateHouseholdAccount(List<Account> accounts) {
        Id householdRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('IndustriesHousehold').getRecordTypeId();
        Id personAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();

        Map<Id, Account> personAccountToHouseholdAccountMap = new Map<Id,Account>();
        for (Account acc : accounts) {
            // Only create HH accounts for newly created Person Accounts
            if (acc.RecordTypeId == personAccountRecordTypeId && acc.CDHID__c == null) {
                // Set Household Account name as "FirstName LastName"
                String hhName = acc.FirstName + ' ' + acc.LastName;
                Account householdAccount = new Account(Name = hhName, recordTypeId = householdRecordTypeId, Agency__c = acc.Agency__c);
                personAccountToHouseholdAccountMap.put(acc.PersonContactId,householdAccount);
            }
        }
        if (personAccountToHouseholdAccountMap.size() > 0) {
            insert personAccountToHouseholdAccountMap.values();
        }

        List<AccountContactRelation> relationships = new List<AccountContactRelation>();
        for (Id personAccountId : personAccountToHouseholdAccountMap.keySet()) {
            AccountContactRelation acr = new AccountContactRelation();
            // Pre grant access to household account for CSR
            AccountShare householdShareCreated = AmFam_Utility.preGrantAccountAccess(personAccountToHouseholdAccountMap.get(personAccountId).Id,UserInfo.getUserId());
            // Set Household Account ID
            acr.AccountId = personAccountToHouseholdAccountMap.get(personAccountId).Id;
            // Set Person Account Contact Id
            acr.ContactId = personAccountId;
            // Set this ACR as the primary relationship for the household
            acr.FinServ__Primary__c = true;
            relationships.add(acr);
        }
        if (relationships.size() > 0) {
            insert relationships;
        }
    }
    
    public static void extendPCIFYToAccount() {
        //The below code extending PCIFY to Account object
        if (pcify.Manager.isOnline('Account')) { 
            pcify.Processor.maskCreditCards(Trigger.new, pcify.Manager.getMaskFields('Account'),'Account');
        }
    }
    
    /*
     *@param List<Account> accounts to insert
     * Validation of Account object to be called by the trigger beforeInsert and beforeUpdate
     */
    public static void runAccountTriggerValidations(List<Account> accounts) {
        
        List<String> cdhIdList = new List<String>();
        for(Account acc : accounts){
            if (acc.CDHID__c != null) {
                cdhIdList.add(acc.CDHID__c);
            }
        }
        Map<String, Account> CDHIdAccountMap = new Map<String, Account>();
        Map<String, Id> CDHIdRTIdMap = new Map<String, Id>();
        if (cdhIdList.size() > 0) {
            for(Account a : [SELECT Id, CDHID__c, RecordTypeId FROM Account WHERE CDHID__c =: cdhIdList]){
                CDHIdAccountMap.put(a.CDHID__c, a);
                CDHIdRTIdMap.put(a.CDHID__c, a.RecordTypeId);
            }
        }
        
        for (Account acc : accounts){
            if(acc.CDHID__c != null){
                if(trigger.isInsert){
                    if(CDHIdAccountMap.get(acc.CDHID__c) != null &&  CDHIdRTIdMap.get(acc.CDHID__c)!= null && CDHIdRTIdMap.get(acc.CDHID__c) ==acc.RecordTypeId ){
                        acc.addError('Exception: Duplicate CDHID'+' '+acc.CDHID__c+' '+ 'is found');
                    }
                }else if(trigger.isUpdate){
                    if(CDHIdAccountMap.get(acc.CDHID__c) != null &&  CDHIdRTIdMap.get(acc.CDHID__c)!= null && CDHIdRTIdMap.get(acc.CDHID__c) ==acc.RecordTypeId && acc.id !=CDHIdAccountMap.get(acc.CDHID__c).id ){
                        acc.addError('Exception: Duplicate CDHID'+' '+acc.CDHID__c+' '+ 'is found');
                    }
                }
            }
        }
    }

    public static void createProducerAccessForRelatedAgency(List<Account> newRecords, Map<Id,Account> accountsMap) {
        Set<Id> agencyAccountIds = new Set<Id>();
        List<ProducerAccess__c> paRecords = new List<ProducerAccess__c>();
        for (Account acc : newRecords) {
            // If created account is a business or person account or COI account
            if (acc.recordTypeId == AmFam_Utility.getPersonAccountRecordType() || acc.recordTypeId == AmFam_Utility.getAccountBusinessRecordType() || acc.recordTypeId == AmFam_Utility.getCOIAccountRecordType() || acc.recordTypeId == AmFam_Utility.getAccountHouseholdRecordType()) {
                if (acc.Agency__c != null) {
                    agencyAccountIds.add(acc.Agency__c);
                }
            }
        }
        if (agencyAccountIds.size() > 0) {
            List<Account> agencyAccounts = [SELECT Id, Producer_Id__c FROM Account WHERE Id IN :agencyAccountIds];
            List<ProducerAccess__c> existingProducerAccessRecords = [SELECT Producer_Identifier__c, RelatedId__c, Reason__c FROM ProducerAccess__c WHERE RelatedId__c IN : accountsMap.keySet()];
            system.debug(existingProducerAccessRecords);

            Map<Id,String> accountIdToProducerIdMap = new Map<Id,String>();
            for (Account agency : agencyAccounts) {
                accountIdToProducerIdMap.put(agency.Id,agency.Producer_Id__c);
            }

            for (Account acc : newRecords) {
                ProducerAccess__c pa = new ProducerAccess__c();
                pa.Producer_Identifier__c = accountIdToProducerIdMap.get(acc.Agency__c);
                pa.RelatedId__c = acc.Id;
                pa.Reason__c = 'ENTRY';
                paRecords.add(pa);
            }
        }

        if (paRecords.size() > 0) {
            insert paRecords;
        }
    }
}