/**
* @author         salesforce services
* @date           05/18/2020
* @description    handler class for Producer Service. Contains method to call out Producer Service API.
* @group          API Callout
*
*/
public class AmFam_ProducerServiceHandler {
    public static AmFam_ProducerService producerServiceCallOut(String userExtId) {
        API_Header__mdt apiHeader = [SELECT MasterLabel, Header_Value__c FROM API_Header__mdt WHERE MasterLabel = 'AFI-API-Key' LIMIT 1];
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setHeader('AFI-AppName', 'Salesforce');
        request.setHeader('AFI-API-Key', apiHeader.Header_Value__c);
        String extraParam = '?idIsExternalId=true&levelOfDetail=EXTENDED%2C%20STAFF%2C%20BOOK+OF+BUSINESS%2C%20MANAGERS';
        request.setEndpoint('callout:Producer_API/' + userExtId + extraParam);
        HttpResponse response;
        try {
            response = http.send(request);
            // If the request is successful, parse the JSON response.
            if (response.getStatusCode() == 200) {
                // Deserialize the JSON string into collections of primitive data types.
                AmFam_ProducerService  prodSvc = (AmFam_ProducerService)JSON.deserialize(response.getBody(), AmFam_ProducerService.class);
                return prodSvc;
            }
        } catch (Exception e) {
            //store exception details on Failure Logs with user details and error details
            AmFam_APIFailureLogger failureLog = new AmFam_APIFailureLogger();
            failureLog.createAPIFailureLog('AmFam_ProducerServiceHandler', response, request.getEndpoint(), userExtId, e.getMessage());
            System.debug(e.getMessage());
       }
       return null;
    }
}