global class AmFam_NewProducerInsPolicyShareBatch implements database.batchable<sobject>, Database.AllowsCallouts, Database.Stateful
{
    List<Id> producerIds;
        
    public AmFam_NewProducerInsPolicyShareBatch(List<Id> producerIds) 
    {
        this.producerIds = producerIds;
    }

    global List<Producer> start(database.batchableContext bc)
    {
        List<Producer> producers = [SELECT Id,InternalUserId,Name FROM Producer WHERE Id IN :this.producerIds];
        return producers;
    }
        
    global void execute(database.batchablecontext bd, List<Producer> scope) 
    {
        AmFam_InsurancePolicyService.createPolicySharesForNewProducers(scope);
    }

    global void finish(database.batchableContext bc) 
    {
    }
}