Public class AmFam_CDHAccountMergeRequestWrapper{

  public cls_master master;
    public cls_duplicates[] duplicates;
    Public class cls_master {
        public cls_attributes attributes;
        public String Id;   //0016w00000HJmJqAAL
        public String PartyVersion; //3
        public String FirstName;    //John
        public String LastName; //John
    }
    public class cls_attributes {
        public String type; //Account
    }
    public class cls_duplicates {
        public cls_attributes attributes;
        public String Id;   //0016w00000HJmJqAAK
    }
    public static AmFam_CDHAccountMergeRequestWrapper parse(String json){
        return (AmFam_CDHAccountMergeRequestWrapper) System.JSON.deserialize(json, AmFam_CDHAccountMergeRequestWrapper.class);
    }
    
  }