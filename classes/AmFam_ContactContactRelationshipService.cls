/**
*   @description: the apex class is the service class for AmFam_ContactContactRelationshipHandler class
*	@author: Anbu Chinnathambi
*   @date: 08/10/2021
*   @group: Apex Class
*/

public class AmFam_ContactContactRelationshipService {
    
    
    /**
     * @description: finds the duplicate Contact-Contact relationship records and remove it. It's particularly gonna help SF clean when CDH creates duplicate records on composite request
     * @date: 08/10/2021
     * @author: Anbu Chinnathambi
     **/
    
    public static void deleteDuplicateContactContactRel(List<FinServ__ContactContactRelation__c> newRecords, Map<ID, FinServ__ContactContactRelation__c> newRecordsMap) {
        List<String> nonPairRel = new List<String>{'Spouse', 'Sibling', 'Ex-Spouse'};
        //To avoid the recursion
        if(!AmFam_checkRecursive.firstCallAmFam_ContactContactRelationshipService && !System.isBatch() && !System.isFuture()){
            
            AmFam_checkRecursive.firstCallAmFam_ContactContactRelationshipService = true;
            
            Map<Id, Id> contactRelatedContactMap = new Map<Id, Id>();
            Set<Id> listOfRecordsForDelete = new set<id>();
            
            for(FinServ__ContactContactRelation__c cc : newRecords){
               contactRelatedContactMap.put(cc.FinServ__Contact__c, cc.FinServ__RelatedContact__c);
            }
            Set<String> ccUnqiueKey =new Set<String>();
            //condition to check duplicates against the database
            for(FinServ__ContactContactRelation__c cc : [select id,FinServ__RelatedContact__c, FinServ__Contact__c, FinServ__Role__c, FinServ__Role__r.Name From FinServ__ContactContactRelation__c where FinServ__Contact__c IN:contactRelatedContactMap.keyset() OR FinServ__RelatedContact__c IN:contactRelatedContactMap.values() OR FinServ__Contact__c IN:contactRelatedContactMap.values() OR FinServ__RelatedContact__c IN:contactRelatedContactMap.keyset() Order by CreatedDate ASC ]){ 
                String uniqueKey =  cc.FinServ__Contact__c+'-'+cc.FinServ__RelatedContact__c+'-'+cc.FinServ__Role__c;
                if(ccUnqiueKey.contains(uniqueKey)){
                    system.debug('uniqueKey' + uniqueKey);
                    listOfRecordsForDelete.add(cc.id);
                }else{
                    ccUnqiueKey.add(cc.FinServ__Contact__c+'-'+cc.FinServ__RelatedContact__c+'-'+cc.FinServ__Role__c);
                    if(!nonPairRel.contains(cc.FinServ__Role__r.Name)){
                    	ccUnqiueKey.add(cc.FinServ__RelatedContact__c+'-'+cc.FinServ__Contact__c+'-'+cc.FinServ__Role__c);   
                    }
                
                }
            }
            system.debug('listOfRecordsForDelete' + listOfRecordsForDelete);
            if(listOfRecordsForDelete.size()>0){
                    deleteContactContactRelRecords(listOfRecordsForDelete);
             }
         }
    }
    
    //delete the duplicate contact-contact relationship
    @future
    public static void deleteContactContactRelRecords(set<Id> ccrIdList) {
        
        List<FinServ__ContactContactRelation__c> deleteList = [select id,FinServ__RelatedContact__c, FinServ__Contact__c, FinServ__Role__r.FinServ__CreateInverseRole__c From FinServ__ContactContactRelation__c where Id IN: ccrIdList];
        system.debug('deleteList' + deleteList);
        try{delete deleteList;}catch(Exception e){}
    }
    
    
}