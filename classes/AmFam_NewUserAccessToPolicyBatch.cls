global class AmFam_NewUserAccessToPolicyBatch implements database.batchable<sobject>, Database.AllowsCallouts, Database.Stateful{
    List<Id> userIds;
        
    public AmFam_NewUserAccessToPolicyBatch(List<Id> userIds) 
    {
        this.userIds = userIds;
    }

    global List<User> start(database.batchableContext bc)
    {
        List<User> users = [SELECT Id,AmFam_User_ID__c,ContactId,ProfileId,Profile.Name,Producer_ID__c FROM User WHERE Id IN :this.userIds];
        return users;
    }
    
    
    //generally this will be getting called with a single record due to a new user login via Jit SSO.  Only time
    //it should be be called with multiple is on data load and in that case they can controll the batch size to
    //something that works. So leaving things in the loop. Trying to bulkify it does not seem worth the effort
    global void execute(database.batchablecontext bd, List<User> scope) {
        List<InsurancePolicyShare> newShares = new List<InsurancePolicyShare>();
        Map<Id, InsurancePolicyShare> ipsMap = new Map<Id, InsurancePolicyShare>();

        for (User u : scope) {
            //first collect ProducerIds User can access via BoB
            AmFam_ProducerTypeService response = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/' + u.Producer_ID__c + '/booksofbusiness');
            List<String> bobProdIds = new List<String>();
                
            if (response?.bookOfBusiness != null) {
                for (AmFam_ProducerTypeService.BookOfBusiness book : response.bookOfBusiness) {
                    if (book.producerCodes != null) {
                        for (AmFam_ProducerTypeService.ProducerCodes prodCode : book.producerCodes) {
                            bobProdIds.add(prodCode.producerCode);
                        }
                    }
                }
            }
        
            List<InsurancePolicy> policies = [SELECT Id,ProducerId,Producer.Name 
                                              FROM InsurancePolicy 
                                              WHERE Producer.Name IN :bobProdIds];
            List<InsurancePolicyShare> existingPolicyShares = [SELECT Id,ParentId,RowCause,Parent.IsDeleted 
                                                               FROM InsurancePolicyShare 
                                                               WHERE UserOrGroupId =: u.Id 
                                                               AND Parent.IsDeleted = false ALL ROWS];
            for (InsurancePolicyShare ips : existingPolicyShares) {
                ipsMap.put(ips.ParentId, ips);
            }


            for (InsurancePolicy policy : policies) {
                if (ipsMap.containsKey(policy.Id)) 
                {
                    if (ipsMap.get(policy.Id).RowCause == 'Manual') {
                        InsurancePolicyShare existingShare = ipsMap.get(policy.Id);
                        existingShare.AccessLevel = 'Read';
                        newShares.add(existingShare);
                    }
                    ipsMap.remove(policy.Id);
                }
                else 
                {
                    InsurancePolicyShare newShare = new InsurancePolicyShare();
                    newShare.ParentId = policy.Id;
                    newShare.UserOrGroupId = u.Id;
                    newShare.AccessLevel = 'Read';
                    newShares.add(newShare);
                }
            }
        } 

        if (!newShares.isEmpty()) {
            upsert newShares;
        }

        if (ipsMap.values().size() > 0) {
            delete ipsMap.values();
        }
    }

    global void finish(database.batchableContext bc) {
        System.debug('AmFam_NewUserAccessToPolicyBatch.finish');
    }
}