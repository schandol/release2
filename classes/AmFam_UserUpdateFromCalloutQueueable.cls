public class AmFam_UserUpdateFromCalloutQueueable implements Queueable,Database.AllowsCallouts {

    @TestVisible
    private static Boolean chainNextQueueable = true;

    private List<Id> userRecIds;

    public AmFam_UserUpdateFromCalloutQueueable(List<Id> userRecIds)
    {
        this.userRecIds = userRecIds;
    }

    //This is setup for JitSSO only and only expects a single record
    //With addition of an AfterUpdate trigger for User this code was causing a cycle. Hence the checks for anything
    //changing based on the callout. If something changed, User is updated (which will cause this queueable to be called again via
    //the trigger). If nothing changed then we can move on to the next step

    public void execute(QueueableContext context) {
        System.debug('ENTER AmFam_UserUpdateFromCalloutQueueable.execute');
        List<User> userRecs = [SELECT Id, Email, FirstName, FederationIdentifier, AmFam_User_ID__c, LastName, MiddleName, Phone, 
                                Producer_ID__c, Profile.Name, Title, UserRole.Name FROM User WHERE Id in :userRecIds];

        List<User> updatedUsers = new List<User>();
        List<Id> userIds = new List<Id>();

        for (User userRec : userRecs) {
            String amFamId = userRec.AmFam_User_ID__c;
            userIds.add(userRec.Id);

            AmFam_ProducerTypeService prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/alternatekeys?requestIdentifierType=USER_ID&responseIdentifierType=PRODUCER_ID&sourceSystemIdentifier=AMFAM&requestIdentifierValue='+amFamId);
            if (prodTypeSvc?.alternateKey?.keyId != null) {
                // Get the Producer Info from the Producer Service
                prodTypeSvc = AmFam_ProducerTypeServiceHandler.producerTypeServiceCallOut('/producers/'+prodTypeSvc.alternateKey.keyId);
            }

            if (userRec.MiddleName != prodTypeSvc?.producer?.demographicInformation?.name?.middleName)
            {
                userRec.MiddleName = prodTypeSvc?.producer?.demographicInformation?.name?.middleName;  
                chainNextQueueable = false;
            }

            if (prodTypeSvc?.producer?.contactDetails != null && 
                prodTypeSvc?.producer.contactDetails.phones.size() > 0 &&
                userRec.Phone != prodTypeSvc?.producer?.contactDetails?.phones[0].phoneNumber) 
            { 
                userRec.Phone = prodTypeSvc?.producer?.contactDetails?.phones[0].phoneNumber;
                chainNextQueueable = false;
            } 

            if (userRec.Producer_ID__c != prodTypeSvc?.producer?.producerId) 
            { 
                userRec.Producer_ID__c = prodTypeSvc?.producer?.producerId;
                chainNextQueueable = false;
            }

            updatedUsers.add(userRec);
        }

        if (chainNextQueueable)
        {
            //this batch process is dependent on the User having the producer id so lets do this here
            if(!System.isBatch() && !Test.isRunningTest()) {
                database.executebatch(new AmFam_NewUserAccessToPolicyBatch(userIds), 1);
            }

            // Chain this job to next job by submitting the next job
            if (chainNextQueueable && !Test.isRunningTest()) {
                System.debug('CHAIN AmFam_CreateUserAccountsQueueable');
                System.enqueueJob(new AmFam_CreateUserAccountsQueueable(userRecIds));
            }
        }
        else 
        {
            update updatedUsers;
        }

    }
}