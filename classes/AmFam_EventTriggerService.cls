public without sharing class AmFam_EventTriggerService {
    
    public static void extendPCIFYToEvent() {
        //The below code extending PCIFY to Event object
        if (pcify.Manager.isOnline('Event')) { 
            pcify.Processor.maskCreditCards(Trigger.new, pcify.Manager.getMaskFields('Event'),'Event');
        }
    }

}