public without sharing class AmFam_AccountWithoutSharing {
    public static Account getAccountByCDHID(String cdhID) {
        Id personAccountRTId = AmFam_Utility.getPersonAccountRecordType();
        Account existingAccount = [SELECT Id, CDHID__c, PersonContactId FROM Account WHERE RecordTypeId =: personAccountRTId AND CDHID__c =: cdhID LIMIT 1];
        return existingAccount;
    }

    public static Account getBusinessAccountByCDHID(String cdhID) {
        Id businessAccountRTId = AmFam_Utility.getAccountBusinessRecordType();
        Account existingAccount = [SELECT Id, CDHID__c FROM Account WHERE RecordTypeId =: businessAccountRTId AND CDHID__c =: cdhID LIMIT 1];
        return existingAccount;
    }

    public static void createACR(String accountId, String personAccountId, Boolean isPrimary) {
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = accountId;
        acr.ContactId = personAccountId;
        acr.FinServ__Primary__c = isPrimary;
        insert acr;
    }
    
    public static Integer numExistingACRs(Id accountId, Id contactId) {
        return [SELECT count() FROM AccountContactRelation WHERE AccountId =: accountId AND ContactId =: contactId];
    }

    public static Account getHouseholdAccountByCDHID(String cdhID) {
        Id householdAccountRTId = AmFam_Utility.getAccountHouseholdRecordType();
        Account existingAccount = [SELECT Id, CDHID__c, FinServ__SourceSystemId__c FROM Account WHERE RecordTypeId =: householdAccountRTId AND CDHID__c =: cdhID LIMIT 1];
        return existingAccount;
    }

    public static List<AccountContactRelation> getACRsForHouseholdMembers(String cdhID) {
        return [SELECT Contact.Name, FinServ__Primary__c, Account.Greeting__c FROM AccountContactRelation WHERE Account.CDHID__c =: cdhID AND Account.RecordTypeId =: AmFam_Utility.getAccountHouseholdRecordType() ORDER BY FinServ__Primary__c DESC];
    }

    public static List<AccountContactRelation> getACRsForHouseholdMembers(List<String> cdhIDs) {
        return [SELECT Contact.Name, FinServ__Primary__c, Account.Greeting__c, Account.CDHID__c FROM AccountContactRelation WHERE Account.CDHID__c IN :cdhIDs AND Account.RecordTypeId =: AmFam_Utility.getAccountHouseholdRecordType() ORDER BY FinServ__Primary__c DESC];
    }
}