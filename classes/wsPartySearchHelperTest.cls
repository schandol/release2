@isTest
public class wsPartySearchHelperTest {
    @TestSetup 
    static void setup() { 
        Account a = new Account(Name = 'Test Account', Producer_ID__c = '12345');
          Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Phone = '1231231234';
        l.LeadSource = 'Amfam.com';
        l.Company = 'Test';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
       
    }
    static testmethod void testBasicRefresh() {
 		
        Account b = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert b;
        Test.startTest();
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        
        Test.setMock(HttpCalloutMock.class, new wsPartySearchServiceCalloutHttpMock());
   		l = wsPartySearchHelper.refreshLead(l.id);
        Test.stopTest();
        
    }
    
    static testmethod void testPhoneMapping() {
        Account b = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert b;        
        Test.startTest();
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        List<wsPartyManageParty.PartyPhoneType> phoneList = new List<wsPartyManageParty.PartyPhoneType>();
        wsPartyManageParty.PartyPhoneType phone1 = new wsPartyManageParty.PartyPhoneType();
        phone1.Phone = new wsPartyManageParty.PhoneType();
        phone1.Phone.PhoneNumber = new wsPartyManageParty.PhoneNumberType();
        phone1.Phone.PhoneNumber.areaCode = '111';
        phone1.Phone.PhoneNumber.number_x = '5555555';
        phone1.PartyPhoneUsageAndDescription = new List<wsPartyManageParty.PartyPhoneUsageAndDescriptionType>();
        wsPartyManageParty.PartyPhoneUsageAndDescriptionType usage = new wsPartyManageParty.PartyPhoneUsageAndDescriptionType();
        usage.PartyPhoneUsage = 'HOME';
        phone1.PartyPhoneUsageAndDescription.add(usage);
        phoneList.add(phone1);
        l = AmFam_Utility.mapPhones(phoneList, l);
        System.assertEquals( '1115555555', l.Phone);
        
        }
    
    static testmethod void testEmailMapping() {
        Account b = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert b;        
        Test.startTest();
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        List <wsPartyManageParty.PartyEmailType> emailList = new List<wsPartyManageParty.PartyEmailType>();
        wsPartyManageParty.PartyEmailType email1 = new wsPartyManageParty.PartyEmailType();
        email1.Email = new wsPartyManageParty.EmailType();
        email1.Email.emailAddressText = 'fake@email.com';
        emailList.add(email1);

        l = AmFam_Utility.mapEmails(emailList, l);
        System.assertEquals(null, l.Email);
        emailList[0].PartyEmailUsageAndDescription = new List<wsPartyManageParty.PartyEmailUsageAndDescriptionType>();
        wsPartyManageParty.PartyEmailUsageAndDescriptionType usage = new wsPartyManageParty.PartyEmailUsageAndDescriptionType();
        usage.PartyEmailUsage = 'HOME';
        emailList[0].PartyEmailUsageAndDescription.add(usage);
        
        l = AmFam_Utility.mapEmails(emailList, l);
        System.assertEquals('fake@email.com', l.Email);
        }
    
        static testmethod void testAddressMapping() {
        Account b = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert b;        
        Test.startTest();
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
            
       	List<wsPartyManageParty.PartyAddressType> addressList = new List<wsPartyManageParty.PartyAddressType>();
        wsPartyManageParty.PartyAddressType address = new wsPartyManageParty.PartyAddressType(); 
        address.Address = new wsPartyManageParty.AddressType();
        address.Address.addressLine1 = '123 W Main St';
        address.PartyAddressPurposeAndUsage = new List<wsPartyManageParty.PartyAddressPurposeAndUsageType>();
		wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurpose = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurpose.PartyAddressPurpose = 'PRM';
        usageAndPurpose.PartyAddressUsage = 'R';    
        address.PartyAddressPurposeAndUsage.add(usageAndPurpose);
        
        wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurposeForMailing = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurposeForMailing.PartyAddressPurpose = 'PRM';
        usageAndPurposeForMailing.PartyAddressUsage = 'M';    
        address.PartyAddressPurposeAndUsage.add(usageAndPurposeForMailing);
            
		addressList.add(address);
            
        wsPartyManageParty.PartyAddressType address2 = new wsPartyManageParty.PartyAddressType(); 
        address2.Address = new wsPartyManageParty.AddressType();
        address2.Address.addressLine1 = '143 N Main St';
        address2.PartyAddressPurposeAndUsage = new List<wsPartyManageParty.PartyAddressPurposeAndUsageType>();
		wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurpose2 = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurpose2.PartyAddressPurpose = 'SEC';
        usageAndPurpose2.PartyAddressUsage = 'R';    
        address2.PartyAddressPurposeAndUsage.add(usageAndPurpose2);
            
        wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurposeForMailing2 = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurposeForMailing2.PartyAddressPurpose = 'OTHER';
        usageAndPurposeForMailing2.PartyAddressUsage = 'M';    
        address2.PartyAddressPurposeAndUsage.add(usageAndPurposeForMailing2);
            
		addressList.add(address2);
            
        wsPartyManageParty.PartyAddressType address3 = new wsPartyManageParty.PartyAddressType(); 
        address3.Address = new wsPartyManageParty.AddressType();
        address3.Address.addressLine1 = '223 S Main St';
        address3.PartyAddressPurposeAndUsage = new List<wsPartyManageParty.PartyAddressPurposeAndUsageType>();
		wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurpose3 = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurpose3.PartyAddressPurpose = 'SEC';
        usageAndPurpose3.PartyAddressUsage = 'R';    
        address3.PartyAddressPurposeAndUsage.add(usageAndPurpose3);
            
        wsPartyManageParty.PartyAddressPurposeAndUsageType usageAndPurposeForMailing3 = new wsPartyManageParty.PartyAddressPurposeAndUsageType();      
        usageAndPurposeForMailing3.PartyAddressPurpose = 'OTHER';
        usageAndPurposeForMailing3.PartyAddressUsage = 'M';    
        address3.PartyAddressPurposeAndUsage.add(usageAndPurposeForMailing3);
		addressList.add(address3);
            
        l = AmFam_Utility.mapAddresses(addressList, l);
        System.assertEquals('123 W Main St', l.Street);
        
        }
    
    static testmethod void testMapEntrantToLead() {
        Account b = new Account(Name = 'Test Account', Producer_ID__c = '12345');
        insert b;
        Test.startTest();
        
        Account a = [SELECT Id FROM Account LIMIT 1];
        
        Lead l = new Lead();
        l.Agency__c = a.Id;
        l.FirstName = AmFam_TestDataFactory.generateRandomString(5);
        l.LastName = AmFam_TestDataFactory.generateRandomStringWithoutSpecifiedString(5,'nln');
        l.Email = 'testing@test.com';
        l.Email_Usage__c = 'HOME';
        l.Lines_of_Business__c = 'Personal Vehicle;Property';
        
        
        wsPartyManageParty.EntrantType entrantResponse = new wsPartyManageParty.EntrantType();
        entrantResponse.EntrantDemographicInfo = new wsPartyManageParty.EntrantDemographicInfoType();
        entrantResponse.EntrantIdentityInfo = new wsPartyManageParty.EntrantIdentityInfoType();
        entrantResponse.EntrantDemographicInfo.PersonName = new wsPartyManageParty.PersonNameType();
        entrantResponse.EntrantDemographicInfo.PersonName.firstName = 'John';
        entrantResponse.EntrantDemographicInfo.PersonName.lastName = 'Smith';
        
        
        l = wsPartySearchHelper.mapEntrantToLead(entrantResponse, l);
        System.assertEquals('John', l.FirstName);
        System.assertEquals('Smith', l.LastName);
        
        }
    
}