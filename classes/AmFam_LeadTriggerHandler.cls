public class AmFam_LeadTriggerHandler {

    public void OnBeforeInsert (List<Lead> newLeads) {
        AmFam_LeadService.markImportedLeadsLPEStatus(newLeads);
        AmFam_LeadService.extendPCIFYToLead();
        AmFam_LeadService.setValueOnCreation(newLeads);
    }
    public void OnAfterInsert (List<Lead> newLeads) {
        wsPartyManage.addEntrants(newLeads, null);
        AmFam_LeadAssociationToProducer.leadAssociationToProducers(newLeads);
        AmFam_LeadService.setSharingAccessForNewLeads(newLeads);
        Amfam_LeadRefreshHelper.refreshLeadsFromTrigger(newLeads);
    }
    public void OnAfterUpdate (List<Lead> newLeads,
                                List<Lead> oldLeads,
                                Map<ID, Lead> newLeadsMap,
                                Map<ID, Lead> oldLeadsMap) {
        wsPartyManage.addEntrants(newLeads, oldLeadsMap);
        //wsPartyManage.editEntrants(newLeads, oldLeadsMap);
        AmFam_LeadAssociationToProducer.leadAssociationToProducers(newLeads);
        AmFam_LeadService.setSharingForOwnerChangeLeads(newLeads,oldLeadsMap);
        ApexActServiceHelper.updateApexNotes(newLeads, oldLeadsMap);
    }
    public void OnBeforeUpdate(List<Lead> newLeads,
                                List<Lead> oldLeads,
                                Map<ID, Lead> newLeadsMap,
                                Map<ID, Lead> oldLeadsMap) {
        AmFam_LeadService.checkLeadOwnerChangeBelongsToSameAgency(newLeads, newLeadsMap, oldLeadsMap);
        //sendLeadEditToCDH method on AmFam_NewLeadController would be covering this scenario 
        //AmFam_LeadService.lockLeadChangesAtContactPartyLevel(newLeads, oldLeadsMap);
        AmFam_LeadService.leadStageDispositionValues(newLeads);
        AmFam_LeadService.extendPCIFYToLead();
    }
}