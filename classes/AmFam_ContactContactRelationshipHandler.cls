/**
*   @description: trigger handler for Contact-Contact Relationship object. Note: this should be the only place
*	where ALL Contact-Contact Relationship related trigger logic should be implemented.
*   @author: Anbu Chinnathambi
*   @date: 08/10/2021
*   @group: Trigger Handler
*/

public without sharing class AmFam_ContactContactRelationshipHandler {
    
    /*
    public void OnBeforeInsert (List<FinServ__ContactContactRelation__c> newRecords) {
    }
    */

    public void OnAfterInsert (List<FinServ__ContactContactRelation__c> newRecords, Map<ID, FinServ__ContactContactRelation__c> newRecordsMap) {

        AmFam_ContactContactRelationshipService.deleteDuplicateContactContactRel(newRecords, newRecordsMap);
    }

    /*
    public void OnAfterUpdate (List<FinServ__ContactContactRelation__c> newRecords,
                               List<FinServ__ContactContactRelation__c> oldRecords,
                               Map<ID, FinServ__ContactContactRelation__c> newRecordsMap,
                               Map<ID, FinServ__ContactContactRelation__c> oldRecordsMap) {

    }


    public void OnBeforeUpdate(List<FinServ__ContactContactRelation__c> newRecord,
                               List<FinServ__ContactContactRelation__c> oldRecord,
                               Map<ID, FinServ__ContactContactRelation__c> newRecordsMap,
                               Map<ID, FinServ__ContactContactRelation__c> oldRecordsMap) {

    }

    public void OnBeforeDelete(List<FinServ__ContactContactRelation__c> oldRecords,
    						   Map<ID, FinServ__ContactContactRelation__c> oldRecordsMap) {

    }
    
    
    public void OnAfterDelete(List<FinServ__ContactContactRelation__c> oldRecords,
    						   Map<ID, FinServ__ContactContactRelation__c> oldRecordsMap) {
    }*/



}