import { LightningElement,wire,api } from 'lwc';
import getHouseholdOpportunities from '@salesforce/apex/AmFam_HouseholdOppsListController.getHouseholdOpportunities';

const columns = [
    {label: 'Opportunity Name',fieldName: 'NameUrl',type: 'url',typeAttributes: {label: { fieldName: 'Name' }, 
        target: '_parent'},hideDefaultActions:"true"},
    {label: 'Amount', fieldName: 'Amount', type: 'currency', hideDefaultActions:"true", typeAttributes: { currencyCode: 'USD', step: '0.01'}},
    {label: 'Stage', fieldName: 'StageName', type: 'text', hideDefaultActions:"true"},
    {label: 'Type', fieldName: 'Type', type: 'text', hideDefaultActions:"true"},
    {label: 'Lines of Business', fieldName: 'LinesOfBusiness', type: 'text', hideDefaultActions:"true"},    
    {label: 'Member', fieldName:'AccountName', type: 'text', hideDefaultActions:"true"}
];

export default class amFam_HouseholdOpportunitiesList extends LightningElement 
{
    @api recordId;
    opportunities;

    columns = columns;

    @wire(getHouseholdOpportunities, { householdAccountId: '$recordId' })
    wireOpps({ error, data }) {

        if (data)
        {
            var flattenedData=[];

            for (let rec of data)
            {
                let row={'Name':rec.Name,'Amount':rec.Amount,'StageName':rec.StageName,'Type':rec.Type,
                        'AccountName':rec.Account.Name,'LinesOfBusiness':rec.Lines_of_Business__c,'NameUrl':'/'+rec.Id};
                flattenedData.push(row);
            }

            this.opportunities = flattenedData;
        }
    }

 }